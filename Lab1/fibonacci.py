'''@file fibonacci.py
Docstring is required at the beginning of a Python source file
with file tagged in it
          number.
   @brief      Calculates Fibonacci value at desired index.
   @details    Computs a while loop until desired Fibonacci value is calculated.
               See code here https://bitbucket.org/dhern138/me305_lab/src/master/Lab1/
   @author     David Hernandez
   @date       1/16/21
'''

def fib (idx):
    '''
    @brief      This function calculates a Fibonacci number at a specific index.
    @param idx  An integer specifying the index of the desired Fibonacci
                number.
    '''
    ## Initial Fibonacci Terms
    f0 = 0
    f1 = 1
    count = 1
    
  

    if   int(idx) < 0:
        
        return ('Incorrect. Please Input Positive Integers only')
     
    elif int(idx) <= 1:
        
        return idx
     
    else:
        
        while count < int(idx):
            fi = f0 + f1
            f0 = f1
            f1 = fi
            count += 1
            
        return fi
        
        

if __name__ == '__main__':
    # Will allow testing of this Fibonacci function. Any code
    # within the if __name_ == '_main_' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    
    
    ## The index of the fibonacci number selected by the user
    idx = input(' Please enter index for desired Fibonacci number (ctrl+c at any time to quit):')
    
print ('Fibonacci number at'
               ' index {:} is {:}.'.format(idx,fib(idx)))
    
    

