'''@file        leds.py
@brief          Cycles through three various LED pulses.
@details        Implements a finite state machine, shown below, to transition
                between three modes of LED states.
                Here is a snapshot of my new application:
                    *  \image html diagram.jpg width=800cm height=600cm
                    *  \image html leds.mp4
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/Lab2/leds.py
                    
                See video demonstration of LED states here:
                    https://drive.google.com/file/d/15A_SqO3HJPLcVTp4pzM3yLnQ9Vt7Zcjq/view?usp=sharing
@image          IMAGE_PATH 
@author         David Hernandez
@date           2/05/21
@copyright      2020-2021 Hernandez
'''
import utime
import math
import pyb
from pyb import Pin


# Initiates the variables at 0
myVar = 0
## @brief   Acts as a counter when buttons is pressed
#
state = 0
## @brief   Initiates the state counter at 0
#
start = 0
## @brief   Initiates tick counter at 0
#
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
## @brief   Locates blue button on nucleo
#  @details Associates further commands with this button
#
pinLED = Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief   Locates LED on nucleo
#  @details Associates further commands with this LED
#

def onButtonPressFCN(IRQ_src):
    '''@brief   Commands the button to switch LED patterns
       @details Callback button to switch between patterns
       @param   IRQ_src the command to give the button
                myVar counts amount button is press
                start counts the milliseconds that pass
                since button was pushed.
    '''
    global myVar
    myVar += 1
    
    
    global start
    start = utime.ticks_ms()
          
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
## @brief    Sets the callback
#  @details  The command interrupts current code and calls back function
#            adding a counter each time
#

if __name__=="__main__":
    '''@brief   Initiates program when script is executed only.
    '''
    # Program initialization goes here
    state = 0   # Initial state is the init state
    print (' Welcome! Please press the Blue button to cycle' 
            ' through the led patterns or    (ctrl+c at any time to quit):')
    
    while True:
        try:
            
             tim2 = pyb.Timer(2, freq = 20000)
             ## @brief Sets timer channel for LED on nucleo
             #
             t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin = pinLED)
             ## @brief Control pulse width percent of LED
             #
             time = 0
             
            # Main program code goes here
             if state == 0:
                # run state 0 (init) code 
                  
                ## If button is pressed it will transition from help menu 
                # to led pattern 1
                if myVar == 1:
                    state = 1
                    print('Squarewave pattern selected')
                   
                               
             elif state == 1:
                      # Sets a time limit for led to be on and off
                     if utime.ticks_diff(utime.ticks_ms(),start) < 500:
                         t2ch1.pulse_width_percent(100)
                     elif utime.ticks_diff(utime.ticks_ms(),start)  >= 500 and utime.ticks_diff(utime.ticks_ms(),start) <= 1000:
                         t2ch1.pulse_width_percent(0)   
                     elif utime.ticks_diff(utime.ticks_ms(),start)  > 1000:
                      # Reset timer for next transition
                         start = utime.ticks_ms()
                                       
                     # Transitions the led from state 1 to state 2
                     # Commencing led pattern 2
                     if myVar == 2:
                          state = 2
                          print('Sinewave pattern selected')
                          x = 0
                                                           
             elif state == 2: 
                     #Starts the sin wave period at 0 x-axis with 50% light              
                     if utime.ticks_diff(utime.ticks_ms(),start) >= 0:
                         # Equation for desired sin wave
                         sin = 50*(math.sin((2*math.pi/10)*x)+1)
                         # Command to PWM
                         t2ch1.pulse_width_percent(sin)
                         # Time to increase by each cycle
                         x += 0.0005
                         #print(x)
                         #print(sin)
                     elif utime.ticks_diff(utime.ticks_ms(),start)  > 10000:
                      # Reset timer for next transition
                         start = utime.ticks_ms()
                   
                    # On button press it transitions to sawtooth pattern
                     if myVar == 3:
                         state = 3
                         print('Sawtooth pattern selected')
                         time = 0
                         
            
             elif state == 3: 
                    # Start sawtooth pattern by taking difference of time
                    if utime.ticks_diff(utime.ticks_ms(),start) >= 0 and utime.ticks_diff(utime.ticks_ms(),start) <= 1000:
                     # Variable that accounts for each ms that passes
                     time = utime.ticks_diff(utime.ticks_ms(),start)
                     # Variable used in PWM for LED brightness
                     duty = 100*(time/100)
                     t2ch1.pulse_width_percent(duty)
                     #print (time)
                     #print (duty)
                    elif utime.ticks_diff(utime.ticks_ms(),start) > 1000:
                      # Reset timer for next transition
                         start = utime.ticks_ms()


                    # On button press return to state 1                   
                    if myVar == 4:                        
                        print('Back to Squarewave!')
                        pass
                        myVar = 1
                        state = 1
                        
             else:                
                print('Invalid input, goodbye!')
                break
            # code to run if state is invalid
            # program should ideally never reach here
            
           
        except KeyboardInterrupt:
                # This except block catches "Ctrl+C" from the keyboard
                # while (True) loop when desired
                break