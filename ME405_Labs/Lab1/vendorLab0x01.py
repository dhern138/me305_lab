""" @file               vendorLab0x01.py 
    @brief              This files will perform the common functions of a typical vending machine.
    @details            The common functions are: Displaying items to vend and their prices, accept funds,
                        compute additional funds to balance, subtract funds if a purchase was made and display
                        the remaining balance, vend beverage, and have the capacity to return all funds when
                        customer hits the eject command.
                        The following is an image of the transition diagram used to write this code:
                            \image html Vendotron_Diagram.png width=800cm height=600cm
                        See files here:
                            https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab1/vendorLab0x01.py
    @author             David Hernandez
    @date               Apr. 8, 2021
    @copyright          Hernandez 2020 - 2021
"""

from time import sleep
import keyboard 

## This command resets previous keyboard input and prepares to accept next key input
last_key = ''
## @brief       Command used to callback on keyboard demands by user.
#

## This dictionary has all the beverages and their corresponding prices
#  It also has the associate keyboard value
drinkDict = {'c':('Cuke', 100), 'p':('Popsy',120), 'd':('Dr.Pupper', 110), 's':('Spryte', 85)}
## @brief       This dictionary lists the nomenclature and price tag for each available beverage.
#
moneyDict = {'0':('Penny', 1), '1':('Nickel', 5), '2':('Dime', 10), '3':('Quarter', 25), '4':('One Dollar', 100),
             '5':('Five-Dollars', 500), '6':('Ten-Dollars', 1000), '7':('Twenty-Dollars', 2000)}
## @brief       This dictionary lists the nomenclature and coin value for each allowed coin or bill.
#

def kb_cb(key):
    """ @brief              Callback function which is called when a key has been pressed.
        @details            When user inputs any of the designated keys, this functions will output
                            the prescribed outcome.
        @param key          key, is locating and asscociating a letter with the keyboard.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
## Commands to select a beverage, eject, or end service
keyboard.on_release_key("c", callback=kb_cb) # Vends Cuke
keyboard.on_release_key("p", callback=kb_cb) # Vends Popsi
keyboard.on_release_key("d", callback=kb_cb) # Vends Dr. Pupper
keyboard.on_release_key("s", callback=kb_cb) # Vends Spryte
keyboard.on_release_key("e", callback=kb_cb) # Ejects remaining balance
keyboard.on_release_key("q", callback=kb_cb) # Pulls out of keyboard function

## Monetary values will be indexed and called using these values
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)


def getChange(price, pmt):
    ''' @brief              This functions will return a correct change calculation.
        @details            Will compute monetary transactions and return the change with
                            the fewest denominations as possible. Some of the code used within 
                            this function was borrowed from our generous instruction: Charlie Refvem.
        @param price        price, is an integer representing the price of the item
                            in cents.
        @param pmt          pmt, A collection indicating the quantity of each
                            denomination (penny, nickel, dime, quarter, one-five-ten and
                            twenty dollar bills).
        @return             If the payment is sufficient, returns a  list representing
                            the computed change in the same format as pmt. If payment
                            is insufficient, returns None.
    '''

    #  Will iterate through each denomination according to customer input
    pay = pmt

    # If the customer supplied insufficient funds then it will return a None (no change)
    if pay < price:
        return None
    
    # If the customer has supplied sufficient funds, then the function will compute the
    # price and payment difference to provide the change if any
    change = pay - price

    return change


def VendotronTask():
    """ @brief      Continuously runs the vending machine functions.
        @details    Will indefinitely loop through the task until a key is pressed
                    for beverage selection or refund. This function will accept 
                    the command for the desired beverage, display the required funds and balance,
                    compute the change by calling on getChange function, readily accept further 
                    purchases or eject remaining balance at any time.
    """

    global last_key
    state = 0
    funds = 0
    ## @brief       Bills or coins added to the Vendotron by customer.
    #
    balance = 0
    ## @brief       Total balance still within the Vendotron applied by customer.
    #

    while True:
        ## Initialization state
        if state == 0:
            print('Welcome to Vendotron! Please press c for Cuke($1.00), p for Popsi($1.20), d for Dr. Pupper($1.10),'
                  's for Spryte($0.85), or e for a refund!')
            state = 1

        ## Monetary and Beverage Selection State
        elif state == 1:
                # Implement FSM using a while loop and an if statement
                # will run eternally until user presses CTRL-C
                if last_key == 'c':
                    cuke_sel = 'c'
                    last_key = ''
                    print('You have selected:', drinkDict[cuke_sel][0])
                    fee = drinkDict[cuke_sel][1]
                    state = 3
                elif last_key == 'p':
                    popsi_sel = 'p'
                    last_key = ''
                    print('You have selected:', drinkDict[popsi_sel][0])
                    fee = drinkDict[popsi_sel][1]
                    state = 3
                elif last_key == 'd':
                    drpupper_sel = 'd'
                    last_key = ''
                    print('You have selected:', drinkDict[drpupper_sel][0])
                    fee = drinkDict[drpupper_sel][1]
                    state = 3
                elif last_key == 's':
                    spryte_sel = 's'
                    last_key = ''
                    print('You have selected:', drinkDict[spryte_sel][0])
                    fee = drinkDict[spryte_sel][1]
                    state = 3
                elif last_key == '0':
                    penny_sel = '0'
                    print('You have entered:', moneyDict[penny_sel][0])
                    last_key = ''
                    funds = moneyDict[penny_sel][1]
                    state = 2
                elif last_key == '1':
                    nickel_sel = '1'
                    print('You have entered:', moneyDict[nickel_sel][0])
                    last_key = ''
                    funds = moneyDict[nickel_sel][1]
                    state = 2
                elif last_key == '2':
                    dime_sel = '2'
                    print('You have entered:', moneyDict[dime_sel][0])
                    last_key = ''
                    funds = moneyDict[dime_sel][1]
                    state = 2
                elif last_key == '3':
                    quarter_sel = '3'
                    print('You have entered:', moneyDict[quarter_sel][0])
                    last_key = ''
                    funds = moneyDict[quarter_sel][1]
                    state = 2
                elif last_key == '4':
                    ones_sel = '4'
                    print('You have entered:', moneyDict[ones_sel][0])
                    last_key = ''
                    funds = moneyDict[ones_sel][1]
                    state = 2
                elif last_key == '5':
                    fives_sel = '5'
                    print('You have entered:', moneyDict[fives_sel][0])
                    last_key = ''
                    funds = moneyDict[fives_sel][1]
                    state = 2
                elif last_key == '6':
                    tens_sel = '6'
                    print('You have entered:', moneyDict[tens_sel][0])
                    last_key = ''
                    funds = moneyDict[tens_sel][1]
                    state = 2
                elif last_key == '7':
                    twenty_sel = '7'
                    print('You have entered:', moneyDict[twenty_sel][0])
                    last_key = ''
                    funds = moneyDict[twenty_sel][1]
                    state = 2
                elif last_key == 'e':
                    print("Here is your change. Thank you!")
                    last_key = ''
                    state = 5
                elif last_key == 'q':
                    print("'q' detected. Quitting...")
                    keyboard.unhook_all ()  # Removes lock on keyboard keys.
                else:
                    pass
        ## Balance state
        #  Total funds within the vendetron applied by customer
        elif state == 2:
            balance += funds
            print('Current Balance:\t${:.2f}'.format(balance/100))
            state = 1
            
        ## User selected beverage
        #  Will calculate if sufficient funds have been applied
        #  If sufficient funds then beverage will vend
        #  If not, then customer will be requested to add more funds
        elif state == 3:

            if last_key =='e':
                state = 5

            print('Please pay for Cuke($1.00), p for Popsi($1.20), d for Dr. Pupper($1.10),'
                  's for Spryte($0.85), or e for a refund!')
            payed = balance
            change = getChange(fee, payed)
            # This prints the price of the chosen beverage
            print('Price:\t${:.2f}'.format(fee/100))
            print('Payment:\t${:.2f}'.format(payed/100))
            # This portion will check if there are enough funds for purchase
            if change is not None:
                ## The value of the change returned.
                change = payed - fee
                print('Change:\t${:.2f}'.format(change/100))
                balance = change
                state = 4
            # If the funds were insufficient, display a message indicating so
            else:
                print('Insufficient funds, Please add more to continue with purchase')
                state = 0

        ## Vending State
        #  vends the desired beverage
        elif state == 4:
            # perform state 2 operations
            print('Here is your beverage!')
            print("The state is",state)
            state = 0

        ## Eject State
        #  refunds all unused funds
        elif state == 5:
            # perform state 3 operations
            print("The state is",state)
            print('Here is your change. Thank you!')
            print('Total Returned:\t${:.2f}'.format(balance/100))
            balance = 0
            state = 0

        ## Should not get here
        else:
            pass
        
        yield(state)



if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')