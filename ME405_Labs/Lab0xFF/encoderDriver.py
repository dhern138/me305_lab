# -*- coding: utf-8 -*-
''' @file       encoderDriver.py
    @brief      Collects and updates positional and speed data. With unit conversion of speed and position.
    @details    Collects, appends and updates the data received from the encoder
                and motors actuated. Then sends that data to controllerTask.
    @author     David Hernandez
    @author     Sydney Lewis
    @date       5/26/21
    @copyright  2020-2021 Hernandez & Lewis Inc.
'''

import utime
import pyb
from pyb import Pin, Timer
from math import pi


class encoderDriver:
    ''' @brief      Class that creates objects for an encoder and updates corrected position, and speed
                    through unit conversion.
        @details    This encoder class will continuously interpret the information from
                    quadrature encoder. This data will be able to be pulled for viewing
                    and for collection.
                See source code here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/encoderDriver.py

    '''

    def __init__(self,PinA, PinB, TIM, sign=1):  # Constructor to initialize attributes
        ''' @brief      Initialization of the system
            @details    All methods and attributes associated with the encoder class
            @param      PinA pyb.Pin.cpu.### for first input of encoder used
            @param      PinB pyb.Pin.cpu.### for second input of encoder used.
            @TIM        Timer being used to run encoder of choice
        '''

        self.PinA = Pin(PinA, pyb.Pin.IN)
        ## @brief       Sets up generic pin input to be called on by a task
        #  @details     First pin input for encoder
        #
        self.PinB = Pin(PinB, pyb.Pin.IN)
        ## @brief       Sets up generic pin input to be called on by a task
        #  @details     Second pin input for encoder
        #

        ## Encoder timer set-up
        # Object of clock that reads the encoders channels
        self.TIM = Timer(TIM, period=0xFFFF, prescaler=0)
        ## @brief       Sets up generic timer input to be called on by a task
        #
        # Object of channels for encoder
        self.TIM.channel(1, Timer.ENC_AB, pin = self.PinA)
        self.TIM.channel(2, Timer.ENC_AB, pin = self.PinB)

        self.period = 0x10000
        ## @brief Period of timer @ [80 MHz]
        #
        self.encoderPosition = 0
        ## @brief most up-to-date encoder position in [deg]
        #
        self.previousPosition = 0
        ## @brief previous encoder position in [deg]
        #
        self.delta = 0
        ## @brief change in sign.
        #
        self.speedMeas = 0
        ## @brief most up-to-date measured motor speed in [RPM]
        #
        self.previousCount = 0
        ## @brief previous counter in [ticks]
        #
        self.previousTime = 0
        ## @brief previous counter time in [s]
        #
        self.time = 0
        ## @brief encoder counter time in [s]
        #
        self.sign = sign
        ## @brief will help distinguish encoder values from pos to neg if necessary.
        #
        
    ## Functions associated with each button command
    def update(self):
        ''' @brief      Called regularly to update recorded position and speed.
            @details    Algorithm to correct delta in the case of overflow or underflow
                        of both the encoder and motor. Then sends the appended data to
                        the controllerTask.
            @param      oldDelta keeps track of initial delta for calculations
        '''
        
        ####-----Timer in [seconds] associated with each tick------######
        self.time = utime.ticks_us()
        self.deltaTime = utime.ticks_diff(self.time, self.previousTime)
        self.previousTime = self.time

        ####-----Encoder counter in ticks ------#######
        self.counter = self.TIM.counter()
        self.deltaPosition = self.counter - self.previousCount
        self.previousCount = self.counter


        ## Alogorithm for BAD DELTAS
        # CASE A Position GREATER than Period
        if self.deltaPosition > 0.5*self.period:
            self.deltaPosition -=  self.period


        # CASE B: Position LESS than Period
        elif self.deltaPosition < -0.5*self.period:
            self.deltaPosition += self.period

        else:
            pass
        
        ## Corrects sign [rad]
        self.delta = self.sign*(self.deltaPosition)*(360/4000)*(pi/180)
        
        # Gives corrected position of the encoder in [Rad]
        # Unit conversion: [ticks*(DEG/ticks)*(RAD/DEG)]
        # IMPORTANT 360 [deg]/4000 [ticks] conversion
        self.encoderPosition += self.delta


        # Gives corrected angular speed in [Rad/Sec]
        # Unit conversion [(Rad/us)*(sec/1e6us)]
        self.speedMeas = (self.delta*1e3/self.deltaTime)
        
        return self.encoderPosition
        return self.speedMeas
        return self.deltaTime


    def set_position(self):
        ''' @brief Resets the position 0 where encoder is physically placed
            @param reset sets the position to 0 at desired location
        '''
        self.encoderPosition = 0
        self.deltaPosition = 0
        self.speedMeas = 0
        self.deltaTime = 0
        self.counter = 0
        self.time = 0
        self.previousTime = 0
        self.previousCount = 0


    def get_position(self):
        ''' brief Returns the most recently updated position of the encoder in [deg]
        '''
        return self.encoderPosition


    def get_delta(self):
        ''' @brief      Returns the angular speed of motor in [deg/sec]
            @details    Divides the corrected position by the period of timer 
                        to get the angular speed of the motor
        '''
        return self.speedMeas
        
if __name__ == '__main__':
    '''
    Test encoderDriver only within this file.
    '''

    ## Pin initialization for Encoders
    PB6 = Pin(pyb.Pin.cpu.B6)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB6 for encoder
    #
    PB7 = Pin(pyb.Pin.cpu.B7)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB7 for encoder
    #
    PC6 = Pin(pyb.Pin.cpu.C6)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinC6 for encoder
    #
    PC7 = Pin(pyb.Pin.cpu.C7)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinC7 for encoder
    #
    ## Encoder 1 timer set-up
    TIM4 = Timer(4, period=0xFFFF, prescaler=0)
    TIM4.channel(1, Timer.ENC_AB, pin = PB6)
    TIM4.channel(2, Timer.ENC_AB, pin = PB7)
    
    ## Encoder 2 timer set-up when required
    TIM8 = Timer(8, period=0xFFFF, prescaler=0)
    TIM8.channel(1, Timer.ENC_AB, pin = PC6)
    TIM8.channel(2, Timer.ENC_AB, pin = PC7)
    
    ## Encoder set up for Encoder 1 and Encoder 2
    e1 = encoderDriver(PB6, PB7, 4)
    ## @brief       Sets the first encoder up with pins B6 and B7 with timer 4
    # @details      Calls on encoderDriver class to input the required arguments into constructor.
      # @param       self.PB6 calls on pin C6 to get into first pin channel
      # @param       self.PB7 calls on pin C7 to get into second pin channel
      # @param       4 is the timer being used for the pin and channel inputs
    #
    e2 = encoderDriver(PC6, PC7, 8)
    ## @brief       Sets the second encoder up with pins C6 and C7 with timer 8
      # @details     Calls on encoderDriver class to input the required arguments into constructor.
      # @param       self.PC6 calls on pin C6 to get into first pin channel
      # @param       self.PC7 calls on pin C7 to get into second pin channel
      # @param       8 is the timer being used for the pin and channel inputs
    #
    while True:

        ## Selecting test type
        test = input('Run or timed test? (r/t): ')
        ## Selecting encoder
        enc = input('Which encoder would you like to test? (1/2): ')
        e1.update()
        e2.update()

        if test == 't':
            if enc == '2':
                print('Testing Encoder 2')
                while True:
                    start = utime.ticks_us()
                    position = e2.get_position()
                    stop = utime.ticks_us()
                    interval = utime.ticks_diff(stop,start)
                    print('Encoder 2 position {:} [deg] in {:} us\n'.format(position, interval))
                    start = utime.ticks_us()
                    delta = e2.get_delta()
                    stop = utime.ticks_us()
                    interval = utime.ticks_diff(stop,start)
                    print('Encoder 2 position {:} [deg] in {:} us\n'.format(position, interval))
                    e2.update()
                    input('Press enter to repeat test: ')
            else:
                print('Testing Encoder 1')
                while True:
                    start = utime.ticks_us()
                    position = e1.get_position()
                    stop = utime.ticks_us()
                    interval = utime.ticks_diff(stop,start)
                    print('Encoder 1 position {:} [Rad] in {:} us\n'.format(position, interval))
                    start = utime.ticks_us()
                    delta = e1.get_delta()
                    stop = utime.ticks_us()
                    interval = utime.ticks_diff(stop,start)
                    print('Encoder 1 position {:} [Rad] in {:} us\n'.format(position, interval))
                    e1.update()
                    input('Press enter to test again: ')
        else:
            if enc == '2':
                print('Testing Encoder 2')
                while True:
                    position = e2.get_position()
                    delta = e2.get_delta()
                    print('Encoder 2 position: {:} [Rad] delta: {:} [rad/s]'.format(position, delta))
                    e2.update()
                    utime.sleep_ms(250)
            else:
                print('Testing Encoder 1')
                while True:
                    position = e1.get_position()
                    delta = e1.get_delta()
                    print('Encoder 1 position: {:} [Rad] delta: {:} [Rad/s]'.format(position, delta))
                    e1.update()
                    utime.sleep_ms(250)