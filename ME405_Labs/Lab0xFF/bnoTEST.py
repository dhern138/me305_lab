'''@file        bnoTEST.py
@brief          This program is a test for the BNO055 IMU.
@details        When combined with the BNO055 driver, the IMU will be able to use inertial measurement
                to provide enhanced feedback and error checking during balancing operation.

@author         Sydney Lewis
@author         David Hernandez
@date           06/08/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''
import pyb
from pyb import I2C
import BNO055
from BNO055 import bno055
import utime
import shares


if __name__ == "__main__": 
    while True:  
        ## Check for state
        state = shares.bnoState.get() 
        
        ## State 0: Instantiating I2C bus and checking sensor connection
        if state == 0:
            print('init')
            i2c = I2C(1)
            i2c.init(I2C.MASTER, baudrate=115200, gencall=False, dma=False)
            utime.sleep_ms(100)
            print('setting up IMU')
            imu = bno055(i2c)
            print('checking device ID')
            imu.check()
            if True:
                print('test confirmed')
                shares.bnoState.put(1)
                utime.sleep_ms(100)
                pass
            else:
                print('No device found')
                pass
         
        ## State 1: Set BNO Operating Mode to NDOF
        elif state == 1:
            print('setting operating mode')
            imu.mode()
            shares.bnoState.put(2)
            utime.sleep_ms(100)
            
        ## State 2: Check Calibration Status
        elif state == 2:
            print('checking calibration')
            imu.cal_status()
            shares.bnoState.put(3)
            utime.sleep_ms(100)
            
        ## State 3: Read Euler Angles and Angular Velocity
        elif state == 3:
            print('')
            print('reading euler angles and angular velocity')
            imu.Euler()
            imu.omega()
            angle = imu.get_Eul()
            omega = imu.get_omega()
            print('Euler yaw, pitch, roll: {:}, {:}, {:} [rad]\n'.format(angle[0], angle[1], angle[2]))
            #print('Angular velocity x, y, z: {:}, {:}, {:} [rad]\n'.format(omega[0], omega[1], omega[2]))

            utime.sleep_ms(300)
            

        else:
            pass
    
        

