# -*- coding: utf-8 -*-
''' @file       shares.py
    @brief      Baak of variables used by tasks to store and retrieve data.
    @details    Can be used across other files and classes
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''

import task_share

encoderPosition  = task_share.Share('f', thread_protect = False, name = "encoderPosition")
## @brief array of up-to-date encoder position
# #

encoder2Position = task_share.Share('f', thread_protect = False, name = "encoder2Position")
## @brief Retrieves current position of encoder 2
#
speed1Meas       = task_share.Share('f', thread_protect = False, name = "speed1Meas")
## @brief Retrieves angular speed of encoder 1 in RPM
#
speed2Meas       = task_share.Share('f', thread_protect = False, name = "speed2Meas")
## @brief Retrieves angular speed of encoder 2 in RPM
#
tref1            = task_share.Share('f', thread_protect = False, name = "tref1")
## @brief encoder 1 elapsed time during operation.
#
tref2            = task_share.Share('f', thread_protect = False, name = "tref2")
## @brief encoder 2 elapsed time during operation.
#
alpha_gain       = task_share.Share('f', thread_protect = False, name = "alpha_gain")
## @brief Desired user gain
#
beta_gain        = task_share.Share('f', thread_protect = False, name = "beta_gain")
## @brief Desired user position gain
#
speedRef         = task_share.Share('f', thread_protect = False, name = "speedRef")
## @brief Desired user speed
#
posMeas          = task_share.Share('f', thread_protect = False, name = "posMEAS")
## @brief Measurement of position
#
pos1Ref          = task_share.Share('f', thread_protect = False, name = "pos1REF")
## @brief Estimate of encoder 1 position.
#
pos2Ref          = task_share.Share('f', thread_protect = False, name = "pos2REF")
## @brief Estimate of encoder 2 position.
#
PWM1             = task_share.Share('f', thread_protect = False, name = "PWM1")
## @brief Duty cycle for motor 1.
#
PWM2             = task_share.Share('f', thread_protect = False, name = "PWM2")
## @brief Duty cycle for motor 2.
#
ENCODERstate     = 0#task_share.Share('I', thread_protect = False, name = "ENCODERstate")
## @brief       Current state of class encoderTask 
#  @details     Helps transition between states for each button pressed
#
BACKstate        = 0#task_share.Share('I', thread_protect = False, name = "Backstate")
## @brief       Current state of class encoderTask 
#  @details     Helps transition between states for each button pressed
#
x  =0# task_share.Share('f', thread_protect = False, name = "x")
y  =0# task_share.Share('f', thread_protect = False, name = "y")
vx =0# task_share.Share('f', thread_protect = False, name = "vx")
vy =0# task_share.Share('f', thread_protect = False, name = "vy")
# x                = task_share.Queue ('f', 8, thread_protect = False, overwrite = False, name = "x_position")
## @brief RTP x position.
#
# y                = task_share.Queue ('f', 8, thread_protect = False, overwrite = False, name = "y_position")
# ## @brief RTP y position.
# #
# v_x              = task_share.Queue ('f', 8, thread_protect = False, overwrite = False, name = "x_vel_fil")
# ## @brief RTP x velocity filtered.
# #
# v_y              = task_share.Queue ('f', 8, thread_protect = False, overwrite = False, name = "y_vel_fil")
# ## @brief RTP y velocity filtered.
# #
