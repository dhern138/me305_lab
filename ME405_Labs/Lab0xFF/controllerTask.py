# -*- coding: utf-8 -*-
''' @file       controllerTask.py
    @brief      Creates a file to run a motor and encoder that can track a given reference profile.
    @details    The class will use provided data to run motors at given speed and integrate simultaneously
                providing position data as well. 
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/controllerlTask.py
    @author     David Hernandez
    @author     Sydney Lewis
    @date       5/26/21
    @copyright  2020-2021 Hernandez & Lewis Inc.
'''

import shares
from closedLoop import closedLoop
import print_task


def controllerTask():
    ''' @brief      Function that will control all task operations for system model.
        @details    Will function with Nucleo board to collect data from all task sources. 
                    It will collect data via the encoderDriver, TouchDriver, 
                    MotorDriver, and closedLoop files. Data provided is position, speed,
                    duty cycle, and gain constant.
    '''
    # State constants
    S0_INIT                 = 0 # User PRESSES:
    # S1_ZERO_POSITION        = 1 # Z
    # S2_PRINT_ENC_POSITION   = 2 # P
    # S3_PRINT_ENC_DELTA      = 3 # D
    # S4_COLLECT_DATA         = 4 # G
    # S5_STOP_COLLECTING      = 5 # S
    S6_GET_ALPHA            = 6 # A
    S7_GET_BETA             = 7 # B
    S8_SET_ALPHA            = 8 # U
    S9_SET_BETA             = 9 # I
    # S10_CLR_ERROR           = 10 # E
    S11_BAL_BOARD           = 11 # F

    Cloop = closedLoop()
    def dot(v1, v2):
        ''' @brief    Calculates dot product of two vectors.
        '''
        return sum(x1*y1 for x1, y1 in zip(v1, v2))

    #######------- K, GAINS -------#########
    # K_gain = [-5.6397, -1.5532, -1.5159, -0.1299] # Our found data
    #K_charl=[-0.7658, -0.0847, -2.0736, -1.0889] # Charlies data
    K_gain = [-0.7658, -0.0847, -2.0736, -1.0889]
    ## @brief    Defined gains from homework solution
    #  @details  Unit order: N, N*m, N*s, N*m*s. Using a damping ratio of 0.8
    #            and a natural frequency of 10, and defined poles, these values
    #            were obtained.
    #

    print('Controller good')
    while True:

            state_vector_x = [shares.x,shares.encoderPosition.get(),shares.vx,shares.speed1Meas.get()]
            state_vector_y = [shares.y,shares.encoder2Position.get(),shares.vy,shares.speed2Meas.get()]
            ## state vector units: m, rad, m/s, rad/s
            
            K1 = dot(K_gain, state_vector_x)
            K2 = dot(K_gain, state_vector_y)

            #K=[-5.6397, -1.5532, -1.5159, -0.1299]
            ## @brief    Defined gains from homework solution
            #  @details  Unit order: N, N*m, N*s, N*m*s. Using a damping ratio of 0.8
            #            and a natural frequency of 10, and defined poles, these values
            #            were obtained.
            #

            ############ ------ Closed-Loop Full-State Feedback Set-Up -------#########
            ## Places the appropriate attributes to be used within the closed-loop class.
            shares.PWM1.put(Cloop.update(K1))
            ## @brief       Calls on closedLoop class to update the arguments in the constructor
             # @details     Sets the users gain values, motor's resistor, Vdc output and torque constant
             #              to output proper duty cycles for [x,theta_y,x_dot,theta_y_dot]
             # @param       K are the gains defined through the homework solution.
            #
            shares.PWM2.put(Cloop.update(K2))
            ## @brief       Calls on closedLoop class to update the arguments in the constructor
             # @details     Sets the users gain values, motor's resistor, Vdc output and torque constant
             #              to output proper duty cycles for [x,theta_y,x_dot,theta_y_dot]
             # @param       shares.speedRef is the user speed input
             # @param       shares.speed2Meas is the measured speed taken from encoder 2
             # @param       shares.pos2Ref is the integrated positional reference of the estimated speed reference.
             # @param       shares.encoder2Position provides most current encoder position value in [deg]
             # @param       shares.tref2 provides most current encoder position time difference in [s]
             # @param       K are the gains defined through the homework solution.
            #
            # shares.xk_1.put(Cloop.RTP_filter(shares.x.get(),shares.xk.get(),shares.vk_x.get(),shares.deltaTime.get()))
            # ## @brief       Calls on closed loop class to update arguments in RTP_filter.
            #  # @details     This helps increase resolution, increase stability and reduce noise in the readings.
            #  # @param       shares.x is the user speed input
            #  # @param       shares.xk is the measured speed taken from encoder 2
            #  # @param       shares.vk_x is the integrated positional reference of the estimated speed reference.
            #  # @param       shares.deltaTime provides amount of time between points [us]
            #  #
            # shares.yk_1.put(Cloop.RTP_filter(shares.y.get(),shares.yk.get(),shares.vk_y.get(),shares.deltaTime.get()))
            # ## @brief       Calls on closed loop class to update arguments in RTP_filter.
            #  # @details     This helps increase resolution, increase stability and reduce noise in the readings.
            #  # @param       shares.x is the user speed input
            #  # @param       shares.xk is the measured speed taken from encoder 2
            #  # @param       shares.vk_x is the integrated positional reference of the estimated speed reference.
            #  # @param       shares.deltaTime provides amount of time between points [us]
            #  #
            yield(0)

            ####----- FSM ------#####
            if shares.ENCODERstate == S0_INIT:
                pass
    
            # User pressed A, Change state and prints gain constant alpha [position gain]
            elif shares.ENCODERstate == S6_GET_ALPHA:
                shares.gain = Cloop.get_alpha()
                print_task.put('Gain Constant Alpha{:}:'.format(shares.alpha_gain))
                shares.ENCODERstate = S0_INIT
                
            # User pressed B, Change state and prints gain constant alpha [velocity gain]
            elif shares.ENCODERstate == S7_GET_BETA:
                shares.gain = Cloop.get_beta()
                print_task.put('Gain Constant Alpha{:}:'.format(shares.beta_gain))
                shares.ENCODERstate = S0_INIT
                
            # User pressed U, Change state and lets user set new alpha [position gain]
            elif shares.ENCODERstate == S8_SET_ALPHA:
                shares.alpha_gain.put(Cloop.set_alpha(float((input('Please input new Speed Gain between 0.0-1.0, try 0.85:')))))
                shares.ENCODERstate = S0_INIT
        
            # User pressed I, Change state and lets user set new beta [velocity gain]
            elif shares.ENCODERstate == S9_SET_BETA:
                shares.beta_gain.put(Cloop.set_beta(float((input('Please input new Speed Gain between 0.0-1.0, try 0.005:')))))
                shares.ENCODERstate = S0_INIT

            ####------ BOARD BALANCE PROCEDURE ------#######
            if shares.BACKstate == S0_INIT:
                pass
            elif shares.BACKstate == S11_BAL_BOARD:
                print_task('Please correct the board')
                shares.BACKstate == S0_INIT
            yield (0)
