''' @file       backEndTask.py
    @brief      This file communicates with UI front end.
    @details    Sends serially inputted values to all taskers via methods
                and the shares files.
    @author     David Hernandez
    @author     Sydney Lewis
    @date       6/8/21
    @copyright  2020-2021 Hernandez and Lewis Inc.
'''


import pyb
from pyb import UART
import shares
import print_task

pyb.repl_uart(None)

def backEndTask():
    ''' @brief      A task class that writes gathered motor speed and positional data to UI for processing.
        @details    When user interface commands are pressed, this task will work with other task
                    to gather any data user demands. Then using UART will write the data over to
                    the front end for processing.
    '''
    myuart = UART(2)
    ##@brief Establishes communication link to Nucleo
    #
    # State constants
    S0_INIT                 = 0 # User PRESSES:
    S1_ZERO_POSITION        = 1 # Z
    S2_PRINT_ENC_POSITION   = 2 # P
    S3_PRINT_ENC_SPEED      = 3 # D
    S4_COLLECT_DATA         = 4 # G
    S5_STOP_COLLECTING      = 5 # S
    S6_GET_ALPHA            = 6 # A
    S7_GET_BETA             = 7 # B
    S8_SET_ALPHA            = 8 # U
    S9_SET_BETA             = 9 # I
    S10_CLR_ERROR           = 10 # E
    S11_BAL_BOARD           = 11 # F


    def BACK_transitionTo(newState):
        """ @brief          Function that assists with the transition of UI_back_Task relates states
            @param          newState Input variable, new State that we want to move to.
        """
        print_task.put('S{:}->S{:}'.format(shares.BACKstate,newState)) 
        shares.BACKstate = newState 
        
    def ENCODER_transitionTo(newState):
        """ @brief          Function that assists with the transition of controllerTask related states
            @param          newState Input variable, new State that we want to move to.
        """
        print_task.put('S{:}->S{:}'.format(shares.ENCODERstate,newState))
        shares.ENCODERstate = newState 

    def keyboardInput():
        ''' @brief      Function that creates an instance for each key press
            @details    When a key is pressed the function calls on the respective
                        state and transitions to the appropriate FSM
        '''

        val = myuart.readchar()
        if val == 90 or val == 122:        # if input is 'Z' or 'z'
            ENCODER_transitionTo(1)
        elif val == 80 or val == 112:      # if input is 'P' or 'p'
            ENCODER_transitionTo(2)
        elif val == 68:# or val == 100:    # if input is 'D' or 'd'
            ENCODER_transitionTo(3)
        elif val == 71 or val == 103:      # if input is 'G' or 'g'
            print_task.put('Data Collection Started')
            BACK_transitionTo(4)
        elif val == 83 or val == 115:      # if input is 'S' or 's'
            BACK_transitionTo(5)
        elif val == 65 or val == 97:       # if input is 'A' or 'a'
            ENCODER_transitionTo(6)
        elif val == 66 or val == 98:       # if input is 'B' or 'b'
            ENCODER_transitionTo(7)
        elif val == 85 or val == 117:      # if input is 'U' or 'u'
            ENCODER_transitionTo(8)
        elif val == 73 or val == 105:      # if input is 'I' or 'i'
            ENCODER_transitionTo(9)
        elif val == 69 or val == 101:      # if input is 'E' or 'e'
            ENCODER_transitionTo(10)
        elif val == 70 or val == 102:      # if input is 'F' or 'f'
            ENCODER_transitionTo(11)
        else:
            pass

    # print_task.put('back-end linked')
    print('back good')
    while True:

            #######------- FSM ---------#######
            if shares.ENCODERstate == S0_INIT:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses Z, reset encoder position
            elif shares.ENCODERstate == S1_ZERO_POSITION:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses P, print out up-to-date encoder position
            elif shares.ENCODERstate == S2_PRINT_ENC_POSITION:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses D, print out encoder speed
            elif shares.ENCODERstate == S3_PRINT_ENC_SPEED:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses A, print alpha position gain
            elif shares.ENCODERstate == S6_GET_ALPHA:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses B, print beta velocity gain
            elif shares.ENCODERstate == S7_GET_BETA:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses U, set alpha position gain
            elif shares.ENCODERstate == S8_SET_ALPHA:
                if myuart.any() != 0:
                    keyboardInput()
        
        
            ## User presses I, set beta velocity gain
            elif shares.ENCODERstate == S9_SET_BETA:
                if myuart.any() != 0:
                    keyboardInput()
            
            if shares.BACKstate == S0_INIT:
                if myuart.any() != 0:
                    keyboardInput()
                    
            ## User presses G, Start Data collection
            elif shares.BACKstate == S4_COLLECT_DATA:
                if myuart.any() != 0:
                    keyboardInput()

            ## User presses S, Stop all systems
            elif shares.BACKstate == S5_STOP_COLLECTING:
                if myuart.any() != 0:
                    keyboardInput()

            ## User presses E, Fix fault error
            elif shares.BACKstate == S10_CLR_ERROR:
                if myuart.any() != 0:
                    keyboardInput()

            ## User presses F, Balance board
            elif shares.BACKstate == S11_BAL_BOARD:
                if myuart.any() != 0:
                    keyboardInput()

            else:
                pass
    
            yield(0)


