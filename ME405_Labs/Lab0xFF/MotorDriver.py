'''@file        MotorDriver.py
@brief          This file contains the class MotorDriver for use with the DRV8847 motor.
@details        This class allows the user to configure the pins, timer, and channel
                to an H-Bridge motor. The user can then create motor objects to test
                any motor.

                See source code here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/MotorDriver.py

@author         Sydney Lewis
@author         David Hernandez
@date           5/24/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

import pyb
import utime


class MotorDriver:
    ''' 
    @brief      A motor driver class that allows motor objects to be created.
    @details    This class uses four pin objects, two for the timer channel variables,
                two for each motor configured and a timer object. Using these it is 
                possible to configure a sleep pin, a fault pin. The driver software
                uses logic to interpolate the given duty command into the correct
                PWM percentage and direction. The driver also has the ability to enable 
                and disable motor motion.
    '''

    def __init__ (self, nSLEEP, nfault, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer, primary=False):
        '''
        @brief              Creates a motor driver by initializing GPIO
                            pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on
                            IN1_pin and IN2_pin. 
        @param nfault_pin   A pyb.Pin object used to connect to the motor fault pin, must be configured as an input
        @param debug        A boolean used to enable debug messages
        @param primary      A boolean used to set if an external interrupt should be created to listen for a fault.
        '''


        
        ## Sleep pin object
        self.nSleep = nSLEEP
        ## @brief        Locates sleep pin on board to enable and disable motor
        #
        self.disable()
        ## Fault pin object
        self.fault_pin = nfault
        ## @brief        Locates fault pin on H-Bridge. Allows clearance of faults.
        #

        self.mot_fault_flag = False
        ## @brief        Notifies driver if there is a fault by becoming True.
        #

        if primary == True:
            ## Motor fault ISR object
            self.MotFault_int = pyb.ExtInt(nfault, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.fault_CB)


        self.tch1 = timer.channel(IN1_ch, pyb.Timer.PWM, pin=IN1_pin)
        ## @brief       Sets up generic timer input to be called on by a task
        #  @details     Creates channel object for a first motor by configuring
        #               the pins and timer to be driven via PWM in half bridge 21.
        #

        self.tch2 = timer.channel(IN2_ch, pyb.Timer.PWM, pin=IN2_pin)
        ## @brief       Sets up generic timer input to be called on by a task
        #  @details     Creates channel object for a second motor by configuring
        #               the pins and timer to be driven via PWM in half bridge 2.
        #


    def enable (self):
        ''' @ brief     Turns on motor.
        '''


        self.MotFault_int.disable()
        self.nSleep.high()
        self.mot_fault_flag = False
        utime.sleep_us(100)
        self.MotFault_int.enable()
    
    def disable (self):
        ''' @brief      Turns off motor.
        '''

        self.nSleep.low()

    def set_duty (self, duty):
        ''' @brief      Sets motor duty.
            @ details   This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty A signed integer holding the duty
                        cycle of the PWM signal sent to the motor 
        '''

        
        self.duty = duty   
        if self.duty > 100:                             # Ensures the user cannot enter out of range PWM values
            self.duty = 100
            self.tch1.pulse_width_percent(self.duty)
        elif self.duty < -100:
            self.duty = -100
            self.tch2.pulse_width_percent(-1*self.duty)
        elif duty < -1:
            self.tch2.pulse_width_percent(-1*self.duty)
        else:
            self.tch1.pulse_width_percent(self.duty)


    def set_time(self, Time):
        ''' @brief     This method sets the time period to test the motor.
            @details   The user can set a test period from 1-10s.
            @param     Time; A user entered integer from 1-10 that sets the interval of 
                       testing time for motor operation.
        '''

        if Time > 10:
            print('Time period entered is greater than 10s, defaulting to 10s run time.') 
            self.set_duty(40)
            pyb.delay(10000)
            self.set_duty(0)
            
        elif Time < 1:
            print('Time period less than 1s, please enter a new value.')
            
        else:
            print('Time period set to {:} s\n'.format(Time))
            self.set_duty(40)
            pyb.delay(Time*1000)
            self.set_duty(0)

    def clear_fault(self):
        '''
        @brief       This method checks the motor driver for any failures and clears them.
        @details     This method checks if the motor is trying to be enabled by
                     any means and sends a True fault. This requires the user to
                     check the nfault pin and clear any faults to unlock the driver.
                     To prevent the software latch from sticking, the fault ISR
                     is disabled to allow the motor to power on without triggering
                     another momentary fault.
        '''

        while self.fault_pin.value() == 0:
            self.disable()
                


        
    def fault_CB(self,ISR_SOURCE):
        '''
        @brief     A callback method in case of a over-current condition.
        @details   To avoid motor damage from runaway motors
                   or PWM too high, a failsafe was required. 
                   This method will instantly disable the motors
                   to prevent any damage.
        @param     ISR_SOURCE  If motor fails this notifies this method
        '''

        self.disable()
        self.mot_fault_flag = True
        
# if __name__ == '__main__':
#         ''' Allows the user to test a configured motor using the MotorDriver class only within this file'''

#         from pyb import Pin, Timer
        
#         nSLEEP = Pin(Pin.cpu.A15, Pin.OUT_PP)
#         nSLEEP.low()
#         ## @brief       Locates respective pin on the Nucleo board
#         #  @details     Locates PinA15 for motor
#         #
#         ## The pin object used for the motor driver fault pin. Here the user-button is used
#         # to clear any faults from over-current condition.
#         nFAULT = Pin(Pin.board.PC13, Pin.IN)
#         ## @brief       Locates respective pin on the Nucleo board
#         #  @details     Locates PinB2 for motor
#         #
        
#         ##### ----- H-Bridge Pin Initialization ----- #####
        
#         IN1 = Pin(Pin.cpu.B4)
#         ## @brief       Locates respective pin on the Nucleo board for the motor.
#         #  @details     Locates PinB4 for motor and configure first input for motor 1.
#         #
#         IN2 = Pin(Pin.cpu.B5)
#         ## @brief       Locates respective pin on the Nucleo board for the motor.
#         #  @details     Locates PinB5 for motor and configure second input for motor 1.
#         #
        
#         IN3 = Pin(Pin.cpu.B0)
#         ## @brief       Locates respective pin on the Nucleo board for the motor.
#         #  @details     Locates PinB0 for motor and configure second input for motor 2.
#         #
#         IN4 = Pin(Pin.cpu.B1)
#         ## @brief       Locates respective pin on the Nucleo board for the motor.
#         #  @details     Locates PinB1 for motor and configure second input for motor 2.
#         #
#         ##### ----- Motor Initialization ----- #####
        
#         ## The timer object used for PWM generation
#         tim3 = Timer(3,freq=20000);
        
        
#         m1 = MotorDriver(nSLEEP, nFAULT, IN1, 1, IN2, 2, tim3, debug=True, primary=True)
#         ## @brief       Creates the first motor object.
#          # @details     Call on MotorDriver class to input the required arguments into constructor.
#          #              This configures all the required attributes to operate motor 1.
#          # @param       pin_nSlEEP enables and disables the motor.
#          # @param       pin_nFAULT Allows user to clear any faults via blue button on nucleo
#          # @param       1 Calls on channel 1
#          # @param       2 Calls on channel 2
#          # @param       pin_IN1 calls on pin PB4 to get into first pin channel
#          # @param       pin_IN2 calls on pin PB5 to get into second pin channel
#          # @param       3 is the timer being used for the pin and channel inputs
#          # @param       debug allows user to receive any errors if found.
#          # @param       primary allows external interrupt to work if true.
#         #
        
#         m2 = MotorDriver(nSLEEP, nFAULT, IN3, 3, IN4, 4, tim3)
#         ## @brief       Creates the second motor object.
#          # @details     Call on MotorDriver class to input the required arguments into constructor.
#          #              This configures all the required attributes to operate motor 2.
#          # @param       pin_nSlEEP enables and disables the motor.
#          # @param       pin_nFAULT Allows user to clear any faults via blue button on nucleo
#          # @param       3 Calls on channel 1
#          # @param       4 Calls on channel 2
#          # @param       pin_IN3 calls on pin PB4 to get into first pin channel
#          # @param       pin_IN4 calls on pin PB5 to get into second pin channel
#          # @param       3 is the timer being used for the pin and channel inputs
#          # @param       debug allows user to receive any errors if found.
#          # @param       primary allows external interrupt to work if true.
#         #
#         m1.enable()
#         while True:

#             ## The type of test desired
#             test_type = input('Which test would you like to perform? Timed, motion, duty, or fault? (t/m/d/f): ')
            
#             ## The motor selected for testing
#             test_mot = input('Which motor? (1/2): ')
    
#             if test_type == 't':
#                 print('Timed Test')
#                 if test_mot == '2':
#                     print('Testing Motor 2')
#                     Time = input('Enter a time period to run the motor (1-10) seconds: ')
#                     while True:
#                        m2.set_time(int(Time))
#                        input('Press enter to test again: ')
#                 else:
#                     print('Testing Motor 1')
#                     while True:
#                          Time = input('Enter a time period to run the motor (1-10) seconds: ')
#                          m1.set_time(int(Time))
#                          input('Press enter to test again: ')
                       
#             elif test_type == 'f':
#                 print('Fault Test... \nPress or hold the blue button to simulate a fault')
                
#                 try:
#                     while True:
#                         m1.set_duty(40)
#                         m2.set_duty(40)
#                         pyb.delay(1000)
#                 except:
#                     m1.set_duty(0)
#                     m2.set_duty(0)
                
#             elif test_type == 'd':
#                 print('Running Duty Cyle Test for Five Seconds')
#                 if test_mot == '2':
#                     print('Testing Motor 2')
#                     while True:
#                         duty = input('Enter a percentage (0-100): ')
#                         m2.set_duty(int(duty))
#                         pyb.delay(5000)
#                         m2.set_duty(0)
#                 else:
#                     print('Testing Motor 1')
#                     while True:
#                         duty = input('Enter a percentage (0-100): ')
#                         m1.set_duty(int(duty))
#                         pyb.delay(5000)
#                         m1.set_duty(0)
#             else:
#                 print('Motion Test')
#                 if test_mot == '2':
#                         print('Testing Motor 2')
    
#                     # while True:
#                         input('Ready... Press enter to run the motor')
#                         print('Forward')
#                         m2.set_duty(40)
                        
#                         pyb.delay(1500)
                        
#                         print('Stop')
#                         m2.set_duty(0)
                        
#                         pyb.delay(1500)
                    
#                         print('Reverse')
#                         m2.set_duty(-40)
                        
#                         pyb.delay(1500)
                        
#                         print('Stop')
#                         m2.set_duty(0)
#                 else:
#                     print('Testing Motor 1')
#                     while True:
#                         input('Ready... press enter to run motor')
#                         print('Forward')
#                         m1.set_duty(40)
                        
#                         pyb.delay(1500)
                        
#                         print('Stop')
#                         m1.set_duty(0)
                        
#                         pyb.delay(1500)
                    
#                         print('Reverse')
#                         m1.set_duty(-40)
                        
#                         pyb.delay(1500)
                        
#                         print('Stop')
#                         m1.set_duty(0)