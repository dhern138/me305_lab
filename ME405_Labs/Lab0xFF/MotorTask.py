'''@file        MotorTask.py
@brief          This file contains the task to run the DRV8847 motor.
@details        The user can then create motor objects to test
                any motor by configure the pins, timer, and channel.

                See source code here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/MotorTask.py

@author         Sydney Lewis
@author         David Hernandez
@date           5/20/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

from MotorDriver import MotorDriver
from pyb import Pin, Timer
import print_task
import shares



def MotorTask ():
    """ @brief    This file inputs the duty cycle for each motor.
        @details  Puts computed duty cycle over from the controller's
                  Full-state feedback.
    """


    #####----- State constants ----#####
    S0_INIT                 = 0 # User PRESSES:
    # S1_ZERO_POSITION        = 1 # Z
    # S2_PRINT_ENC_POSITION   = 2 # P
    # S3_PRINT_ENC_SPEED      = 3 # D
    # S4_COLLECT_DATA         = 4 # G
    # S5_STOP_COLLECTING      = 5 # S
    # S6_GET_ALPHA            = 6 # A
    # S7_GET_BETA             = 7 # B
    # S8_SET_ALPHA            = 8 # U
    # S9_SET_BETA             = 9 # I
    S10_CLR_ERROR           = 10
    
    ## The pin object used for the H-bridge sleep pin
    nSLEEP = Pin(Pin.cpu.A15, Pin.OUT_PP)
    nSLEEP.low()
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinA15 for motor
    #
    ## The pin object used for the motor driver fault pin. Here the user-button is used
    # to clear any faults from over-current condition.
    nFAULT = Pin(Pin.board.PB2, Pin.IN)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB2 for motor
    #
    
    ##### ----- H-Bridge Pin Initialization ----- #####
    
    IN1 = Pin(Pin.cpu.B4)
    ## @brief       Locates respective pin on the Nucleo board for the motor.
    #  @details     Locates PinB4 for motor and configure first input for motor 1.
    #
    IN2 = Pin(Pin.cpu.B5)
    ## @brief       Locates respective pin on the Nucleo board for the motor.
    #  @details     Locates PinB5 for motor and configure second input for motor 1.
    #
    
    IN3 = Pin(Pin.cpu.B0)
    ## @brief       Locates respective pin on the Nucleo board for the motor.
    #  @details     Locates PinB0 for motor and configure second input for motor 2.
    #
    IN4 = Pin(Pin.cpu.B1)
    ## @brief       Locates respective pin on the Nucleo board for the motor.
    #  @details     Locates PinB1 for motor and configure second input for motor 2.
    #
    ##### ----- Motor Initialization ----- #####
    
    ## The timer object used for PWM generation
    tim3 = Timer(3,freq=20000);
    
    
    m1 = MotorDriver(nSLEEP, nFAULT, IN1, 1, IN2, 2, tim3, primary=True)
    ## @brief       Creates the first motor object.
     # @details     Call on MotorDriver class to input the required arguments into constructor.
     #              This configures all the required attributes to operate motor 1.
     # @param       pin_nSlEEP enables and disables the motor.
     # @param       pin_nFAULT Allows user to clear any faults via blue button on nucleo
     # @param       1 Calls on channel 1
     # @param       2 Calls on channel 2
     # @param       pin_IN1 calls on pin PB4 to get into first pin channel
     # @param       pin_IN2 calls on pin PB5 to get into second pin channel
     # @param       3 is the timer being used for the pin and channel inputs
     # @param       deadband allows the user to put input to overcome motor static friction
     # @param       debug allows user to receive any errors if found.
     # @param       primary allows external interrupt to work if true.
    #
    
    m2 = MotorDriver(nSLEEP, nFAULT, IN3, 3, IN4, 4, tim3)
    ## @brief       Creates the second motor object.
     # @details     Call on MotorDriver class to input the required arguments into constructor.
     #              This configures all the required attributes to operate motor 2.
     # @param       pin_nSlEEP enables and disables the motor.
     # @param       pin_nFAULT Allows user to clear any faults via blue button on nucleo
     # @param       3 Calls on channel 1
     # @param       4 Calls on channel 2
     # @param       pin_IN3 calls on pin PB4 to get into first pin channel
     # @param       pin_IN4 calls on pin PB5 to get into second pin channel
     # @param       3 is the timer being used for the pin and channel inputs
     # @param       deadband allows the user to put input to overcome motor static friction
     # @param       debug allows user to receive any errors if found.
     # @param       primary allows external interrupt to work if true.
    #

    m1.enable()
    # print_task.put('Motors Initialized')
    print('motor good')

    while True:

            m1.set_duty(shares.PWM1.get())
            m2.set_duty(shares.PWM2.get())
            print(shares.PWM1.get())
            print(shares.PWM1.get())


            ####---- Fault Clear -----######
            if nFAULT.value() == 0:
                MotorDriver.clear_fault()
                # print_task.put('Check motors, press E when done')
                print('Check Motors')
                if shares.BACKstate == S10_CLR_ERROR:
                    MotorDriver.enable()
                    print_task.put('Error cleared')
                    shares.BACKstate = S0_INIT
            yield(0)

