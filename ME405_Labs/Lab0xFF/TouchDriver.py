'''@file        TouchDriver.py
@brief          This scripts performs the driver for a resistive touch panel(RTP).
@details        This driver class allows the set up for similar resistive touch
                panels. It scans X and Y on a RTP as well Z, to verify that contact
                has been made.
                It is important to note that works from Peter Hinch, retired hardware
                and firmware developer, was used for the filter and settling period 
                of the following driver.

                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/TouchDriver.py

@author         Sydney Lewis
@author         David Hernandez
@date           5/14/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''


from pyb import ADC
from pyb import Pin
from avg import avg
from fir import fir
from array import array


class TouchDriver:
    '''
    @brief      This class incoporates the required methods and objects for a touch panel.
    @details    This class builds the driver for a resistive touch pannel. It will allow
                scans of the X,Y, and Z components on the touch panel. A settling period 
                filter were also added for more accurate measurements. This was done by
                creating a delay and FIR filtering, respectively.
    '''

    OUT_PP = Pin.OUT_PP
    ##@brief    Initiates the pins with the pull-out
    #
    IN = Pin.IN
    ##@brief    Initiates the pins into cpu
    #

    def __init__(self, PIN_ym, PIN_xm, PIN_yp, PIN_xp, panel_xy, panel_cntr, ID=1, filt='none', debug=False):
        '''
        @brief              This constructor contains the parameters necessary to create resistive touch panel objects. 
        @details            This constructor creates the necessary input arguments to set up any RTP and the following:
                            its width and height dimensions, the panel's center, a way to identify the RTP, and a filter
                            to reduce outside noise.
        @param PIN_xm       The Pin object connected to the xm terminal on the RTP.
        @param PIN_xp       The Pin object connected to the xp terminal on the RTP.
        @param PIN_ym       The Pin object connected to the ym terminal on the RTP.
        @param PIN_yp       The Pin object connected to the yp terminal on the RTP.
        @param panel_xy     This parameter provides the width and height dimensions of the panel.
        @param panel_cntr   This parameter provides center of the panel by taking half of the width and height.
        @param ID           A variable to identify the RTP being used.
        @param filt         A string describing the desired type of running filtering (none, avg, fir)
        @param debug        A debug toggle output.
        '''

                                ###### ----- Panel Setup ----- ######

        self.xm = PIN_xm
        ##@brief    Creates the pin object for the negative terminal on the x axis of the RTP.
        #
        self.ADC_xm = ADC(self.xm)
        ##@brief    Creates an ADC output on the negative terminal on the x axis of the RTP.
        #
        self.xp = PIN_xp
        ##@brief    Creates the pin object for the positive terminal on the x axis of the RTP.
        #
        self.ym = PIN_ym
        ##@brief    Creates the pin object for the negative terminal on the y axis of the RTP.
        #
        self.ADC_ym = ADC(self.ym)
        ##@brief    Creates an ADC output on the negative terminal on the y axis of the RTP.
        #
        self.yp = PIN_yp
        ##@brief    Creates the pin object for the positive terminal on the y axis of the RTP.
        #
        self.x_xy = panel_xy[0]
        ##@brief    Takes input argument for the width of the RTP in [mm].
        #
        self.y_xy = panel_xy[1]
        ##@brief    Takes input argument for the height of the RTP in [mm].
        #
        self.x_ctr = panel_cntr[0]
        ##@brief    Takes input argument for the width center of the RTP in [mm].
        #
        self.y_ctr = panel_cntr[1]
        ##@brief    Takes input argument for the height center of the RTP in [mm].
        #

                                ###### ------Calibration-------- ######

        self.cal_x = (200,3800)
        ##@brief    Uses ADC to gather current x positional readings.
        # @details  Takes ADC readings from tapped location to be used in future calcs.
        #
        self.cal_y = (380,3600)
        ##@brief    Uses ADC to gather current y positional readings.
        # @details  Takes ADC readings from tapped location to be used in future calcs.
        #
        self.conv_x = self.x_xy/(self.cal_x[1]-self.cal_x[0])
        ##@brief    Calibrates the x position
        # @details  Considers the panels x axis center and ADC count readings to 
        #           convert to an absolute x axis position in [mm]
        #
        self.conv_y = self.y_xy/(self.cal_y[1]-self.cal_y[0])
        ##@brief    Calibrates the y position
        # @details  Considers the panels y axis center and ADC count readings to 
        #           convert to an absolute y axis position in [mm]
        #

                                ##### ----- Averaging / Filtering ----- #####

        ## A string describing the type of filtering performed can be 'none' 'avg' or 'fir'
        self.filt = filt
        ## @brief    Arguments for type of filer to use.
        #  @details  Using Peter Hinch's works for filtering it allows the user to 
        #            choose the specific type of filtering of their choosing.
        #            It is possible to do an average 'avg', 'fir', or no filter at all.
        #

                                ## --- AVG ---##
        self.navg = 5
        ## @brief    Integer values for amount of averages taken for signal rejection.
        #

        self.zavg_buf = array('i',[0]*(self.navg+3))
        self.zavg_buf[0] = len(self.zavg_buf)
        ## @brief   This calls on Peter Hinch's avg library.
        #  @details Performs a running average buffer for the Z component.
        #
        self.zavg = 0
        ## @brief    A class attribute that tracks the running average for Z.
        #

        self.xavg_buf = array('i',[0]*(self.navg+3))
        self.xavg_buf[0] = len(self.xavg_buf)
        ## @brief   This calls on Peter Hinch's avg library.
        #  @details Performs a running average buffer for the X component.
        #
        self.xavg = 0
        ## @brief    A class attribute that tracks the running average for Z.
        #

        self.yavg_buf = array('i',[0]*(self.navg+3))
        self.yavg_buf[0] = len(self.yavg_buf)
        ## @brief   This calls on Peter Hinch's avg library.
        #  @details Performs a running average buffer for the Y component.
        #
        self.yavg = 0
        ## @brief    A class attribute that tracks the running average for Z.
        #
                                ## --- FIR ---##
        ## The FIR filter coefficents designed using TFilter
        self.coeffs = array('i', (-124,-38,-12,46,143,285,474,708,982,1285,1602,1917,2210,2462,2656
                                  ,2779,2821,2779,2656,2462,2210,1917,1602,1285,982,708,474,285,143,46
                                  ,-12,-38,-124))
        
        ## The numebr of coefficents
        ncoeffs = len(self.coeffs)
        
        ## Z FIR filter buffer for use with the fir library created by Peter Hinch
        self.zfilt_buf = array('i',[0]*int(ncoeffs+3))
        self.zfilt_buf[0] = ncoeffs
        self.zfilt_buf[1] = 13
        
        ## X FIR filter buffer for use with the fir library created by Peter Hinch
        self.xfilt_buf = array('i',[0]*int(ncoeffs+3))
        self.xfilt_buf[0] = ncoeffs
        self.xfilt_buf[1] = 13
        
        ## Y FIR filter buffer for use with the fir library created by Peter Hinch
        self.yfilt_buf = array('i',[0]*int(ncoeffs+3))
        self.yfilt_buf[0] = ncoeffs
        self.yfilt_buf[1] = 13
        
        
                        ##### ----- Debug ----- #####
        
        ## A string used for identifying the object
        self.ID = 'RTP_'+str(ID)
        
        ## A boolean that toggles the trace output to the REPL
        self.debug = debug
        
        if self.debug:
            print('INIT RTP object: '+ self.ID)


                        ##### ----- Pin Initialization ----- #####
        self.xm.init(mode=self.OUT_PP,value=0)
        ##@brief    Initializes the 'push-pull' output and low for negative terminal of x axis.
        #
        self.yp.init(mode=self.OUT_PP,value=1)
        ##@brief    Initializes the 'push-pull' output as high for positive terminal of y axis.
        #
        self.ADC_ym = ADC(self.ym)
        self.xp.init(mode=self.IN)

        self.last_read = 0
        ## @brief    Defines the last read coordinate where 0,1,2 are z,x,and y, respectively.
        #

    def scan_z(self):
        ''' @brief      This method checks for RTP contact.
            @details    This method checks if the x and y pins are energized.
                        If they are. then a boolean of "1" is returned, designating
                        to the user that contact with the RTP has been made. Otherwise
                        a "0" boolean will be returned if no contact is detected.
        '''
        
        ## Takes adc count from y axis
        if self.last_read == 1:
            self.yp.init(mode=self.OUT_PP,value=1)
            self.xp.init(mode=self.IN)

        ## Taks adc count from x axis
        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            self.ADC_ym = ADC(self.ym)
            self.xp.init(mode=self.IN)

        z = self.ADC_ym.read()
        ## @brief    Considers the readings from the ADC counts and
        #            calculates if the pins have been energized.
        #
        self.last_read = 0
        
        # If z ADC count is not within range of detection it will remain high
        if z > 4000:
            z = 0
        else:
            z = 1

        if self.debug:
            print('Z ADC count Last was {:}'.format(self.last_read))

        return z

    def scan_x(self, mode='ctr'):
        ''' @brief       Scans for position change on x-axis.
            @details     This method energizes the xm and xp resistors, floats
                         the yp pin and measures the voltage at ym. This locates
                         the read ADC value for the x position and returns the 
                         value as a float.
            @param       mode, this param designates which mode to use either 'ctr' 'org' or 'count'
        '''
        
        # Takes ADC readings from energized resistors and returns high or low value.
        if self.last_read == 0:
            self.xp.init(mode=self.OUT_PP,value=1)
            self.yp.init(mode=self.IN)

        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            self.xp.init(mode=self.OUT_PP,value=1)
            self.ADC_ym = ADC(self.ym)
            self.yp.init(mode=self.IN)


        # Read the x-coordinate value
        x = self.ADC_ym.read()
        self.last_read = 1

        if mode == 'ctr':
            x = (self.conv_x*(self.ADC_ym.read()-self.cal_x[0]))-(self.x_ctr)
        elif mode == 'org':
            x = (self.conv_x*(self.ADC_ym.read()-self.cal_x[0]))
        else:
            pass

        if self.debug:
            print('X ADC count Last was {:}'.format(self.last_read))

        return x

    def scan_y(self, mode='ctr'):
        ''' @brief       Scans for position change on y-axis.
            @details     This method energizes the ym and yp resistors, floats
                         the xp pin and measures the voltage at xm. This locates
                         the read ADC value for the y position and returns the 
                         value as a float.
            @param       mode, this param designates which mode to use either 'ctr' 'org' or 'count'
        '''
        
        # Takes ADC readings from energized resistors and returns high or low value.
        if self.last_read == 0:
            self.ym.init(mode=self.OUT_PP,value=0)
            self.ADC_xm = ADC(self.xm)
            self.xp.init(mode=self.IN)


        elif self.last_read == 1:
            self.ym.init(mode=self.OUT_PP,value=0)
            self.yp.init(mode=self.OUT_PP,value=1)
            self.ADC_xm = ADC(self.xm)
            self.xp.init(mode=self.IN)

        # Read the x-coordinate value
        y = self.ADC_xm.read()
        self.last_read = 2

        if mode == 'ctr':
            y = (self.conv_y*(self.ADC_xm.read()-self.cal_y[0]))-(self.y_ctr)
        elif mode == 'org':
            y = (self.conv_y*(self.ADC_xm.read()-self.cal_y[0]))
        elif mode == 'count':
            pass

        if self.debug:
            print('Y ADC count Last was {:}'.format(self.last_read))

        return y

    def scan(self,mode='ctr',avg_type='none'):
        ''' @brief       This will check x,y, and z as quickly as possible.
            @details     This method runs the three previous scan methods below 1500 [us]
                         and displays a tuple of the three coordinates in (x,y,z) for user
                         viewability. The values for x and y are in [mm] and z will return
                         a boolean of "1" or "0" if contacted or not, respectively. The point
                         of reference is the designated center of the user.
            @param       mode, this param designates which mode to use either 'ctr' 'org' or 'count'
            @param       avg_type A string describing the batch averaging performed on each sample 'none' '5' or '10'
        '''

        # Takes ADC readings from energized resistors and returns high or low value.
        if self.last_read == 1:
            self.yp.init(mode=self.OUT_PP,value=1)
            self.xp.init(mode=self.IN)
            
        elif self.last_read == 2:
            self.xm.init(mode=self.OUT_PP,value=0)
            self.ADC_ym = ADC(self.ym)
            self.xp.init(mode=self.IN)

        # Uses scan_z() method
        if avg_type == 'ten' or avg_type == 10:
            z0 = self.ADC_ym.read()
            z1 = self.ADC_ym.read()
            z2 = self.ADC_ym.read()
            z3 = self.ADC_ym.read()
            z4 = self.ADC_ym.read()
            z5 = self.ADC_ym.read()
            z6 = self.ADC_ym.read()
            z7 = self.ADC_ym.read()
            z8 = self.ADC_ym.read()
            z9 = self.ADC_ym.read()
            z = (z0 + z1 + z2 + z3 + z4 + z5 + z6 + z7 + z8 + z9)/10
        elif avg_type == 'five' or avg == 5:
            z0 = self.ADC_ym.read()
            z1 = self.ADC_ym.read()
            z2 = self.ADC_ym.read()
            z3 = self.ADC_ym.read()
            z4 = self.ADC_ym.read()
            z = (z0 + z1 + z2 + z3 + z4)/5
        else:
            z = self.ADC_ym.read()
        
        self.zavg = avg(self.zavg_buf,z)
        
        if (z-self.zavg) > 500 or (z-self.zavg) < -500:
            z = self.zavg
        
        if self.filt == 'avg':    
            z = self.zavg
        elif self.filt == 'fir':
            z = fir(self.zfilt_buf,self.coeffs,z)*(.22026)
        else:
            pass
            
        if z > 4000:
            z=0
        else:
            z = 1
        
        # Uses scan_y() method
        if z == 1:

            self.ym.init(mode=self.OUT_PP,value=0)
            self.ADC_xm = ADC(self.xm)
    
            if avg_type == 'ten' or avg_type == 10:
                y0 = self.ADC_xm.read()
                y1 = self.ADC_xm.read()
                y2 = self.ADC_xm.read()
                y3 = self.ADC_xm.read()
                y4 = self.ADC_xm.read()
                y5 = self.ADC_xm.read()
                y6 = self.ADC_xm.read()
                y7 = self.ADC_xm.read()
                y8 = self.ADC_xm.read()
                y9 = self.ADC_xm.read()
                y = (y0 + y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9)/10
    
            elif avg_type == 'five' or avg_type == 5:
                y0 = self.ADC_xm.read()
                y1 = self.ADC_xm.read()
                y2 = self.ADC_xm.read()
                y3 = self.ADC_xm.read()
                y4 = self.ADC_xm.read()
                y = (y0 + y1 + y2 + y3 + y4)/5
    
            else:
                y = self.ADC_xm.read()
            
            self.yavg = avg(self.yavg_buf,y)
            
            if (y-self.yavg) > 100 or (y-self.yavg) < -100:
                y = self.yavg
    
            if self.filt == 'avg':    
                y = self.yavg
            elif self.filt == 'fir':
                y = fir(self.yfilt_buf,self.coeffs,y)*(.22026)
            else:
                pass
    
            # Uses scan_x() method
            self.xm.init(mode=self.OUT_PP,value=0)
            self.xp.init(mode=self.OUT_PP,value=1)
            self.ADC_ym = ADC(self.ym)
            self.yp.init(mode=self.IN)
             
            if avg_type == 'ten' or avg_type == 10:
                x0 = self.ADC_ym.read()
                x1 = self.ADC_ym.read()
                x2 = self.ADC_ym.read()
                x3 = self.ADC_ym.read()
                x4 = self.ADC_ym.read()
                x5 = self.ADC_ym.read()
                x6 = self.ADC_ym.read()
                x7 = self.ADC_ym.read()
                x8 = self.ADC_ym.read()
                x9 = self.ADC_ym.read()
                x = (x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9)/10
            elif avg_type == 'five' or avg_type == 5:
                x0 = self.ADC_ym.read()
                x1 = self.ADC_ym.read()
                x2 = self.ADC_ym.read()
                x3 = self.ADC_ym.read()
                x4 = self.ADC_ym.read()
                x = (x0 + x1 + x2 + x3 + x4)/5
            else:
                x = self.ADC_ym.read()
                
            self.xavg = avg(self.xavg_buf,x)
                
            if (x-self.xavg) > 500 or (x-self.xavg) < 500:
                x = self.xavg
            
            if self.filt == 'avg':    
                x = self.xavg
            elif self.filt == 'fir':
                x = fir(self.xfilt_buf,self.coeffs,x)*(.22026)
            else:
                pass
            
            if mode == 'ctr':
                y = (self.conv_y*(y-self.cal_y[0]))-(self.y_ctr)
                x = (self.conv_x*(x-self.cal_x[0]))-(self.x_ctr)
            elif mode == 'org':
                y = (self.conv_y*(y-self.cal_y[0]))
                x = (self.conv_x*(x-self.cal_x[0]))
            else:
                pass
        else:
            y = 0
            x = 0
            pass
    
        if self.last_read != 1:
            self.last_read = 1
            

        return (x, y, z)


# if __name__ == '__main__':
#     ## The following code allows user to interact with touch panel.
#     #
#     from pyb import Pin
#     import utime

#     PIN_ym = Pin(Pin.cpu.A0)
#     ## @brief    Associates ym to pinA0 on NucleoSTM32
#     #
#     PIN_xm = Pin(Pin.cpu.A1)
#     ## @brief    Associates xm to pinA1 on NucleoSTM32
#     #
#     PIN_yp = Pin(Pin.cpu.A6)
#     ## @brief    Associates yp to pinA6 on NucleoSTM32
#     #
#     PIN_xp = Pin(Pin.cpu.A7)
#     ## @brief    Associates xp to pinA7 on NucleoSTM32
#     #
#     TouchPanel = TouchDriver(PIN_ym,PIN_xm,PIN_yp,PIN_xp,(176,100),(88,50),debug=False)
#     ##@brief    The touch panel object created to work touchdriver class.
#     # @details  Calls on the touch driver class to input arguments into the attributes
#     #           which will be used within the class driver code. Each parameter is discussed
#     #           in the driver. Please look at driver code for more information on each param.
#     #

#     while True:

#         sel_test = input('Running or point test? (r/p): ')
#         ## @brief    Allows user to select whether to test points or while running
#         #
#         sel_axis = input('Which axis would you like to test (X Y Z ALL): ') 
#         ## @brief    Allows user to select which axis or axes to test.
#         #
#         try:
            
#             if sel_test == 'p':
#                 if sel_axis == 'X' or sel_axis == 'x':
#                     while True:
#                         start = utime.ticks_us()
#                         x = TouchPanel.scan_x()
#                         ##@brief    Calls on touch driver class and its method.
#                         #
#                         stop = utime.ticks_us()
#                         ## Test run time in us
#                         run_time = utime.ticks_diff(stop,start)
#                         print('Contact coordinate X = '+str(x)+' mm in '+str(run_time)+' us\n')
#                         input('     Press enter to test again: ')
                        
#                 elif sel_axis == 'y' or sel_axis == 'Y':
#                     while True:
#                         start = utime.ticks_us()
#                         y = TouchPanel.scan_y()
#                         ##@brief    Calls on touch driver class and its method.
#                         #
#                         stop = utime.ticks_us()
#                         run_time = utime.ticks_diff(stop,start)
#                         print('Contact coordinate Y = '+str(y)+' mm in '+str(run_time)+' us\n')
#                         input('     Press enter to test again: ')
                        
#                 elif sel_axis == 'Z' or sel_axis == 'z':
#                     while True:
#                         start = utime.ticks_us()
#                         z = TouchPanel.scan_z()
#                         ##@brief    Calls on touch driver class and its method.
#                         #
#                         stop = utime.ticks_us()
#                         run_time = utime.ticks_diff(stop,start)
#                         print('Contact Test Z = '+str(z)+' mm in '+str(run_time)+' us\n')
#                         input('     Press enter to test again: ')
                        
#                 else:
#                      while True:
#                         start = utime.ticks_us()
#                         pos = TouchPanel.scan()
#                         ##@brief    Calls on touch driver class and its method.
#                         # @details  This allows user to test and observe the
#                         #           three components at once.
#                         #
#                         stop = utime.ticks_us()
#                         run_time = utime.ticks_diff(stop,start)
#                         print('Contact coordinates ({:},{:},{:}) [mm] in {:} us\n'.format(pos[0],pos[1],pos[2],run_time))
#                         input('     Press enter to test again: ')
                    
#             else:
#                 if sel_axis == 'X' or sel_axis == 'x':
#                     print('x-axis test')
#                     while True:
#                         x = TouchPanel.scan_x(mode='ctr')
#                         print('X: {:} mm'.format(x))
#                         utime.sleep_ms(250)
                        
                        
#                 elif sel_axis == 'y' or sel_axis == 'Y':
#                     print('y-axis test')
#                     while True:
#                         y = TouchPanel.scan_y(mode='ctr')
#                         print('Y: {:} mm'.format(y))
#                         utime.sleep_ms(250)
                        
#                 elif sel_axis == 'Z' or sel_axis == 'z':
#                     print('Contact test')
#                     while True:
#                         z = TouchPanel.scan_z()
#                         print('Z: {:} mm'.format(z))
                        
#                 else:
#                     print('Selected All Axes')
#                     while True:
#                        pos = TouchPanel.scan(mode='ctr')
#                        print('Contact coordinates ({:},{:},{:}) [mm]'.format(pos[0],pos[1],pos[2]))
#                        utime.sleep_ms(250)
            
#         except KeyboardInterrupt:
#             # This except block catches "Ctrl-C" from the keyboard
#             # to end the while(True) loop when desired
#             print('Ctrl+c has been pressed, program ended. Goodbye')
#             break