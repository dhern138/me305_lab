'''@file        TouchPanelTask.py
@brief          This program creates a resistive touch panel (RTP) object from the developed driver.
@details        This task allows the user to create an RTP object from the driver class TouchDriver developed
                beforehand. The RTP object can be used to test, calibrate, and gather required positional
                information from the x and y axes. This data is crucial for the overall system model.

                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/TouchPanelTask.py

@author         Sydney Lewis
@author         David Hernandez
@date           5/13/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

from TouchDriver import TouchDriver
from pyb import Pin
import utime
import shares

def TouchPanelTask():
    ''' @brief    This function continuously provides the RTP coordinates.
        @details  Configures the appropriates x,y terminals of the RTP in
                  order to output the local coordinates of the rubberized
                  ball. This will be crucial in keeping the ball in the center.
    '''
    
    PIN_ym = Pin(Pin.cpu.A0)
    ## @brief    Associates ym to pinA0 on NucleoSTM32
    #
    PIN_xm = Pin(Pin.cpu.A1)
    ## @brief    Associates xm to pinA1 on NucleoSTM32
    #
    PIN_yp = Pin(Pin.cpu.A6)
    ## @brief    Associates yp to pinA6 on NucleoSTM32
    #
    PIN_xp = Pin(Pin.cpu.A7)
    ## @brief    Associates xp to pinA7 on NucleoSTM32
    #


    TouchPanel = TouchDriver(PIN_ym,PIN_xm,PIN_yp,PIN_xp,(176,100),(88,50),debug=False)
    ##@brief    The touch panel object created to work touchdriver class.
    # @details  Calls on the touch driver class to input arguments into the attributes
    #           which will be used within the class driver code. Each parameter is discussed
    #           in the driver. Please look at driver code for more information on each param.
    #
    x = 0
    y = 0
    vx = 0
    vy = 0
    print('RTP good')
    while True:

        if x and y != 0:
            start = utime.ticks_us()
            scan = TouchPanel.scan(mode='ctr',avg_type='fir')
            x = scan[0]*1e-3  # m
            y = scan[1]*1e-3  # m
            stop = utime.ticks_us()
            run_time = utime.ticks_diff(stop,start)
            x = (scan[0]*1e-3) - x
            y = (scan[1]*1e-3) - y
            vx = x*1e3/run_time # m/sec
            vy = y*1e3/run_time # m/sec
            shares.x = x
            shares.y = y
            shares.vx = vx
            shares.vy = vy
            # print('x',x)
            # print('vx',vx)
            # print('y',y)
            # print('vy',vy)
            # print('Time',run_time)
            # utime.sleep_ms(250)
        else:
            pass
            #print('X, Y, Z ({:},{:},{:}) [mm] in {:} us\n'.format(scan[0],scan[1],scan[2],run_time))
        yield(0)

