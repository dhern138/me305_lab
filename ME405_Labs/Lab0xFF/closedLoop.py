# -*- coding: utf-8 -*-
''' @file       closedLoop.py
    @brief      This class updates and calculates 2 factors into power applied to motor.
    @details    The controllerTask will call on this class to operate motors
                The gain position and velocity, and measured speed will continuously update
                the controllerTask with the new power output, until desired. 
                speed is achieved.
    @author     David Hernandez
    @author     Sydney Lewis
    @date       5/26/21
    @copyright  2020-2021 Hernandez & Lewis Inc.
'''


class closedLoop:
    ''' @brief      This class will establish the power output and speed of a motor with alpha and beta modifiers.
        @details    The alpha modifier is the position gain of the system, the beta modifier is the velocity gain
                    of the system. Both behave as filters to stability and reduce noise.
    '''

    def __init__(self):
        ''' @brief      This function established all methods required for motor
                        operation.
        '''
        
        ## Motor output initialization
        self.Pmax         = 100                      # Max PWM
        self.Pmin         = -100                     # Min PWM
        self.PWM          = 0                       # Current PWM
        self.alpha_gain   = 0.85                     # Position gain constant
        self.beta_gain    = 0.005                    # Velocity gain constant
        self.X_k          = 0                        # Position filter equation [deg]
        self.V_k          = 0                        # Velocity filter equation [deg/sec]
        self.R            = 2.21                     # Terminal Resistance of DCX22S [OHMS]
        self.Vdc          = 12                       # DC motor voltage supplied [Volts]
        self.Kt           = 0.0138                   # Torque constant,divide by 1000 for conversion [N*m/A]


    def update(self, K_gains):
        ''' @brief      This function will calculated and update PWM level.
            @details    This function takes the defined gain values and iterates
                        through each one to provide proper power outputs.
            @param      K_gains are the defined gain values from the homework solution
            
        '''
        self.K_gains = K_gains
        ####-------- Motor Duty cycle using provided K gain values ------- #######
        ## Duty cycle in math: [%*ohms/(N/A)*V]*[N] == [%]
        self.P_new = -(100*self.R/(4*self.Kt*self.Vdc))*(self.K_gains)

        ## IF duty cycle below MIN; cap at -100%
        if self.P_new <= self.Pmin:
            self.PWM = -100

        ## IF duty cycle above MAX; cap at 100%
        elif self.P_new >= self.Pmax:
            self.PWM = 100
        ## Otherwise keep defined value
        else:
            self.PWM = self.P_new

        return self.PWM

    def RTP_filter(self, x, xk, vk_x, deltaTime):
        ''' @brief      This function helps increase the resolution of ticks/sec.
            @details    This function takes the touchpanel data for the x and y
                        coordinates and their velocities; then recalculates more
                        precise values using alpha and beta filters.
            @param      speedRef estimated velocity of motor/encoder in [DEG/Sec]
            @param      speedMeas takes in calculated measured speed of motor/encoder [DEG/Sec]
            @param      posRef estimated position of encoder in [DEG]
            @param      posMeas calculated position from encoderDriver in [DEG]
            @param      deltaTime elapsed time of encoder [Sec]
        '''
        # Position in [deg]
        # Speeds in [deg/sec]
        # gains unitless
        # deltaTime corrected to reflect in [s]
        
        #####------- Alpha and Beta Filter Equations -------##########
        self.X_k = xk + self.alpha_gain*(x - xk) + (deltaTime*1e-6)*vk_x
        self.V_k = vk_x + (self.beta_gain*1e6)/deltaTime * (x - xk)

        return self.X_k
        return self.V_k


    def get_pos(self):
        ''' @brief   Returns corrected position value.
        '''
        return self.X_k

    def get_delta(self):
        ''' @brief   Returns corrected velocity values.
        '''
        return self.V_k

    def get_alpha(self):
        ''' @brief      This functions gets the current position gain of the motor.
        '''
        return self.alpha_gain

    def get_beta(self):
        ''' @brief      This functions gets the current velocity gain of the motor.
        '''
        return self.beta_gain
    
    def set_alpha(self,input_alpha):
        ''' @brief      This functions sets the constant gain to desired input.
            @param      inputKp is the desired gain constant input
        '''
        self.alpha_gain = input_alpha

    def set_beta(self,input_beta):
        ''' @brief      This functions sets the constant position gain to desired input.
            @param      inputKi is the desired position gain constant input
        '''
        self.beta_gain = input_beta