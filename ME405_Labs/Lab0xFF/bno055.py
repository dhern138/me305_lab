'''@file        bno055.py
@brief          This program is the driver for the BNO055 IMU.
@details        When combined with the IMUtask, the IMU will be able to use inertial measurement
                to provide enhanced feedback and error checking during balancing operation.

@author         Sydney Lewis
@author         David Hernandez
@date           06/08/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

import shares



class bno055:
    ''' @brief      Creates a class to use with any BNO055 IMU.
        @details    This class will allow the user to set up any BNO055 
                    sensor via I2C. The methods will allow the user
                    to perform an initial homing sequence to balance the platform,
                    act as a stand-alone sensor to use as feedback during balancing
                    operation, and act as a secondary sensor to provide error checking
                    and enhanced accuracy during balancing operation.
    '''

                                     
    def __init__(self, i2c, dev_addr=40):
        ''' @brief      Creates the constructor for a general BNO055 sensor.
            @details    This constructor allows the user to configure any
                        BNO055 sensor with a selected I2C line on the Nucleo.
            @param      i2c, this param configures the sensor to an already instantiated pyb.I2C object preconfigured in master mode.
            @param      dev_addr, this param configures the unique device address. This value could not be found in a data sheet; rather, the I2C.scan() had to be used.
        '''
        self.dev_addr = dev_addr
        self.i2c = i2c
        self.data = bytearray(2)
        self.chip_reg = 0x00    
        ## @brief       BNO chip register address
        #
        self.chip_ID = 0xA0           
        ## @brief       Expected data in register
        #  @details     Given on the BNO055 data sheet page 52
    

        self.pitch_Euler = 0
        self.roll_Euler = 0
        self.yaw_Euler = 0
        self.omega_x = 0
        self.omega_y = 0
        self.omega_z = 0
        pass

    def check(self):
        ''' @brief      Verifies sensor is attached to given bus address.
            @details    Locates the configured BNO055 sensor and verifies
                        that a connection has been established.
        '''
        self.i2c.mem_read(self.data, self.dev_addr, self.chip_reg)
        if self.data[0] == self.chip_ID:     
            return True
        else:
            return None
        
    def mode(self, MODE=0b1100):
        ''' @brief      Sets the operating mode of the BNO055.
            @details    The mode defaults to NDOF giving access to the accelerometer,
                        magnometer, and gyroscope data.
        '''
        self.MODE = MODE
        self.OPR_MODE = 0x3D
        ## @brief       Value of NDOF register as specified in data sheet.
        self.i2c.mem_write(self.MODE, self.dev_addr, self.OPR_MODE)
        print('NDOF mode selected')
        pass
    
    def cal_status(self):
        self.cal_reg = 0x35
        ## @brief       Value of calibration register as specified in data sheet.
        self.i2c.mem_read(self.data, self.dev_addr, self.cal_reg)
        if 3:
            print('calibrated')
            pass
        else:
            print('NOT calibrated')
            pass
        
    def Euler(self):
        ''' @brief      Reads the euler data registers of the BNO055.
            @details    The DOF parameter allows the user to specify which degree of freedom to read data from.
                        The default is set to all registers.
        '''
        self.pitch = 0x1E
        self.roll = 0x1C
        self.yaw = 0x1A
        
        ## Pitch data
        self.i2c.mem_read(self.data, self.dev_addr, self.pitch)
        self.pitch_LSB = self.data[0]
        self.pitch_MSB = self.data[1]     
        self.word = (self.pitch_MSB<<8)|self.pitch_LSB
        self.conv = 900*(self.pitch_LSB & 1)
        if self.conv != 0:
            self.pitch_Euler = self.word/self.conv
            shares.pitch_Euler.put(self.pitch_Euler)
            ## Roll data
            self.i2c.mem_read(self.data, self.dev_addr, self.roll)
            self.roll_LSB = self.data[0]
            self.roll_MSB = self.data[1]
            self.word = (self.roll_MSB<<8)|self.roll_LSB
            self.conv = 900*(self.roll_LSB & 1)
            if self.conv != 0:
                self.roll_Euler = self.word/self.conv
                shares.roll_Euler.put(self.roll_Euler)
                ## Yaw data
                self.i2c.mem_read(self.data, self.dev_addr, self.yaw)
                self.yaw_LSB = self.data[0]
                self.yaw_MSB = self.data[1]
                self.word = (self.yaw_MSB<<8)|self.yaw_LSB
                self.conv = 900*(self.yaw_LSB & 1)
                if self.conv != 0:
                    self.yaw_Euler = self.word/self.conv
                    shares.yaw_Euler.put(self.yaw_Euler)
                else:
                    self.yaw_Euler = 0
                    return self.yaw_Euler  
            else:
                self.roll_Euler = 0
                return self.roll_Euler  
        else:
            self.pitch_Euler = 0
            return self.pitch_Euler     
        

    def omega(self):
           ''' @brief      Reads the angular velocity data registers of the BNO055.
           '''
           
           self.omegax = 0x14
           self.omegay = 0x16
           self.omegaz= 0x18
           
           ## x axis data
           self.i2c.mem_read(self.data, self.dev_addr, self.omegax)
           self.omegax_LSB = self.data[0]
           self.omegax_MSB = self.data[1]     
           self.word = (self.omegax_MSB<<8)|self.omegax_LSB
           self.conv = 900*(self.omegax_LSB & 1)
           if self.conv != 0:
               self.omega_x = self.word/self.conv
               shares.omega_x.put(self.omega_x)
               ## y axis data
               self.i2c.mem_read(self.data, self.dev_addr, self.omegay)
               self.omegay_LSB = self.data[0]
               self.omegay_MSB = self.data[1]
               self.word = (self.omegay_MSB<<8)|self.omegay_LSB
               self.conv = 900*(self.omegay_LSB & 1)
               if self.conv != 0:
                   self.omega_y = self.word/self.conv
                   shares.omega_y.put(self.omega_y)
                   ## z axis data
                   self.i2c.mem_read(self.data, self.dev_addr, self.omegaz)
                   self.omegaz_LSB = self.data[0]
                   self.omegaz_MSB = self.data[1]
                   self.word = (self.omegaz_MSB<<8)|self.omegaz_LSB
                   self.conv = 900*(self.omegaz_LSB & 1)
                   if self.conv != 0:
                       self.omega_z = self.word/self.conv
                       shares.omega_z.put(self.omega_z)
                       return self.omega_x, self.omega_y, self.omega_z
                   else:
                       self.omega_z = 0
                       return self.omega_z  
               else:
                   self.omega_y = 0
                   return self.omega_y 
           else:
               self.omega_x = 0
               return self.omega_x 
            
    def get_Eul(self, DOF='all'):
       ''' @brief          Returns the most recently updated Euler angles.
           @details        The user can specify which angle to read from by typing 'all','pitch','roll', or 'yaw'.
                           The default is set to 'all'.
       '''
       self.DOF = DOF
       if self.DOF == 'all':
           return self.yaw_Euler, self.roll_Euler, self.pitch_Euler
       if self.DOF == 'pitch':
           return self.pitch_Euler
       if self.DOF == 'roll':
           return self.roll_Euler
       if self.DOF == 'yaw':
           return self.yaw_Euler
       pass
    
    def get_omega(self, axis='all'):
        ''' @brief          Returns the most recently updated angular velocities.
            @details        The user can specify which which axis to read data from by typing 'all','x','y', or 'z'.
                            The default is set to 'all'.
                    
        '''
        self.axis = axis
        if self.axis == 'all':
            return self.omega_x, self.omega_y, self.omega_z
        if self.axis == 'x':
            return self.omega_x
        if self.axis == 'y':
            return self.omega_y
        if self.axis == 'z':
            return self.omega_z
        pass
    
    
