'''@file        main.py
@brief          This file program runs all the tasks.
@details        This file designates the period and priority for each
                task in order to attempt an almost simultaneous performance.

                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/main.py

@author         Sydney Lewis
@author         David Hernandez
@date           6/9/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''


from micropython import const,alloc_emergency_exception_buf
import cotask

import print_task
import encoderTask
import MotorTask
import controllerTask
import dataCollectTask
import backEndTask
import TouchPanelTask

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)



# =============================================================================

if __name__ == "__main__":

    print ('Scheduler Running')
    # Create the tasks. If trace is enabled for any task, memory will be
    # allocated for state transition tracing, and the application will run out
    # of memory after a while and quit. Therefore, use tracing only for 
    # debugging and set trace to False when it's not needed
    # PERIOD in milliseconds
    encTask = cotask.Task (encoderTask.encoderTask, name = 'Encoder', priority = 1, 
                          period = 15, profile = True, trace = False)
    motTask = cotask.Task (MotorTask.MotorTask, name = 'Motor', priority = 1, 
                          period = 40, profile = True, trace = False)
    priTask = cotask.Task (print_task.run, name = 'Printer', priority = 0, 
                          period = 15, profile = True, trace = False)
    conTask = cotask.Task (controllerTask.controllerTask, name = 'Controller', priority = 1, 
                          period = 60, profile = True, trace = False)
    datTask = cotask.Task (dataCollectTask.dataCollectTask, name = 'Data', priority = 0, 
                          period = 20, profile = True, trace = False)
    endTask = cotask.Task (backEndTask.backEndTask, name = 'UI', priority = 0, 
                          period = 10, profile = True, trace = False)
    RTPTask = cotask.Task (TouchPanelTask.TouchPanelTask, name = 'RTP', priority = 1, 
                          period = 40, profile = True, trace = False)

    cotask.task_list.append (RTPTask)
    cotask.task_list.append (encTask)
    cotask.task_list.append (motTask)
    cotask.task_list.append (priTask)
    cotask.task_list.append (conTask)
    cotask.task_list.append (datTask)
    cotask.task_list.append (endTask)


    while True: 
        cotask.task_list.pri_sched ()
