# -*- coding: utf-8 -*-
''' @file       encoderTask.py
    @brief      Creates a class to run a motor and encoder that can track a given reference profile.
    @details    The class will use provided data to run motors at given speed and integrate simultaneously
                providing position data as well. 
                
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/encoderTask.py
    @author     David Hernandez
    @author     Sydney Lewis
    @date       5/27/21
    @copyright  2020-2021 Hernandez & Lewis Inc.
'''
import pyb
from pyb import Pin, Timer
from encoderDriver import encoderDriver
import shares
import print_task


def encoderTask ():
    """ @brief     This function file configures the encoders.
        @details   Connects the proper pin and timer set up to each encoder
                   which are then used to collect position, angular velocity,
                   and elapsed time. This portion is crucial because it will
                   denote the proper values the encoder should be to correct 
                   the balls position.
    """
    
    # State constants
    S0_INIT                 = 0 # User PRESSES:
    S1_ZERO_POSITION        = 1 # Z
    S2_PRINT_ENC_POSITION   = 2 # P
    S3_PRINT_ENC_SPEED      = 3 # D
    # S4_COLLECT_DATA         = 4 # G
    # S5_STOP_COLLECTING      = 5 # S
    # S6_GET_ALPHA            = 6 # A
    # S7_GET_BETA             = 7 # B
    # S8_SET_ALPHA            = 8 # U
    # S9_SET_BETA             = 9 # I
    
    #####------- Pin initialization for Encoders --------########
    PB6 = Pin(pyb.Pin.cpu.B6)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB6 for encoder
    #
    PB7 = Pin(pyb.Pin.cpu.B7)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinB7 for encoder
    #
    PC6 = Pin(pyb.Pin.cpu.C6)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinC6 for encoder
    #
    PC7 = Pin(pyb.Pin.cpu.C7)
    ## @brief       Locates respective pin on the Nucleo board
    #  @details     Locates PinC7 for encoder
    #
    ## Encoder 1 timer set-up
    TIM4 = Timer(4, period=0xFFFF, prescaler=0)
    TIM4.channel(1, Timer.ENC_AB, pin = PB6)
    TIM4.channel(2, Timer.ENC_AB, pin = PB7)
    
    ## Encoder 2 timer set-up when required
    TIM8 = Timer(8, period=0xFFFF, prescaler=0)
    TIM8.channel(1, Timer.ENC_AB, pin = PC6)
    TIM8.channel(2, Timer.ENC_AB, pin = PC7)
    
    ###### ------Encoder set up -------######
    e1 = encoderDriver(PB6, PB7, 4)
    ## @brief       Sets the first encoder up with pins B6 and B7 with timer 4
    # @details      Calls on encoderDriver class to input the required arguments into constructor.
     # @param       self.PB6 calls on pin B6 to get into first pin channel
     # @param       self.PB7 calls on pin B7 to get into second pin channel
     # @param       4 is the timer being used for the pin and channel inputs
    #
    e2 = encoderDriver(PC6, PC7, 8)
    ## @brief       Sets the second encoder up with pins C6 and C7 with timer 8
     # @details     Calls on encoderDriver class to input the required arguments into constructor.
     # @param       self.PC6 calls on pin C6 to get into first pin channel
     # @param       self.PC7 calls on pin C7 to get into second pin channel
     # @param       8 is the timer being used for the pin and channel inputs
    #
    
    # print_task.put('Encoders Online')
    print('encoder good')
    while True:

            ## Recieves data from selected encoder for: Position, and Speed
            shares.encoderPosition.put(e1.update())
            shares.speed1Meas.put(e1.get_delta())
            shares.encoder2Position.put(e2.update())
            shares.speed2Meas.put(e2.get_delta())
            # print_task.put('Encoder 1 position {:} RAD and speed {:} [RAD/Sec]'.format(shares.encoderPosition.get(),shares.speed1Meas.get()))
            # print_task.put('Encoder 2 position {:} RAD and speed {:} [RAD/Sec]'.format(shares.encoder2Position.get(),shares.speed2Meas.get()))
            yield(0)

            ####----- FSM ------#####
            if shares.ENCODERstate == S0_INIT:
                pass
        
            # User pressed Z, Change state and reset position of encoder
            elif shares.ENCODERstate == S1_ZERO_POSITION:
                shares.encoderPosition = e1.set_position()
                shares.encoder2Position = e2.set_position()
                shares.ENCODERstate = S0_INIT
        
            # User pressed P, Change state and print encoder's current position
            elif shares.ENCODERstate == S2_PRINT_ENC_POSITION:
                shares.encoderPosition = e1.get_position()
                shares.encoder2Position = e2.get_position()
                print_task.put('Encoder 1 position {:} RAD'.format(shares.encoderPosition))
                print_task.put('Encoder 2 position {:} RAD'.format(shares.encoder2Position))
                shares.ENCODERstate = S0_INIT
        
        
            # User pressed D, Change state and print encoder's delta
            elif shares.ENCODERstate == S3_PRINT_ENC_SPEED:
                shares.speed1Meas = e1.get_delta()
                shares.speed2Meas = e2.get_delta()
                print_task.put('Measured Speed for Encoder 1 {:} [RAD/sec]'.format(shares.speed1Meas))
                print_task.put('Measured Speed for Encoder 2 {:} [RAD/sec]'.format(shares.speed2Meas))
                shares.ENCODERstate = S0_INIT
            yield (0)

