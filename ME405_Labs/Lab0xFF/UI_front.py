
''' @file       UI_front.py
    @brief      User interface that serially communicates with back end programs.
    @details    Sends information back and forth between Nucleo and laptop
    @author     David Hernandez
    @author     Sydney Lewis
    @date       6/8/21
    @copyright  2020-2021 Hernandez and Lewis Inc.
'''

import keyboard
import serial


def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name


# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb)
## @brief End data collection prematurely
#
keyboard.on_release_key("G", callback=kb_cb)
## @brief Collect encoder 1 data for 30 seconds
#
keyboard.on_release_key("Z", callback=kb_cb)
## @brief Zero the encoder 1 position
#
keyboard.on_release_key("P", callback=kb_cb)
## @brief Print out the encoder 1 position
#
keyboard.on_release_key("D", callback=kb_cb)
## @brief Print out the encoder 1 delta
#
keyboard.on_release_key("A", callback=kb_cb)
## @brief Print out the motor alpha gain position
#
keyboard.on_release_key("B", callback=kb_cb)
## @brief Print out the motor beta gain velocity
#
keyboard.on_release_key("U", callback=kb_cb)
## @brief Set out the motor alpha gain position
#
keyboard.on_release_key("I", callback=kb_cb)
## @brief Set out the motor beta gain velocity
#
keyboard.on_release_key("F", callback=kb_cb)
## @brief Set out the motor beta gain velocity
#

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization

    ## Store the last input key from the Keyboard
    last_key = None
    ## @brief Verifies if a key has been pressed or not
    #

    ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
    ## @brief Opens serial port to communicate with Nucleo
    #
    print('Press G to collect data:\r\nPress S to STOP anytime:\r\nPress Z to ZERO encoder position'
          '\r\nPress P to PRINT out encoder position:\r\nPress D to PRINT out encoder delta:'
          '\r\nPress M to PRINT out motor speed:\r\nPress A to GET alpha position gain:'
              '\r\nPress B to GET beta velocity gain''\r\nPress U to SET alpha position gain'
              '\r\nPress I to SET beta velocity gain''\r\nPRess F to FIX board')


    while True:
        try:

            ## Provides the user feedback when the appropriate command is pressed
            if last_key is not None:
                print("You pressed " + last_key)
                ser.write(str(last_key).encode('ascii'))
                last_key = None

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed, program ended. Goodbye')
            break

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()