''' @file       dataCollectTask.py
    @brief      This file gathers data request by user via UI.
    @details    Sends formatted data collected from the encoder and motor
                between the Nucleo and computer.
    @author     David Hernandez
    @author     Sydney Lewis
    @date       6/8/21
    @copyright  2020-2021 Hernandez and Lewis Inc.
'''



import shares

def dataCollectTask():
    ''' @brief      A task class that writes gathered motor speed and positional data to UI for processing.
        @details    When user interface commands are pressed, this task will work with other task
                    to gather any data user demands. Then using UART will write the data over to
                    the front end for processing.
    '''

    # State constants
    # S0_INIT                 = 0 # User PRESSES:
    # S1_ZERO_POSITION        = 1 # Z
    # S2_PRINT_ENC_POSITION   = 2 # P
    # S3_PRINT_ENC_SPEED      = 3 # D
    # S4_COLLECT_DATA         = 4 # G
    # S5_STOP_COLLECTING      = 5 # S
    # S6_GET_ALPHA            = 6 # A
    # S7_GET_BETA             = 7 # B
    # S8_SET_ALPHA            = 8 # U
    # S9_SET_BETA             = 9 # I
    # S10_CLR_ERROR           = 10 # E
    # S11_BAL_BOARD           = 11 # F

    period = 1
    ## @brief   1 microsecond of elapsed time.
    #
    time = 0
    ## @brief   Total time elapsed.
    #

    print('DATA good')
    
    while True:
        #######------- Encoder Data --------########
            e1Pos = shares.encoderPosition.get()
            e1Vel = shares.speed1Meas.get()
            e2Pos = shares.encoder2Position.get()
            e2Vel = shares.speed2Meas.get()
        #######------- RTP Data --------########
            RTPx  = shares.x
            RTPvx = shares.vx
            RTPy  = shares.y
            RTPvy = shares.vy
            
        #### -----Print data as CSV in console -----#####
            with open("Lab0xFF.csv", "a") as a_file:
                a_file.write("{:},{:},{:},{:},{:},{:},{:},{:},{:}\r\n".format(e1Pos,e1Vel,e2Pos,e2Vel, RTPvx, RTPx, RTPvy, RTPy, time))
            time += period

            yield(0)
