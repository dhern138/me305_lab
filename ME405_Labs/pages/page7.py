## \page page7 HW0x04 System Model Simulation
#  \tableofcontents
#  The following documents are for a MATLAB script with the purpose of simulating the 
#  dynamics of the ball and platform. The script takes the developed matrices from \ref page6 System Modeling
#  and using the Jacobian function in MATLAB it converts them in the matrices required for state space.
#  The matrices were kept symbolically for the majority of the code, allowing user modification. Once the matrices 
#  were manipulated, the given parameters were substituted to simulate the system under different initial conditions.
#  The conditions and their affects on the system are described and displayed further on. The system model being discussed can be observed below.
#
#     \image html Platform.JPG "Figure 1. System Model Analyzed" width=400cm height=300cm
#      
#
#  The model has two motors that shift the platform with 2 degrees of freedom
#  As mentioned the lab manual, 'The first motor spins about its x-axis but tilts the
#  platform about its y-axis and the second motor spins about its y-axis but tilts the platform about
#  its x-axis'. 
#
#  \section sec5 System Model Simulation
#  This page contains the subsection MATLAB
#  \subsection subsection14 MATLAB
#  In this assignment, a controller was used to simulate the ball and platform dynamics using Matlab and Simulink.
#  The full Matlab script can be downloaded here: https://bitbucket.org/dhern138/me305_lab/src/syd/ME405_Labs/HW0x04/HW0x04.mlx
#  Once the parameters were input and the matrices from assignment HW0x02 were linearized with Jacobian methods, the system was 
#  modeled in Simulink with the block diagram below:
#
#     \image html HW4_Block.png "Figure 2. System Block Diagram" width=600cm height=400cm
#
#  The following image displays the response of the ball and platform system
#  when the initial conditions are zero and there is no applied torque from the motor.
#  The parameters focused on were: Position [m], Velocity[m/s], Angle[rad], and Angle Velocity[rad/s].
#
#     \image html HW4_Part3A.png "Figure 3. System Response at Rest and Balanced with No Motor Torque" width=600cm height=600cm
#
#  Looking at the subplots, it comes to no surprise that they were all flatlined. The
#  system had no external or internal energy acting on the system, thus it remained at rest.
#  The next image displays the system's reponse when the initial conditions were changed.
#  The only change made was to the position of the ball. It was offset 5 [cm] from the center
#  of gravity of the system. The four parameters performed as expected. Due to having no feedback from the 
#  system, the parameters continue indefinitely following Newton's First Law. 
#
#     \image html HW4_Part3B.png "Figure 4. System Response at Rest with Ball Offset" width=600cm height=600cm
#
# For part 4 of the simulation, the system was tested in closed-loop by implementing full state feedback. The controller
# was not designed, rather the gains were provided in the assignment as: K = [-0.3, -0.2, -0.05, -0.02] (N, N*m, N*s, N*m*s).
# With full state feedback, the expected response with the system at rest and the ball off-centered by 5cm is oscillation.
# As the system tries to correct the error and achieve a balanced and centered position, the controller changes the 
# current value, overshoots, corrects again, and continuously fluctuates until the system is stable and decays to zero. 
# As shown in the following image, this is precisely the behavior exhibited in the simulation.
#
#    \image html HW4_Part4.png "Figure 5. System Response with Full-State-Feedback" width=600cm height=600cm

