'''@file        Lab0x04_main.py
@brief          This program measures temperature on the MCP9808 and microcontroller.
@details        When uploaded to the microcontroller and ran from Putty, this program will
                measure temperature over the course of 8 hours using I2C communication.
                See file here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab4/Lab0x04_main.py

@author         Sydney Lewis
@author         David Hernandez
@date           5/6/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''

import pyb
from pyb import I2C
from array import array
import utime
from binascii import unhexlify


ID = bytearray(2) 
## @brief Buffer to hold the manufacturer ID data from the sensor
data = bytearray(2)
## @brief Buffer to hold the temperature data from the sensor
data_l = bytearray(1)
## @brief Buffer to hold the lower byte of temperature data from the sensor
data_u = bytearray(1)
## @brief Buffer to hold the upper byte of temperature data from the sensor
time = 0
## @brief Variable to count time
state = 0
period = 1


class MCP9808:
    ''' @brief      Creates a class to use with any MCP9808 temperature sensor.
        @details    This class will allow the user to set up any MCP9808 
                    temperature sensor via I2C. The methods will allow the user
                    to check if the sensor is connected and return values for 
                    temperature in celcius and fahrenheit.
    '''


    def __init__(self,I2C_bus,Dev_addr):
        ''' @brief      Creates the constructor for a general MCP9808 sensor.
            @details    This constructor allows the user to configure any
                        MCP9808 sensor.
            @param      I2C_bus, this param configures the sensor to a bus.
            @param      Dev_addr, this param configures the unique device address.
        '''
        self.registers = {'TA_reg':0x05, 'MFG_reg':0x06}
        self.Dev_addr = Dev_addr
        self.I2C_bus = I2C_bus
        self.ID = bytearray(2)
        self.mfg_reg = 0x06


    def check(self,I2C_bus):
        ''' @brief      Verifies sensor is attached to given bus address.
            @details    Locates the configured MCP9808 sensor and verifies
                        that a connections has been established.
            @param      I2C_bus, this param calls on the configured temperature sensor.
        '''

        self.I2C_bus.mem_read(self.ID, self.Dev_addr, self.mfg_reg)

        if ((self.ID[0]<<8|self.ID[1]) == 0x0054):     # expected data in register, given on the MCP9808 data sheet page 33
            return True
        else:
            return None



    def celcius(self,temp_sens):
        ''' @brief      Returns the  temperature in Celcius (C).
            @details    Converts temperature derived from the MCP9808.
        '''
        self.temp_C = 0
        self.temp_C = temp_sens
        return self.temp_C

    def temp_F(self):
        '''@brief      Convert sensor data from Celcius to Fahrenheit
        '''
        self = self*9/5 + 32
        return self


adcall = pyb.ADCAll(12, 0x70000)    # 12 bit resolution, internal channels
val = adcall.read_vref()
i2c_1 = I2C(1)
i2c_1.init(I2C.MASTER, baudrate=400000, gencall=False, dma=False)
sensor = MCP9808(i2c_1, 0x18)        # select the I2C bus and connect the sensor to the clock
                                     # and data line on pins PB_8 and PB_9

utime.sleep_ms(100)



# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play;
#   it does not run if the script is imported as a module
if __name__ == "__main__":
    ## Program initialization. Give reference to I2C object and the address 
    ## of the MCP9808 temperature sensor. 
    ## The device address for the MCP9808 temperature sensor is ‘0011,A2,A1,A0’ in
    # binary, where the A2, A1 and A0 bits are set externally by connecting the pins 
    # to VDD ‘1’ or GND. Since we are using only one sensor, the default
    # configuration is kept which is A2, A1, and A0 as GND, or '0'.

    while True:
        try:

            if state == 0:
                MCP9808.check(sensor,i2c_1)         # read 2 bytes of data into the ID byte array from the
                                                     # hex address 0x18 starting at register 6.
                                                     # 0x18 hex corresponds to device address 0011000
                                                     # 6 hex corresponds to register address 0110 (manufacturer ID)
                print('The sensor ID is:')
                print(ID)
                print('')                                     
                
                if True:     # expected data in register, given on the MCP9808 data sheet page 33
                    state = 1
                else:
                    print('Incorrect ID, check sensor attachment.')
            
            # After verifying the sensor is attached, read the upper and lower bytes of temperature data
            if state == 1:
                print('')
                print('Collecting temperature data')
                print('')
                i2c_1.mem_read(data, 0x18, 0x05)           # read both bytes
                data_u = data[0]                         # separate the upper and lower bytes
                data_l = data[1]
                temp_mc = adcall.read_core_temp()        # read the core temperature from the MC
                mc_F = MCP9808.temp_F(temp_mc)
                print(temp_mc,'Core - Celcius')
                
                # Convert Data to degrees Celsius. First check flag bits
                if ((data_u and 0x80) == 0x80):
                    data_u = data_u and 0x1F
                if ((data_u and 0x40) == 0x40):
                    data_u = data_u and 0x1F
                if ((data_u and 0x20) == 0x20):
                    data_u = data_u and 0x1F                 # clear flags 
                    # if no flags are set convert temperature
                if ((data_u and 0x10) == 0x10):    # if temp is greater than zero C
                    data_u = data_u and 0x0F
                    temp_sens = 256 - data_u*16 + data_l/16
                    print(temp_sens, 'Sensor - Celcius')
                    Tf = MCP9808.temp_F(temp_sens)
                    print(Tf, 'Sensor - Fahrenheit')
                    utime.sleep(1)
                    state = 2
                else:   # if temp is less than zero C
                    temp_sens = data_u*16 + data_l/16
                    print(temp_sens, 'Sensor - Celcius')
                    Tf = MCP9808.temp_F(temp_sens)
                    print(Tf, 'Sensor - Fahrenheit')
                    utime.sleep(1)
                    state = 2
            
            # File Writing
            if state == 2:
                with open("Temperature.csv", "a") as a_file:
                    a_file.write("{:},{:},{:},{:},{:}\r\n".format(temp_sens, Tf, temp_mc, mc_F, time))
                    #
                print('Data Written to CSV')
                time += period
                state = 1

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed, program ended. Goodbye')
            break

