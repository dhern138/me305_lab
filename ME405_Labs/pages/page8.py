## \page page8 HW0x05:Full State Feedback
#  \tableofcontents
#  A script was created via live editor in MATLAB which allowed a mixture of code and text for result interpretation.
#  The purpose was to develop a closed loop controller for a ball and platform system model.
#  The goal is to analytically determine a starting set of gain values that are to be used in the physical tuning of 
#  the model system. 
#
#The numerical parameters provided are below:
# * Radius of Lever Arm,     rm = 60   [mm]
# * Length of Push Rod,      lr = 50   [mm]
# * Radius of Ball,          rb = 10.5 [mm]
# * Vertical Distance from U-Joint to CG of Platform,      rG   42   [mm]
# * Horizontal Distance from U-Joint to Push-Rod Pivot,    lP   110  [mm]
# * Vertical Distance from U-Joint to Push-Rod Pivot,      rP   32.5 [mm]
# * Vertical Distance from U-Joint to Platform Surface,    rC   50   [mm]
# * Mass of Ball,     mB 30   [g]
# * Mass of Platform, mP 400 [g]
# * Moment of Inertia of Platform (About Horizontal Axis through CG), IP 1.88x10^6 [g *mm^2]
# * Viscous Friction at U-Joint, b 10 [mN*m*s/rad]
#
#  The image below is from running the newly defined gains in MATLAB (Please read further for more information):
#
#     \image html HW5_graphs.png "Figure 1. System with newfound gains" width=500cm height=500cm
#
#  The link below will allow you to download the required MATLAB script for this assignment:
#  https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/HW0x05/
#
# \section sec6 New Gains
#  This page contains the subsections:
# * Eigenvalues
# * Characteristic Polynomial Selection
# * Gain Results
# \subsection subsection15 Eigenvalues
#  Using the previous work from \ref page7 the group was able to derive the open
#  loop eigenvalues of state space matrix A. The numerical value was not too important
#  but its sign value was. Since the eigenvalues represent the poles of the system
#  it is crucial for them to be negative in order to have a stable system, however,
#  not all values were negative. There are a total of 4 poles for this system and 
#  because there are 1 positive, this makes the desired system unstable. Thus, requires 
#  further evaluation. This was when the group defined the control law using the full
#  state feedback. This Redefined the state-space matrix A by modifying it with matrix B
#  and unknown gain vector of K. It then proceeds to take the determinate, which is 
#  solved for K1, K2, K3, and K4. Then using the characteristic polynomial equation 
#  P = det(SI-A) 
#  with the newly developed A; the group used system of equations to break the equations
#  4 equations solved for each K variable from K1 to K4. Please read further to understand
#  how each was solved in \ref Characteristic Polynomial Selection.
#
# \subsection subsection16 Characteristic Polynomial Selection
#  Our group decided to choose method (b), in which we pick 2 arbitrary pole locations and
#   a natural frequency (w) with a damping ratio (z). It is important to note that if poles 3 
#  and 4 are much larger than 1 and 2 associate with w and z, then the system will behave as a 
#  2nd order system. The group decided to choose this method because we can decide when the 
#  system will reach a settling period by deciding a time. MATLAB provides a simple equation 
#  to find that settling period time using the chose w and z values.
#
#  Ts = 3.9/(z*w)  for 2% settling time.
#
#  overshoot = exp(-z*pi/sqrt(1-z^2))*100  for overshoot percent.
#  where damping ratio, z was 0.8, and the natural frequency, w was 10.
#  The settling time for the system came out to 0.4875 [s], which the 
#  group believed was a reasonable response for the system to reach 
#  steady state. The overshoot percentage was about 1.52 [%], which
#  was something that group was expecting because the settling time
#  was set to about 2 %. All represented in table below:
#
#     \image html freqTable.png "Figure 2. System Damping Ratio and Natural Frequency" width=400cm height=100cm  
#
# \subsection subsection17 Gain Results
#  Taking the coefficients from the generic equation 
#
#  P(s) = (S^2+2zwS+w^2)(S-lambda3)(S-lambda4) 
#
#  to represent a0 through a3 from the char. polynomial equations 
#  and then plug them into the system equations to find gain vector K.
#  This gave us new gain values of:
#
#  K = [-5.6397, -1.5532, -1.5159, -0.1299]
#
#  These values were tested by putting them back into char.polynomial
#  equation to find the eigenvalues from the results. 
#
#     \image html eigenVal.png "Figure 3. Developed Eigenvalue" width=200cm height=200cm  
#
#  The results were agreeable so we pushed forward and developed the plots seen in figure 1.
#  The plots are for system position, velocity, angle, angular velocity
#  also labeled for the reader. These are the same plots first mentioned in this report.