## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @tableofcontents
#  @section sec_port David Hernandez's Portfolio
#  This ME405 Portfolio contains all the laboratories and homeworks created and developed
#  during the Spring 2021 ME405 course. Please read further for more information on ME405.
#
#     \image html FrontView.jpg "Figure 1. Ball and Platform System Model" width=400cm height=500cm
#
#  All laboratories and homeworks created and developed for this course can be quickly accessed
#  via the following links:
# - \subpage labs
# - \subpage hw
#
#  @section intro_sec What is ME405: Mechatronics
#  ME405 was a Mechatronics course offered at California Polytechnical College San Luis Obispo.
#  The term Mechatronics is derived by fusing the individual disciplines 'Mechanical' and 'Electronics'. 
#  It combines the best of both worlds or as we ME's like to say 'Put the smarts into a mechanical system'.
#  The purpose is to efficiently use 'programming' to implement 'controls' to 'electronic' systems for mechanical operations.
#  
#  @subsection how_sec How the Knowledge was Implemented
#  Using the computer language of Python we were able to:
#  * Embedd systems into modern microcontrollers
#  * Select problems for which mechatronics is a good solution
#  * Perform high level design of a mechatronic system
#  * Design complex programs using an organized methodology
#  * Design software to meet real-time constraints
#  * Write and debug program code efficiently
#  * Document hardware and software designs thoroughly via Doxygen
#  * Understand and design solutions for mechatronics safety
#  * Work effectively as a member of a development team
#  * Search for and find design data using appropriate resources 
#
#  @subsection tools_sec Tools and Equipment
#  The list of equipment used are as follows:
#      * 2 Maxon Motor: DCX22S
#      * 1 Nucleo L476RG Development Board
#      * 1 MCP9808 Temperature Sensor Breakout Board
#      * x Jumper Wires
#      * 1 USB Type A Breakout Cable
#      * 2 USB-B mini cables
#      * 1 Laptop
#      * 1 AC-DC Power Adapter, 12V/5A
#      * 1 Mechatronics Base Unit
#      * 2 Motor Assemblies
#      * 1 Top Panel Assembly
#      * 1 Resistive Touch Panel
#      * 2 Encoder Cables
#      * 1 U-Joint Assembly
#      * 2 Motor Arm/Push Road Assemblies
#      * 1 BN0055 Breakout w/ Cable
#      * 1 FPC Touch Breakout w/ Cable
#      * 1 Rubberized Steel Ball
#
#  The list of tools used are as follows:
#      * Computer with Windows 10
#      * Python Computer Language
#      * Anaconda with Ampy
#      * Sourcetree
#      * Doxygen
#      * Bitbucket
#
#  @subsection mod_sec Modules
#  See individual modules for details. Included modules are:
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x02 (\ref sec_lab2)
#  * Lab0x03 (\ref sec_lab3)
#  * Lab0x04 (\ref sec_lab4)
#  * Lab0xFF (\ref sec_lab0xFF)
#  * HW0x01 (\ref sec_hw1)
#  * HW0x02 (\ref sec_hw2)
#  * HW0x03 (\ref sec_hw3)
#  * HW0x04 (\ref sec_hw4)
#  * HW0x05 (\ref sec_hw5)
#
#  @author David Hernandez
#  @author Sydney Lewis
#  @date May 15, 2021
#
#
#  \page labs Laboratories
#  All laboratories completed for ME405 can be observed below with a description about each one.
#  @tableofcontents
#
#  @section sec_lab1 Lab0x01 Documentation 
#  * Description: This lab contains the program for a simulated vending machine.
#  * Source: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab1/vendorLab0x01.py
#  * Documentation: \ref vendorLab0x01.py
#  * Please see \ref page1 for more information.
#
#  @section sec_lab2 Lab0x02 Documentation 
#  * Description: This lab contains the programs for two methods of testing human reaction times.
#  * Source: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab2/
#  * Documentation: \ref Lab0x02A.py and \ref Lab0x02B.py
#  * Please see \ref page2 for more information.
#
#  @section sec_lab3 Lab0x03 Documentation 
#  * Description: This lab contains the user interface and program to verify the analog digital
#  converter (ADC) on board the NucleoSTM32 at the press of a button.
#  * Source: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab3/
#  * Documentation: \ref Lab0x03_UI.py and \ref Lab0x03_main.py
#  * Please see \ref page3 for more information.
#
#  @section sec_lab4 Lab0x04 Documentation 
#  * Description: This lab contains a class with program that configures a MCP9808 breakout board
#  with I2C allowing visibility of temperature in multiple formats.
#  * Source: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab4/
#  * Documentation: \ref Lab0x04_main.py
#  * Please see \ref page4 for more information.
#
#  @section sec_lab0xFF Lab0xFF Documentation 
#  * Description: This lab contains multiple taskers and drivers required for the culminatative
#  term project. The term project was to use all previous knowledge to create a program of the system
#  model of the ball and platform. This considered the equations and simulations from the homeworks.
#  * Source: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab0xFF/
#  * Documentation: \ref Lab0xFF_main.py
#  * Please see \ref page5 for more information.
#
#  \page hw Homeworks
#  All homeworks completed for ME405 can be observed below with a description about each one.
#  @tableofcontents
#
#  @section sec_hw1 HW0x01 Documentation 
#  * Description: A simple program that returns change in smallest allowed denominations.
#  * Source: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/HW0x01/
#  * Documentation: \ref refund.py
#
#  @section sec_hw2 HW0x02 Documentation 
#  * Description: This homework were the necessary hand calculations to develop
#  the proper equations of motion (EOM) for the system model of the ball and platform.
#  * Please see \ref page6 for more information.
#
#  @section sec_hw3 HW0x03 Documentation
#  * Description: This was an adminstrative assignment.
#  Hardware distribution. (Administrative)
#
#  @section sec_hw4 HW0x04 Documentation 
#  * Description: This homework used MATLAB to simulate the performance of the ball
#  on the platform using the equation of motions and MATLAB's Jacobian commands.
#  * Please see \ref page7 for more information.
#
#  @section sec_hw5 HW0x05 Documentation 
#  * Description: This homework used MATLAB to expand further on \ref sec_hw4. This
#  time the goal was define our own gain values for the system model.
#  * Please see \ref page8 for more information.
#
