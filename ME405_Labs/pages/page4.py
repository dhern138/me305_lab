## \page page4 Lab0x04: Hot or Not?
#  \tableofcontents
#   For this lab a temperature sensor, the MCP9808 shown in Figure 1, was soldered and connected by wires to
#   the Nucleo L476RG microcontroller.
#
#     \image html MCP9808.png "Figure 1. MCP9808 Temperature Sensor with Un-Soldered Pins" width=400cm height=400cm
#      
#  The sensor needed to be connected to the Nucleo's clock and data lines, PB_8 and PB_9, boxed in red in Figure 2. With the 
#  lines connected, I2C communication was initialized allowing data to be collected over an eight hour time period.
#
#     \image html nuc_pinout-1.jpg "Figure 2. Nucleo-L476RG with Clock and Data Pins Highlighted" width=600cm height=400cm
#
# \section sec4 Hot or Not?
#  \ref Lab0x04 is broken down into two parts. The code written and the results of the data collection.
#  \subsection subsection12 Code
#  Initializing I2C was the main learning objective in this assignment. To compare temperature readings, the internal temperature
#  of the microcontroller was measured at the same time. To initialize the core temperature readings, the battery voltage needed
#  to be read beforehand. This was unexpected, and appears to be a bug in the code. This feature was verified from multiple students 
#  and Charlie Revfem, the instructor.    
#  To view the full Python script, click here: https://bitbucket.org/dhern138/me305_lab/src/master/ME405_Labs/Lab4/Lab0x04_main.py
#
#
# \subsection subsection13 Results
#  After collecting data for an eight hour period, the results of the core and sensor measurement were plotted. 
#  The data is shown graphically below in Figure 3.
#
#     \image html Temperature_plot.png "Figure 3. Temperature Data Over Eight Hours" width=600cm height=600cm
#
#  As expected, the internal core temperature of the microcontroller reads higher values than the temperature sensor since it is 
#  processing information and commanding peripherals. As the microcontroller runs the readings get larger apart showing the heating
#  effect of current flow. The temperature data was taken on Sydney's desk in Atascadero CA, on May 12th, 2021 from 3:45 - 11:45pm.
#
##     \image html repository.png "Figure 4. Git repository of work on Lab0x04" width=600cm height=400cm
#