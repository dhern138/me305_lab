## \page page5 Lab0xFF: Term Project
#  \tableofcontents
#   This final laboratory was the culmination of every facet learned in the ME405 course. This incorporated the skills
#  of creating multiple drivers, taskers, user interface, and a main task to run them all as one system. The purpose was to
#  develop a program to autonomously and continuously keep a ball of some mass on a motorized platform. The development and 
#  calculations of this project can be seen in \ref page6 and \ref page7. Each specific driver and task are described below 
#  in more details.
#
#  Equipment Used:
#  * 2 - USB-B mini cables
#  * 1 - Laptop
#  * 1 - AC-DC Power Adapter, 12V/5A
#  * 1 - NucleoSTM32 MCU
#  * 1 - Mechatronics Base Unit
#  * 2 - Motor Assemblies
#  * 1 - Top Panel Assembly
#  * 1 - Resistive Touch Panel
#  * 2 - Encoder Cables
#  * 1 - U-Joint Assembly
#  * 2 - Motor Arm/Push Road Assemblies
#  * 1 - BN0055 Breakout w/ Cable
#  * 1 - FPC Touch Breakout w/ Cable
#  * 1 - Rubberized Steel Ball
#
#  The images below are the official set up of the system model (Different Angles for viewability)
#     \image html FrontView.jpg "Figure 1. System Model Front View" width=600cm height=800cm
#     \image html RearView.jpg "Figure 2. System Model Rear View" width=600cm height=800cm
#
#  The task diagram representing the theoretical set up for each driver and tasker.
#
#     \image html TaskDiagram.png "Figure 3. System Model task diagram" width=600cm height=600cm
#
# \section sec7 System Model Term Project
#  \ref Lab0xFF contains the following subsections: 
#  * TouchDriver
#  * TouchTask
#  * MotorDriver
#  * Motor Task
#  * Encoder Driver
#  * Encoder Task
#  * Closed Loop Driver
#  * Controller Task
#  * Data Collect Task
#  * UI Back Task
#  * UI Front Task
#  * Shares
#  * Main
#
# \subsection subsection18 TouchDriver
#  This driver interacts with the Nucleo STM32 microcontroller allowing objects of a
#  resistive touch panel to be created. The purpose was to allow interaction with the
#  provided resistive touch panel (RTP), which was required for the term project.
#  This driver allowed the capabilities of measuring the position of the rubberized 
#  steel ball. The position was crucial for defining the velocity of the ball, which
#  was required to for various equations. To do this, the driver takes negative and positive
#  terminals for the predetermined X and Y axes on the RTP. By using voltage division and
#  the analog to digital converter, the driver grounds the negative terminal for the 
#  specific component the user is looking at and 3.3V to the positive terminal, while floating
#  the remaining component's terminals. This provides a specific distance that is returned 
#  to the user.  To verify if there was contact with the RTP, the driver considers if
#  there is a voltage being pushed; if there is, it immediately recognized it as contact
#  being made.  An x,y,and z scan method was develop for each to individually find each 
#  position or contact. A settling period and filter were developed by oversampling and 
#  taking the average and a filter, respectively. An overall scan method was also created to allow
#  the user to measure all 3 components at once. In addition, a calibration code was added to 
#  each which considered the voltage reading from a designated center, 88 and 50, for the x and
#  y axes, respectively. It is important to note that works from Peter Hinch, retired hardware
#  and firmware developer, was used for the filter and settling period. 
#  Files can be found at \ref TouchDriver.py, \ref avg.py, and \ref fir.py
#
#  The following image is for the breakout board used for the RTP:
#     \image html RTPbreakout.jpg "Figure 4. FPC Touch Breakout w/ Cable" width=300cm height=400cm
#
# \subsection subsection19 TouchPanelTask
#  This task called on the \ref TouchDriver and specified the appropriate RTP object to be used
#  Using the following pin set up: ym=PA0, xm=PA1, yp=PA6, xp=PA7, where the 'm' subscripts 
#  associate to the negative terminal on the breakout board for the appropriate axis, and the
#  'p' subscripts for the positive terminal. This task allows the user to test, calibrate, and
#  collect necessary positional values. File can be found at \ref TouchPanelTask.py.
#  The task then collected the positional values in a tuple for x,y,and z and returned the 
#  appropriate values for each. There were two conditions that had to be met and those were:
#
#  * That it must be able to read from a single channel in less than 500us  
#  * All three channels in less than 1500us 
#
#  This was to guarantee that the driver worked cooperatively with the other code.
#  Benchmark tests were performed to ensure the conditions were met. These can be seen in the 
#  images below.
#
#  The following image was the benchmark for a single channel:
#     \image html singleChannel.jpg "Figure 5. RTP single channel benchmark." width=300cm height=400cm
#
#  The following image was the benchmark for all three channels:
#     \image html allChannel.jpg "Figure 6. RTP three channel benchmark." width=300cm height=400cm
#
#  File can be found at \ref TouchPanelTask.py
#
# \subsection subsection20 MotorDriver
#  This file contains the class MotorDriver for use with the DRV8847 motor.
#  This class allows the user to configure the pins, timer, and channel
#  to an H-Bridge motor. The user can then create motor objects to test any motor.
#  This class uses four pin objects, two for the timer channel variables,
#  two for each motor configured and a timer object. Using these it is 
#  possible to configure a sleep pin, a fault pin. The driver software
#  uses logic to interpolate the given duty command into the correct
#  PWM percentage and direction. The driver also has the ability to enable 
#  and disable motor motion.
#  File can be found at \ref MotorDriver.py
#
# \subsection subsection21 MotorTask
#  The user can create motor objects to test any motor by configure the pins, 
#  timer, and channel. After doing so it is possible to test any motor by
#  continuously running the motor, testing the fault, motion or by time.
#  This task was modified to run with the main tasker allowing it to control
#  all motor operations to adjust the motor as needed to maintain the ball
#  in the center of the platform.  These motors are not ideal, thus requiring
#  a unique duty cycle for each to overcome their individual static friction.
#  A video, link posted below, has been made to show a benchmark test for 
#  each one of the motors with the required duty cycle for each. It also
#  displays the fault method at work in the case that there were a fault
#  with the motors. Responsible for interfacing with the motors and motor
#  drivers to deliver the correct PWM signal associated with the actuation values computed in
#  the controller task using an object of the motor driver class.
# 
#  This link will take you to a google drive stored video for the required duty cycles to
#  overcome static friction and the fault method at work.
#
#  https://drive.google.com/file/d/1BFjKUP4uEZPgg1H8Ug0QG-MdE77Gqxpz/view?usp=sharing
#
#  File can be found at \ref MotorTask.py
#
# \subsection subsection22 Encoder Driver
#  This driver creats a contructor which allowed any encoder to be set up with
#  the proper pins and timer configuration. After creating encoder objects it
#  runs them through the 'Update' method, which takes the 'ticks', shaft rotations,
#  then algorithmically corrects any overflow data and produces a corrected position 
#  to use. However, these are in ticks and for further calculations, they needed to be
#  in 'radians', so with some unit conversion this was made possible within the 
#  driver. During this time a timer is running while the encoder would be operating;
#  dividing the corrected encoder position by this time it was possible to have the
#  driver calculate the encoder's angular speed in 'radians per second'. The other methods
#  within the driver allowed the user to pull this data or reset all values to 0 if desired.
#  File can be found at \ref encoderDriver.py
#
# \subsection subsection23 Encoder Task
#  File can be found at \ref encoderTask.py
#
# \subsection subsection24 Closed Loop Driver
#  This class takes the gain value-end calculation from the controller task,
#  and applies it to te defined calculation within the class to produce a
#  proper power output. It consider the motors: resistance, voltage output, 
#  torque constant, and gear ratio. This power output, duty cycle, is 
#  then returned to the controller task.
#  File can be found at \ref closedLoop.py
#
# \subsection subsection25 Controller Task
#  This task takes the previously defined gains from the homework solutions
#  discussed above, and dot products them with the following state vector:
#
#  [x, theta_y, x_dot, theta_y_dot]
#
#  where x is the RTP position in meters, theta_y is the encoder position in radians,
#  x_dot is the RTP velocity in meters per second, and theta_y_dot is the encoder 
#  angular velocity in radians per second. These are multiplied to the gains and 
#  pushed to the Cloosed Loop class which produces the duty cycle for the motors.
#  The goal is to keep the ball balanced on the board, so the RTP needs to constantly
#  correct the ball position by sending values that to the controller task to center
#  the ball.
#  File can be found at \ref controllerTask.py
#
# \subsection subsection26 UI Back Task
#  This back end task serves to communicate serially with the user interface
#  front end. Its purpose is simple, interpret any commands from the user through
#  the UI Front Task, and produce the desired results. It takes serial ASCII integers
#  and switches states, which are shared between all tasks via the shares file.
#  This allows for smooth transition between commands to all files.
#  File can be found at \ref backEndTask.py
#
# \subsection subsection27 UI Front Task
#  This front end UI sends serially encrypted values to the back end UI to get an
#  outcome. The list of possible functions are presented to the user and they simply
#  have to press a key to send the command to the back end UI, which transitions 
#  states within all tasks to produce the desired outcome. Responsible for interaction 
#  between the user and the program. This interface includes commands that allow 
#  the user to begin balancing, clear a motor fault, or start data collection.
#  File can be found at \ref UI_front.py
#
# \subsection subsection28 Shares
#  This file is akin to a hub for variables. It stores all variables, which then
#  allows all tasks to call on any variable required to complete the request.
#  File can be found at \ref shares.py
#
# \subsection subsection29 Main
#  This file is similar to an engine for tasks. It fires them harmoniously 
#  to avoid any data corruption or interruption. It is easy to have tasks 
#  overlap one another, which causes severe system issues or system freezing.
#  It is crucial to have proper period, and priority designations, so that 
#  they fire off nearly simultaneously without affecting one another.
#  File can be found at \ref main.py
#
# \subsection subsection30 Data Collect Task
#  This tasks has the sole purpose of collecting, the RTP position, RTP velocity,
#  encoder position, encoder velocity, and time ran for the duration of the tests.
#  It compiles the data into a comma separated variable (CSV) file, which can
#  be used to create plots or anything you would do in excel.
#  File can be found at \ref dataCollectTask.py