# -*- coding: utf-8 -*-
''' @file       refund.py
    @brief      Computes correct change for a given purchase.
    @details    Using internal functions, the code will collect data and
                numerically solve for the change return in the fewest 
                denominations possible.
    @author     David Hernandez
    @date       4/11/2021
    @copyright  2020-2021 Hernandez
'''

def getChange(price, payment):
    ''' @brief    This functions will return a correct change calculation.
        @param    price, is an integer representing the price of the item
                  in cents.
        @param    payment, A collection indicating the quantity of each
                  denomination.
    '''
    
    refund = payment
    pmt = [1,5,10,25,100,500,1000,2000]
    ## @brief    List of monetary value in integer form.
    #  @details  List is (from left to right): Pennies, Nickels, Dimes,
    #            Quarters, 1 dollar, 5 dollar, 10 dollar, 20 dollar
    #
    
    fee = price
    
    ## Iterates through each value in payment, deducting the fee until a result
    #  is formed.
    for i in range(len(pmt)):
        difference = min((fee // pmt[i]), refund[i])
        refund[i] -= difference    # Subtracts each payment value from list
        fee -= difference * pmt[i] # Subtracts each payment value from price
        print(fee)

    ## If price due is larger than payment then no change is due.
    if fee > 0:
        return None
    else:
        return refund

# Main program
# This code only runs if the script is executed as a main by pressing play
# but does not run if the script is imported as a module
if __name__=="__main__":
    # Program Initialization

    ## The following examples represent how much change is returned if:

    # The price is greater than payment
    # print(getChange(2901, [0,0,0,0,4,1,0,1]))

    # # The price is exact to payment
    # print(getChange(2900,[0,0,0,0,4,1,0,1]))

    # The price is less than payment
    print(getChange(2500,[0,0,0,0,0,0,0,2]))

