## \page page2 Lab0x02
#  \tableofcontents
#  The following document used the Python language to develop a program that would simulate a reaction test 
#  for the user. Using the Nucleo STM32 board, jumper wires, and a USB-Type A breaker cable, these pieces of equipment
#  were used with a Python program to run several tests. The basis of the system is simple, the program welcomes the user
#  asks for the button on the board to be pressed to start the test, then the LED cycles through some off and on time,
#  and the user is simply meant to press the blue button on the board as quickly as possible when the green LED is on.
#  This simulation was ran via two separate methods to observe the accuracy of the two. More on each method below.
#  \section sec2 Reaction Time: Comparisons
#  This page contains the subsections \ref Lab0x02A.py, \ref Lab0x02B.py, Result Comparisons
#  \subsection subsection2 Reaction with Utime Lab0x02A.py
#  As mentioned in the introduction, this file performs the simulated program for a reaction test. In this method,
#  it uses two functions and the main code. The first function is the callback feature allowing the user to have
#  a feedback when the button on the Nucleo is pressed. The second function simply calculates averages. The main code
#  has three states total: the first is the inital state; this state welcomes the user and provides appropriate 
#  instructions to continue, the second is the LED state; in this state the green LED remains off for two seconds 
#  and turns on the LED for one second, and the third is the reaction state; here the time difference between the 
#  start and button press times are taken and appended to a list. The tests can be done an infinite amount of times
#  and when the user is done, they simply have to press ctrl-c and an average of reaction time tests will be 
#  output for their convenience.
#  It transitions smoothly according to the transition diagram provided below. 
#  The image of the transition diagram can be viewed below.
#     \image html Reaction_Diagram.png width=800cm height=600cm
#  \subsection subsection3 Reaction with Output Compare and Input Capture Lab0x02B.py
#  This method ran similarly to the previous method, however, more control of the system was taken place by
#  incorporating new functions, and timer objects. The first objective was to choose and configure a timer on the
#  Nucleo STM32, we chose timer 2 and added a prescaler and period objects. These objects help determine the 
#  'width of one step' in the figurative time counter period, as well as the time range one 'full staircase' encompasses.
#  The goal for this part of the lab was a time range over two seconds, but without sacrificing too much resoultion 
#  'widght of one step'. The chosen values were the max period for a 16-bit 65535 and a prescaler of 4000. These two 
#  object provided a time range of 3.277 seconds and a resolution of 50 microseconds. This was acceptable as it met
#  the requirements without sacrificing resolution too much. After this, two channels were developed off this timer.
#  One for the output compare and one for the input capture functions. The channels were configured as follows:
#
#  t2ch1 = tim2.channel(1, Timer.OC_TOGGLE, pin=pinLED, callback=OC_Callback)
#
#  t2ch2 = tim2.channel(2, Timer.IC, polarity=Timer.FALLING, pin=PB3, callback=IC_Callback)
#
#  The objects without controlled the LED on/off, associated the correct pins on the board to each channel, and configured 
#  the callback functions to each one. The callbacks allowed the user to press the blue button on the Nucleo, which in turn
#  returned the appropriate feedback to the user. The output compare function returned the data for when the LED was on and 
#  when the user pressed the button, in other words, the users reaction time. The input capture used this data and multiplied 
#  it to the resolution of 50 [us] to properly configure the reaction time. The rest is the same as method 1.
#  The image of the transition diagram can be viewed below.
#     \image html Reaction2_Diagram.png width=800cm height=600cm
#  \subsection subsection4 Result Comparisons
#  The method employed in part A were by far simpler to develop and program. It was shorter, and concise. Part B was a bit
#  more convoluted and required more knowledge to employ compared to part A.
#  For this test program, both were reasonably similar in reaction times, with part B being more fine tuned and precise. 
#  If the time scale were different, then I believe part B would be the superior choice. Though it may require more programming
#  the resolution and accuracy output are beyond the capabilities of the method in part A.
