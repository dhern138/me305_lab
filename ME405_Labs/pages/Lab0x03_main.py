'''@file        Lab0x03_main.py
@brief          This program is the back end that runs commands from the UI.
@details        The UI commands this program when to begin collecting data
                after the user presses the correct keyboard command. It also
                allows the user to stop gather data at any time or let the time
                finish on its own. The data collected are ADC values from the 
                user pressing the blue button the NucleoSTM32 board. This data
                will be processed and sent back to the UI for further evaluation.


@author         David Hernandez
@author         Sydney Lewis
@date           5/2/21
@copyright      2020-2021 Hernandez & Lewis Inc
'''


import pyb
from array import array
from pyb import UART, Timer


pyb.repl_uart(None)

adc = pyb.ADC(pyb.Pin.cpu.A0)
## @brief      Creates access to the pin onboard the NucleSTM32.
#  @details    By putting a jumper from the PC13 (blue button) to
#              PA0 (ADC) it was possible to collect the ADC values
#              by pushing the blue button.
#
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
## @brief   Locates blue button on nucleo
#  @details Associates further commands with this button
#

myVar = 0
## @brief    Initial button press counter
#

def onButtonPressFCN(IRQ_src):
    '''@brief   Commands the button to start the adc counter.
       @param   IRQ_src the command to give the button
                myVar counts amount button is press
                start counts the milliseconds that pass
                since button was pushed.
    '''
    global myVar
    myVar += 1

ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

if __name__=="__main__":
    '''@brief   Initiates program when script is executed only.
    '''
    # Program initialization goes here
    state = 0   # Initial state is the init state

    myuart = UART(2)
    ##@brief Establishes communication link to Nucleo
    #
    times = 0
    ##@brief Creates blank array for times
    #
    period = 20
    sstate = 0
    acdc = array ('H', (0 for index in range (1000)))
    ##@brief Creates blank array for ADC input
    #
    tim2 = Timer(2, freq = 50000)

    
    print('Link Established')
    
    
    while True:
        try:
            
            ## Initial state
            if state == 0:
                if myuart.any() != 0: # Stating value returned
                    val = myuart.readchar()
                    print(val)
                    ##@brief Decodes input ASCII character into integer
                    #
                    print('Data Collection will start on blue button press [HOLD 100ms]')
                    ## If G button is pressed on keyboard it will transition to state 1
                    if val == 71 or val == 103:
                        # Transition to state 1
                        state = 1
                        # Printing transition from S0 to S1
                        print('S0->S1')
                    ## User inputs incorrect letter
                    elif val != 71 or val != 103:
                        print('Wrong button')
                        state = 0
                        print('Return to S0')
                    
            ## Collecion of Data Starts here
            elif state == 1:
                if myuart.any() != 0: # Stating value returned
                    val = myuart.readchar() # Input translated
                    ## If S button is pressed on keyboard it will transition to state 2
                    if val == 83 or val ==115:
                        state = 2
                        # Printing transition from S1 to S2
                        print('S1->S2')
                        print('Data Collection Stopped')

                if myVar==1:
                    print('BUTTON pushed')
                    ADC = adc.read_timed(acdc,tim2)
                    # sstate = 1
                    # if sstate == 1:
                    for i in acdc:
                        if (acdc[-1]-acdc[0]) >= 3600:
                            print('Gathering data...')
                            state = 2 # Updating state for next iteration
                            n=0
                            myVar = 0
                        else:
                            myVar = 1

            ## End of data collection. Write data.
            elif state == 2:
                print('S2')
                ## Print data as CSV in console
                for data in acdc:
                    myuart.write('{:},{:}\r\n'.format(times,data))
                    times += period
                myuart.write('Complete')
                print('Complete')
                # Standby for next iteration
                state = 0 
                myVar = 0
                print('SO again')
            
            else: 
                pass 
            # code to run if state is invalid
            # program should ideally never reach here
            
        except KeyboardInterrupt:
                # This except block catches "Ctrl+C" from the keyboard
                # while (True) loop when desired
                break
