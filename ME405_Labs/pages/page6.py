## \page page6 HW0x02 System Modeling
#  \tableofcontents
#  The following documents are the hand calculations for the dynamics of 
#  a ball on a platform.The figure being discussed can be observed below.
#
#     \image html Ball_Platform.png width=400cm height=300cm
#      Figure 1. Ball and platform model which had its motions analyzed
#
#  The model has two motors that shift the platform with 2 degrees of freedom
#  As mentioned the lab manual, 'The first motor spins about its x-axis but tilts the
#  platform about its y-axis and the second motor spins about its y-axis but tilts the platform about
#  its x-axis'. The goal was to find the equation of motion for the ball and platform using kinetics
#  and kinematics. Many simplifying assumptions were made in order to have a rough system model
#  to work with. Though it may not be precise it should be good enough to provide decent results.
#
#  The analysis can be broken into 5 parts:
#  1. Schematic
#  2. Ball Kinematics
#  3. Motor Torque Relationship
#  4. Equations of Motion
#  5. Unknowns Isolation
#  6. Matrices manipulation
#
#  \section sec Analysis
#  This page contains the subsections \ref Schematic, \ref Ball Kinematics, \ref Motor Torque Relationship, \ref Equations of Motion, 
#  \ref Unknowns Isolation\Matrices manipulation
#  \subsection subsection5 Schematic
#  In this part we had to draw a simplified schematic of the 4-bar linkage of the system model. Due to the complexity
#  of the system in three dimensions, it had to be 'unfurled' in 2D to produce a rough model of the actual dynamics.
#     \image html part1.jpg width=600cm height=800cm
#  \subsection subsection6 Ball Kinematics
#  For part 2, the no slip condition was assumed to ease the analysis for the balls motion on the platform. The expected motions were:
#  by the platform, and by the platform and ball together. 
#     \image html part2.jpg width=600cm height=800cm
#  \subsection subsection7 Motor Torque Relationship
#  For part 3, the relationship between the motor and torque had to be evaluated for future calculations.
#  The amount of moment applied to the platform is related to the amount of torque applied by the motor and the gear ratio.
#  This information is needed for the dynamics of the model to be true.
#     \image html part3.jpg width=600cm height=800cm
#  \subsection subsection8 Equations of Motion
#  The equations of motion are the meat and potatoes of the dynamic model. Since there are two degrees of freedeom,then two
#  equations of motion were expected. The first was considered with the ball and platform together as an isolated system.
#  A free body diagram of the forces and moments was compared to the kinetic diagram of the angular and translational acceleration
#  as well as its mass moment inertias. This image can be seen below.
#     \image html part4.1.jpg width=600cm height=800cm
#
#  The second equation of motion considered the ball alone. Using the same methods employed on the first equation of motion the
#  second was developed as seen below.
#     \image html part4.2.jpg width=600cm height=800cm
#  \subsection subsection9 Unknowns Isolation\Matrices manipulation
#  The fifth and sixth parts were put into one because the sixth part was simply the manipulation of part 5.
#  It took the developed equation of motions and its substitues and placed them in matrix form.
#  This would allow computer software such as MATLAB or Jupyter, to simulate the ball and platform with the developed equations.
#  This first image places all developed equations into one sheet. At the bottom was the cross product for the first
#  equation of motion, found using Symbolab.com
#     \image html part5.1.jpg width=600cm height=800cm
#
#  The following image also used Symbolab.com to evaluate the cross product of the second equation of motion.
#     \image html part5.2.jpg width=600cm height=800cm
#
#  This image incorporates the final results for parts 5 and 6.
#     \image html part6.jpg width=600cm height=800cm
#


