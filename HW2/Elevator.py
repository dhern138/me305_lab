
'''@file        Elevator.py
   @brief       Simulates vertical motion of an elevator
   @details     Implements a finite state machine, shown below, to simulate
                the vertical behavior of an elevator.
                See website at:
                    https://bitbucket.org/dhern138/me305_lab/src/master/HW2/Elevator.py
   @author      David Hernandez
   @date        1/22/21
   @copyright   License info
'''


import time
import random

def motor_cmd(cmd):
    '''@brief   Commands the motor to move or stop
       @param   cmd The command to give the motor 
    '''
    if cmd == 'DOWN':
        print ('Mot down')
    elif cmd == 'UP':
        print ('Mot up')
    elif cmd == 'STOP':
        print ('Mot stop')

def f1_sensor():
    '''@brief   Informs the system if the elevator is on the first floor
    '''
    return random.choice([True,False])  # Randomly returns T or F
    
    
def f2_sensor():
    '''@brief   Informs the system if the elevator is on the second floor 
    '''
    return random.choice([True,False])  # Randomly returns T or F
    
    
def f1_button():
    '''@brief   Calls the elevator to the first floor
    '''
    return random.choice([True,False])  # Randomly returns T or F
    
    
def f2_button():
    '''@brief   Calls the elevator to the second floor
    '''
    return random.choice([True,False])  # Randomly returns T or F
    
    
    
# Main program / test program begin
# This code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module

if __name__=="__main__":
    '''@brief   Initiates program when script is executed only.
    '''
    state = 0   # Initial state is the init state
    
    while True:
        try:
            # Main program code goes here
            if state==0:
                # run state 0 (init) code 
                print ('s0')
                motor_cmd('DOWN') # Command motor to go forward
                state = 1        # Updating state for next iteration
                
            elif state==1:
                # run state 1 (moving down) code 
                 print ('s1')
                 # If we are at the first floor, stop the motor and transition to s2
                 if f1_sensor():
                     motor_cmd('STOP')
                     f1_button()
                     state=2                                
                     
            elif state==2: 
                # run state 2 (stopped at first floor) code 
                 print ('s2')
                 # If f2 button is pressed, start motor and transition to s3
                 if f2_button():
                     motor_cmd('UP')
                     state=3
                  
                 # If f1 button is pressed, the elevator will remain stationary    
                 elif f1_button():
                     motor_cmd('STOP')
                     state=2
            
            elif state==3: 
                # run state 3 (moving up) code 
                 print ('s3')
                 # If we are at the second floor, stop the motor and transition to s4
                 if f2_sensor():
                     motor_cmd('STOP')
                     f2_button()
                     state=4
                 
            elif state==4:
                # run state 4 (stopped at second floor) code 
                 print ('s4')
                 # If f1 button is pressed, start motor and transition to s1
                 if f1_button():
                     motor_cmd('DOWN')
                     state=1     # Updating state for next iteration
                     
                 # If f2 button is pressed, the elevator will remain stationary
                 elif f2_button():
                     motor_cmd('STOP')
                     state=4    
            else: 
                pass 
            # code to run if state is invalid
            # program should ideally never reach here
            
            # Slow does execution of FSM so we can se output in console    
            time.sleep(2)
            
        except KeyboardInterrupt:
                # This except block catches "Ctrl+C" from the keyboard
                # while (True) loop when desired
                break
   
    #Program de-initialization goes here
   
