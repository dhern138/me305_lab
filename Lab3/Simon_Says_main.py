''' @file       Simon_Says_main.py
    @brief      Runs a memory game using Nucleo LED
    @details    Implements a finite state machine, shown below, to simulate
                a memory game using a bank of codes that the user has to
                mimick.
                Here is a snapshot of our diagram:
                    *  \image html ME305_Lab0x03.png width=800cm height=600cm
                    *  \image html commits.jpg width=800cm height=600cm
                    
                    
                See files here:
                    https://bitbucket.org/dhern138/me305_lab/src/master/Lab3/
                    
                See video demonstration of memory game FSM here:
                    https://drive.google.com/file/d/1PUeAhnEbm9qERl_vGNEpI2JYEjd_9Z_Q/view?usp=sharing
                    
    @image      IMAGE_PATH
    @author     Ahkar Kyaw
    @author     David Hernandez
    @date       2/18/21
    @copyright  2020-2021 Kyaw & Hernandez
'''

# import class
from Simon_Says_class import Simon_Says_class

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    task1 = Simon_Says_class(dotTime=100,dashTime=200,charTime=500,slow=2,DBG_flag=True)
    
    while True:
        try:
            task1.run()
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl+c has been pressed')
            break

    # Program de-initialization goes  here
    
    
    
    
    
    
    
    
    
    