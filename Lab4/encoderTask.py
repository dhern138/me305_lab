# -*- coding: utf-8 -*-
''' @file       encoderTask.py
    @brief      Creates a class for encoder's to work with any combination of pins
    @details    The class will allow user to combine any desired timer and channel combo for use with encoder
    @author     David Hernandez
    @date       3/4/21
    @copyright  2020-2021 Hernandez
'''

import shares
import pyb
from pyb import Pin, Timer
from encoderDriver_part2 import encoderDriver_part2


class encoderTask:
    ''' @brief      Class that configures encoder to driver to access its functions.
        @details    Will function with Nucleo board to collect data for interpretation
    '''
    
    # State constants
    S0_INIT                 = 0 # Making a constant 0 with S0_INIT
    S1_ZERO_POSITION        = 1
    S2_PRINT_ENC1_POSITION  = 2
    S3_PRINT_ENC1_DELTA     = 3
    S4_COLLECT_DATA         = 4
    S5_STOP_COLLECTING      = 5

    def __init__(self):  # Constructor to initialize attributes
        ''' @brief      Initialization of the system
            @details    All methods and attributes associated with the encoder class
            @param      taskNum lets user know which task is being run
            @param      period is the operation frequency of the encoder
            @param      DBG_flag helps debug the class
        '''
        
        self.n = 0

        ## Pin initialization
        self.PB6 = Pin(pyb.Pin.cpu.B6)
        ## @brief Locates respective pin on the Nucleo board
        #  @details     Locates PinB6 for encoder
        self.PB7 = Pin(pyb.Pin.cpu.B7)
        ## @brief Locates respective pin on the Nucleo board
        #  @details     Locates PinB6 for encoder
        self.PC6 = Pin(pyb.Pin.cpu.C6)
        ## @brief Locates respective pin on the Nucleo board
        #
        self.PC7 = Pin(pyb.Pin.cpu.C7)
        ## @brief Locates respective pin on the Nucleo board
        #

        ## Encoder 1 timer set-up
        self.TIM4 = Timer(4, period=0xFFFF, prescaler=0)
        self.TIM4.channel(1, Timer.ENC_AB, pin = self.PB6)
        self.TIM4.channel(2, Timer.ENC_AB, pin = self.PB7)
        
        ## Encoder 2 timer set-up when required
        self.TIM8 = Timer(8, period=0xFFFF, prescaler=0)
        self.TIM8.channel(1, Timer.ENC_AB, pin = self.PC6)
        self.TIM8.channel(2, Timer.ENC_AB, pin = self.PC7)
        
        ## Encoder set up for Encoder 1 and Encoder 2
        self.Enc_1 = encoderDriver_part2(self.PB6, self.PB7, 4)
        self.Enc_2 = encoderDriver_part2(self.PC6, self.PC7, 8)

        ## ASCII CODES FOR:
        # G;g = 71;103
        # S;s = 83;115
        # Z;z = 90;122
        # P;p = 80;112
        # D;d = 68;100


    def run(self):
        ''' @brief runs the encoderTask when called from main
        '''
        # thisTime = utime.ticks_us()
        
        # if utime.ticks_diff(thisTime, self.nextTime) >= 0:
        #      ## Timestamp for the next iteration of the task
        #      self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
        #      self.runs += 1
        #      if self.DBG_flag:
        #          print('T' + str(self.taskNum) + ': S' + str(shares.currentState) + ": R" + str(self.runs))
        
        shares.encoderPosition = self.Enc_1.update(shares.encoderPosition)
        print('encoderPosition'+str(shares.encoderPosition))
        #shares.encoder2Position = self.Enc_2.update(shares.encoder2Position)

        if shares.ENCODERstate == 0:
            pass


        # User pressed Z, Change state and reset position of encoder
        elif shares.ENCODERstate == 1:
            shares.encoderPosition = self.Enc_1.set_position(0)



        # User pressed P, Change state and print encoder's current position
        elif shares.ENCODERstate == 2:
            ## Retrieves data from driver and sends to shares file
            # shares.encoderPosition = self.Enc_1.get_position()
            print(shares.encoderPosition)
            ## @brief Sends Encoder 1 position to shares
            #



        # User pressed D, Change state and print encoder's delta
        elif shares.ENCODERstate == 3:
            # shares.delta_1 = self.Enc_1.get_delta()
            pass
        if shares.BACKstate == 4:
            pass
        
        if shares.BACKstate == 5:
            pass
