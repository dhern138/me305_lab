## \page page1 Week 1
#  \tableofcontents
#  The following documents are the initial programs to create a 'handshake' between
#  the user interface and back end program to read and write to each other.
#  \section sec Part 1: Extending Interface
#  This page contains the subsections \ref UI_front_part1.py and \ref UI_backTask_part1.py.
#  \subsection subsection1 The first UI_front_part1.py
#  The user interface read the data from the back end. Then used it to create plots and a CSV file. See \ref UI_front_part1.py.
#  The plot image of the data and transition diagram can be viewed in this file below.
#     \image html main_diagram.png width=800cm height=600cm
#  Initial Task Diagram for week 3:
#     \image html Task_Diagram.png width=800cm height=600cm
#  This sinwave plot was created using the read and write of UART with given equation:
#     \image html sinwave.png width=400cm height=300cm
#  \subsection subsection2 The second UI_backTask_part1.py
#  The back end program that calculated data via a provided equation and return
#  the values to the user interface by writing them using UART. See \ref UI_backTask_part1.py.
#
#
# \page page2 Week 2
#  \tableofcontents
#  The purpose is to gather data from an encoder by manually rotating the shaft and collecting the data.
#  The following documents builds on the previous files with the addition of a new Task, Driver,
#  and shares files.
#  \section sec2 Part 2: Incremental Encoders
#  This page contains the subsections \ref UI_front_part2.py and \ref UI_backTask_part2.py and
#  \ref encoderTask.py and \ref encoderDriver_part2.py and \ref shares.py and \ref main_part2.py
#  For more info regarding details on each file see page \ref advanced "advanced section"
#  \subsection subsection3 The first UI_front_part2.py
#  The user interface serially reads the written data from the back end task.
#  Then used the data to create plots and a CSV file from the gather encoder data. See \ref UI_front_part2.py.
#  Updated Transition Diagram for week 2:
#     \image html W2_Transition_Diagram.png width=800cm height=600cm
#  Updated Task Diagram for week 2:
#     \image html W2_Task_Diagram.png width=800cm height=600cm
#  Plot showing that encoder reads positive and negative values:
#     \image html encoderDiagram.png width=400cm height=300cm
#  \subsection subsection4 The second UI_backTask_part2.py
#  A finite state machine (FSM) that would interpret key presses and gather data accordingly.
#  Main purpose was to gather data from an encoder by 'Talking' with encoderTask and shares files.
#  Then it would serially write the data to the user interface. See \ref UI_backTask_part2.py
#  \subsection subsection5 The third encoderTask.py
#  This task configures the pin and timer to use physically and via the encoderDriver.
#  It uses a FSM to collect, set, and print data via user interface. See \ref encoderTask.py.
#  \subsection subsection6 The fourth encoderDriver_part2.py
#  This driver class allows the set up of any pin timer combination for an encoder.
#  Then uses internal functions to update, set, or print the angle position of selected encoder. See \ref encoderDriver_part2.py.
#  \subsection subsection7 The fifth shares.py
#  The file allows the sharing of variables among all tasks. See \ref shares.py
#  \subsection subsection8 The sixth main_part2.py
#  This program runs the tasks at set intervals (periods). See \ref main_part2.py.
#
#
# \page page3 Week 3
#  \tableofcontents
#  This third part of this lab was to run the motor using input speed and proportional gain (Kp)
#  values.  By modifying most of the previous drivers and tasks, it allowed the user to simulate
#  the motor reaching steady-state. Also, it allowed for observation of the oscillation because of
#  under/overshoot. Using the modifying gain factor the user was able to mitigate the noise.
#  Due to frictions and other variables, the system was not ideal, but provided enough data
#  to prove the function of the proportional gain factor.  The attempts at fixing the noise can
#  be viewed in plots below. 
#  \section sec3 Part 3: Motor Control
#  This page contains the subsections \ref UI_front_part3.py and \ref UI_backTask_part3.py and
#  \ref controllerTask_part3.py and \ref encoderDriver_part3.py and \ref shares.py and \ref main_part3.py
#  \ref MotorDriver.py and \ref closedLoop_part3.py
#  For more info regarding details on each file see page \ref advanced "advanced section"
#  \subsection subsection9 The first UI_front_part3.py
#  The user interface serially reads the written data from the back end task.
#  Then used the data to create plots and a CSV file from the gathered motor data. See \ref UI_front_part3.py.
#  Updated Transition Diagram for week 3:
#     \image html W3_Transition_Diagram.png width=800cm height=600cm
#  Updated Task Diagram for week 3:
#     \image html W3_Task_Diagram.png width=800cm height=600cm
#  Using a set speed of 1500 rpm, an attempt was made to correct the noise via Kp:
#     \image html 1st_Trial.png width=400cm height=300cm
#     \image html 2nd_Trial.png width=400cm height=300cm
#     \image html 3rd_Trial.png width=400cm height=300cm
#     \image html 4th_Trial.png width=400cm height=300cm
#     \image html 5th_Trial.png width=400cm height=300cm
#  \subsection subsection10 The second UI_backTask_part3.py
#  A finite state machine (FSM) that would interpret key presses and gather data accordingly.
#  Main purpose was to gather data from a motor by 'Talking' with controllerTask and shares files.
#  Then it would serially write the data to the user interface. See \ref UI_backTask_part3.py
#  \subsection subsection11 The third controllerTask_part3.py
#  This task configures the pin and timer to use physically and via the encoderDriver.
#  It uses a FSM to collect, set, and print data of the motor via user interface.
#  Added state for Kp and functions to set, print and input new Kp value. See \ref controllerTask_part3.py.
#  \subsection subsection12 The fourth encoderDriver_part3.py
#  This driver class allows the set up of any pin timer combination for an encoder. See \ref encoderDriver_part3.py.
#  Then uses internal functions to update, set, or print the speed of selected motor.
#  \subsection subsection13 The fifth shares.py
#  The file allows the sharing of variables among all tasks. Added more variables. See \ref shares.py.
#  \subsection subsection14 The sixth main_part3.py
#  This program runs the tasks at set intervals (periods). Change of task names. See \ref main_part3.py
#  \subsection subsection15 The seventh MotorDriver.py
#  This driver class allows the set up of any pin timer combination for a motor.
#  Then uses internal functions to enable, disable, and, according to user input, spin clockwise
#  or counterclockwise. See \ref MotorDriver.py
#  \subsection subsection16 The eighth closedLoop_part3.py
#  This driver class allows the correction of the power output of the motor by introducing 
#  a proportional gain (Kp) coefficient to be used with the corrected speed value. The purpose of
#  Kp is to act as a control for the user, as one would turn a knob to lower or raise the volume
#  of a television set. This driver class allowed the user to set Kp to any value, and print it. See \ref closedLoop_part3.py.
#
#
# \page page4 Week 4
#  \tableofcontents
#  This final part of this lab was to use provided referenced data and to attempt to track
#  the same plot created by the referenced data by modifying most of the previous drivers and tasks.
#  The most difficult portion was to have the user interface extract the data from the CSV file
#  and add it to the shares file for use. Then using the speed data in place of a user set speed,
#  would in theory have the motor follow the speed pattern and by integrating should also output the 
#  angle position following the referenced pattern as well. The user only had to modify the proportional gain (Kp)
#  and the new gain (Ki) to produce more accurate results.
#  \section sec4 Part 4: Reference Tracking
#  This page contains the subsections \ref UI_front_part4.py, \ref UI_backTask_part4.py,
#  \ref controllerTask_part4.py, \ref encoderDriver_part4.py, \ref shares.py, \ref main_part4.py
#  \ref MotorDriver.py, and \ref closedLoop_part4.py
#  \subsection subsection17 The first UI_front_part4.py
#  The user interface serially reads the written data from the back end task.
#  Also, the referenced data was read through the front end and stored in shares file to allow
#  the tasks to use the referenced data. See \ref UI_front_part4.py.
#  Then used the data to create plots and a CSV file from the gathered motor data.
#  The final transition diagram can be seen below:
#     \image html Transition_Diagram.png width=800cm height=600cm
#  The final tasks diagram for the overall system:
#     \image html Tasks_Diagram.png width=800cm height=600cm
#  The plotted data using the referenced speed:
#     \image html Tracking.png width=400cm height=300cm
#  The hand calculations that led to theoretical values for gains:
#     \image html handcalcs1.jpg width=800cm height=1000cm
#     \image html handcalcs2.jpg width=800cm height=600cm
#  \subsection subsection18 The second UI_backTask_part4.py
#  A finite state machine (FSM) that would interpret key presses and gather data accordingly.
#  Main purpose was to gather data from a motor by 'Talking' with controllerTask and shares files.
#  Then it would serially write the data to the user interface. Added state for Ki, and appended
#  position of the encoder to also be written over to the user interface. See \ref UI_backTask_part4.py
#  \subsection subsection19 The third controllerTask_part4.py
#  This task configures the pin and timer to use physically and via the encoderDriver.
#  It uses a FSM to collect, set, and print data of the motor via user interface.
#  Added state for Ki and function to set, and input new Ki values. See \ref controllerTask_part4.py.
#  \subsection subsection20 The fourth encoderDriver_part4.py
#  This driver class allows the set up of any pin timer combination for an encoder.
#  Then uses internal functions to update, set, or print the speed of selected motor.
#  Added calculations to configure position to degrees, and speed to RPM. See \ref encoderDriver_part4.py.
#  \subsection subsection21 The fifth shares.py
#  The file allows the sharing of variables among all tasks. Added even more variables. See \ref shares.py
#  \subsection subsection22 The sixth main_part4.py
#  This program runs the tasks at set intervals (periods). Change of task names. See \ref main_part4.py.
#  \subsection subsection23 The seventh MotorDriver.py
#  This driver class allows the set up of any pin timer combination for a motor.
#  Then uses internal functions to enable, disable, and, according to user input, spin clockwise
#  or counterclockwise. See \ref MotorDriver.py.
#  \subsection subsection24 The eighth closedLoop_part4.py
#  This driver class allows the correction of the power output of the motor by introducing 
#  a proportional gain (Kp) coefficient to be used with the corrected speed value. The purpose of
#  Kp is to act as a control for the user, as one would turn a knob to lower or raise the volume
#  of a television set. This driver class allowed the user to set Kp to any value, and print it.
#  Added Ki to calculation to smooth the vibrational noise by using the position change and factor
#  Ki. Also, added functions to set new Ki value. See \ref closedLoop_part4.py.