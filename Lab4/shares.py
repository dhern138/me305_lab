# -*- coding: utf-8 -*-
''' @file       shares.py
    @brief      Baak of variables used by tasks to store and retrieve data.
    @details    Can be used across other files and classes
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''


encoderPosition     = 0
## @brief array of up-to-date encoder position
# #

encoder2Position       = 0
## @brief Retrieves current position of encoder 2
#

delta_1      = 0
## @brief Retrieves current delta of encoder 1
#

delta_2      = 0
## @brief Retrieves current delta of encoder 2
#
gain = 0
## @brief Desired user gain
#
ki = 0
## @brief Desired user position gain
#
reset = 0
## @brief Sets encoder to desired position
#
tref = 0
speedref = 0
## @brief Desired user speed
#
posref = 0
## @brief Desired user position
#
tcsv = 0
## @brief Time from CSV file
#
scsv = 0
## @brief Speed from CSV file
#
pcsv = 0
## @brief Position from CSV file
#
ENCODERstate = 0
## @brief       Current state of class encoderTask 
#  @details     Helps transition between states for each button pressed
#

BACKstate = 0
## @brief       Current state of class encoderTask 
#  @details     Helps transition between states for each button pressed
#