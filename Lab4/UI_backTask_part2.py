''' @file       UI_backTask_part2.py
    @brief      Collects encoder data using the tasks and drivers.
    @details    The encoder driver sets up the functions and calculations
                required to evaluate correct angle positions of the encoder
                and allows the encoderTask to pull that information from any
                desired pin and timer combination. This data is then collected
                when called by user interface via the backTask. Then each line
                of data is read serially over the UART to the PC.
    @author     David Hernandez
    @date       3/4/21
    @copyright  2020-2021 Hernandez
'''

import utime
import pyb
from array import array
from pyb import UART
import shares

pyb.repl_uart(None)

class UI_backTask_part2:
    ''' @brief      A task class that writes gathered data to user interface for processing.
        @details    When user interface commands are pressed, this task will work with other task
                    to gather any data user demands. Then using UART will write the data over to
                    the front end for processing.
    '''
    # Static Variables - belong to the class, but are not objects of the class
    # like task1()
    
    # State constants
    S0_INIT = (0) # Making a constant 0 with S0_INIT
    S1_ZERO_POSITION       = 1
    S2_PRINT_ENC1_POSITION = 2
    S3_PRINT_ENC1_DELTA    = 3
    S4_COLLECT_DATA        = 4
    S5_STOP_COLLECTING     = 5


    def __init__(self):  # Constructor to initialize attributes
    # Current state of the finite state machine (FSM)
    # This is not your state 0
        ## The current state of FSM
        
        ## Counts the number of runs of our task
        self.runs = 0

        self.n = 0
        
        self.myuart = UART(2)
        ##@brief Establishes communication link to Nucleo
        #
        self.times = array('f',[0])
        ##@brief Creates blank array for times
        #
        self.values = array('f',[0])
        ##@brief Creates blank array for values
        #
        print('Link Established')
        
    def run(self):
        # thisTime = utime.ticks_us()
        
        # if utime.ticks_diff(thisTime, self.nextTime) >= 0:
        #      ## Timestamp for the next iteration of the task
        #      self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
        #      self.runs += 1
        #      if self.DBG_flag:
        #          print('T' + str(self.taskNum) + ': S' + str(shares.currentState) + ": R" + str(self.runs))
        
        ## Initial state
        if shares.ENCODERstate == self.S0_INIT:
            ##@brief Decodes input ASCII character into integer
            #
            if self.myuart.any() != 0:
                self.keyboardInput()
                pass
                
        ## User presses Z, zero out encoder position
        elif shares.ENCODERstate == 1:
            if self.myuart.any() != 0:
                self.keyboardInput()
                pass
            # shares.reset = input('Please input desired position:')
            # self.ENCODER_transitionTo(self.S0_INIT)

        ## User presses P, print out up-to-date encoder position
        elif shares.ENCODERstate == 2:
            if self.myuart.any() != 0:
                self.keyboardInput()
                pass
            # print('Current position = ' + str(shares.pos_1))
            #self.transitionTo(self.S2_PRINT_ENC1_POSITION)
            # self.myuart.write('{:}'.format(shares.pos_1))
            # self.ENCODER_transitionTo(self.S0_INIT)


        ## User presses D, print out encoder difference in recent positions
        elif shares.ENCODERstate == 3:
            if self.myuart.any() != 0:
                self.keyboardInput()
                pass
            #self.transitionTo(self.S3_PRINT_ENC1_DELTA)
            # self.myuart.write('{:}'.format(shares.delta_1))
            # self.ENCODER_transitionTo(self.S0_INIT)

        else:
            pass

        if shares.BACKstate == self.S0_INIT:
            pass
        ## If G button is pressed on keyboard it will transition to state 1
        elif shares.BACKstate == 4:
            if self.myuart.any() != 0:
                self.keyboardInput()
            print('Data Collection Started')
            # Printing transition from S0 to S1
            print('S4->S5')
            self.currentTime = utime.ticks_ms()
            ## @brief Current timer [ms]
            #
            #print('currentTime = '+str(self.currentTime))
            self.deltaTime = utime.ticks_diff(self.currentTime,self.startTime)
            ##@brief Difference in timer [s]
            #
            print('deltaTime = '+str(self.deltaTime))
            #print('deltaTime%1000 = '+str(self.deltaTime%500))


            ## Data collection complete after 30 seconds
            if self.deltaTime >= 30000:
                print('30 seconds of data')
                self.BACK_transitionTo(self.S5_STOP_COLLECTING) # Updating state for next iteration
                self.n = 0


            # The amount of inputs into equation and array.
            elif 45 <= self.deltaTime%50 <= 55:
                self.times.append(self.deltaTime)
                # print(times)
                self.values.append(shares.encoderPosition)
                #print('values = ' +str(self.values))
                ##@brief Output of equation using arrayed time
                #
            pass


        ## End of data collection. Write data.
        elif shares.BACKstate == 5:
            if self.myuart.any() != 0:
                self.keyboardInput()
            print('S5 Retrieving data..')


            ## Print data as CSV in console
            while self.n!=len(self.times):
                self.myuart.write('{:},{:}\r\n'.format(self.times[self.n],self.values[self.n]))
                self.n += 1

            self.myuart.write('Complete')
            #self.myuart.write('Complete')
            print('Complete')
            # Standby for next iteration
            self.BACK_transitionTo(self.S0_INIT)
            print('SO again')
            pass

        else: 
            pass 
      # code to run if state is invalid
      # program should ideally never reach here

    def keyboardInput(self):
        self.val = self.myuart.readchar() # store input from Spyder
        if self.val == 90 or self.val == 122: # if input is 'Z' or 'z'
            self.ENCODER_transitionTo(1) # updating state for next iteration
        elif self.val == 80 or self.val == 112: # if input is 'P' or 'p'
            self.ENCODER_transitionTo(2) # updating state for next iteration
        elif self.val == 68:# or self.val == 100: # if input is 'D' or 'd'
            self.ENCODER_transitionTo(3) # updating state for next iteration
        elif self.val == 71 or self.val == 103: # if input is 'G' or 'g'
            self.startTime = utime.ticks_ms() # store the start time in which data collection begins
            self.BACK_transitionTo(4) # updating state for next iteration
        elif self.val == 83 or self.val == 115: # if input is 'S' or 's'
            self.BACK_transitionTo(5) # updating state for next iteration
        else:
            pass
        
    def BACK_transitionTo(self, newState):
        """ @brief          Function to help change state of the UI_task related class
            @param newState Input variable, new State that we want to move to.
        """
        print('S{:}->S{:}'.format(shares.BACKstate,newState)) 
        shares.BACKstate = newState 
        
    def ENCODER_transitionTo(self, newState):
        """ @brief          Function to help change state of the CON_task related class
            @param newState Input variable, new State that we want to move to.
        """
        print('S{:}->S{:}'.format(shares.ENCODERstate,newState))
        shares.ENCODERstate = newState 