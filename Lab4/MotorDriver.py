# -*- coding: utf-8 -*-
''' @file       MotorDriver.py
    @brief      This class create a general set up for any motor to be configured.
    @details    Will work in in conjunction with the other tasks to collect
                motor speed and other parameters.
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''


import pyb
from pyb import Pin, Timer

class MotorDriver:
    ''' @brief This class will initialize a link with any motor
    '''
    
    def __init__ (self, nSLEEP_pin, CHA, CHB, IN1_pin, IN2_pin, timer):
        ''' @brief  Creates a motor driver by initializing GPIO
                    pins and turning the motor off for safety.
            @param  nSLEEP_pin A pyb.Pin object to use as the enable pin.
            @param  IN1_pin A pyb.Pin object to use as the input to half bridge 1.
            @param  IN2_pin A pyb.Pin object to use as the input to half bridge 2.
            @param  timer A pyb.Timer object to use for PWM generation on
                    IN1_pin and IN2_pin. 
        '''


        pyb.repl_uart(None)
        ## @brief  Prevents any print info to be placed into spyder
        #

        ## Object of the timer
        self.TIM3 = timer
        ## @brief       Sets up generic timer input to be called on by a task
        #
        ## Sleep pin object
        self.nSLEEP = nSLEEP_pin
        # @brief        Locates sleep pin on board to enable and disable motor
        #
        ## Object of pins to connect to motor
        self.IN1 = IN1_pin
        ## @brief       Sets up generic pin input to be called on by a task
        #  @details     First pin input for encoder
        #
        self.IN2 = IN2_pin
        ## @brief       Sets up generic pin input to be called on by a task
        #  @details     First pin input for encoder
        #


    ## Functions to turn motor on and off and run at select speeds
    def enable (self):
        ''' @ brief     Turns on motor for operation
        '''
        print ('Enabling Motor')
        self.nSLEEP.high()
    
    def disable (self):
        ''' @brief      Turns off motor operation
        '''
        print ('Disabling Motor')
        self.nSLEEP.low()
    
    def set_duty (self, duty):
        ''' @brief      This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty A signed integer holding the duty
                        cycle of the PWM signal sent to the motor 
        '''
        self.duty = duty
        if self.duty > 0:
            self.IN1.pulse_width_percent(self.duty)
            self.IN2.pulse_width_percent(0)
        elif self.duty < 0:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(abs(self.duty))
        elif self.duty == 0:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(0)
