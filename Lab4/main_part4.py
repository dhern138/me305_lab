# -*- coding: utf-8 -*-
''' @file       main_part4.py
    @brief      Runs controllerTask and backendTask for both encoder and motor.
    @details    Will call on both tasks to run simultaneously
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''

from controllerTask_part4 import controllerTask_part4
from UI_backTask_part4 import UI_backTask_part4

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    task1 = controllerTask_part4(500000)
    ## @Brief       This task runs the encoder and motor
    #  @details     Will runs the functions and class of the
    #               encoder and motor periodically
    #  @param       Period of this task
    #
    task2 = UI_backTask_part4(1000)
    ## @Brief       This task collects data on the encoder and motor
    #  @details     Will runs the functions and class of the
    #               encoder and motor periodically
    #  @param       Period of this task
    #

    # USER Interface commands
    print("\nHello, please view commands below")
    print("Press 'z' to zero the encoder 1 position")
    print("Press 'p' to print out the encoder 1 position")
    print("Press 'd' to print out the encoder 1 delta")
    print("Press 'g' to collect encoder 1 data for 30 seconds")
    print("Press 's' to end data collection prematurely")
    print("Press 'm' to print gain constant")
    print("Press 'k' to set gain constant")


    while True:
        try:
            ## Runs both task 'Simultaneously'
            task1.run()
            task2.run()


        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed, Program ended. Goodbye')
            break