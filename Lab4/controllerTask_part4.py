# -*- coding: utf-8 -*-
''' @file       controllerTask_part4.py
    @brief      Creates a class to run a motor and encoder that can track a given reference profile.
    @details    The class will use provided data to run motors at given speed and integrate simultaneously
                providing position data as well. Using mainly the speed data I was trying to create an array
                that the reference speed would follow, then integrate those values with the difference in time
                to get position reference via integration and unit conversion.
                The user simply had to put in desired gain inputs. The code works flawlessly, however, the 
                transitioning of the reference data did not go as planned, thus no position data was obtained. 
                This left both plots looking mediocre. It is a simple fix. Only need to transfer the speed 
                reference data.
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''

import shares
import pyb
from pyb import Pin, Timer
from encoderDriver_part4 import encoderDriver_part4
from MotorDriver import MotorDriver
from closedLoop_part4 import closedLoop_part4
import utime


class controllerTask_part4:
    ''' @brief      Class that will control all encoder and motor operations for given referenced speed with Kp and Ki inputs.
        @details    Will function with Nucleo board to collect data for interpretation
                    from both encoders and motors. It will collect data via the encoderDriver
                    MotorDriver, and closedLoop files. Data provided is position, speed,
                    power output, and gain constant.
    '''


    # State constants
    S0_INIT                 = 0 # User PRESSES:
    S1_ZERO_POSITION        = 1 # Z
    S2_PRINT_ENC1_POSITION  = 2 # P
    S3_PRINT_ENC1_DELTA     = 3 # D
    S4_COLLECT_DATA         = 4 # G
    S5_STOP_COLLECTING      = 5 # S
    S6_GET_KP               = 6 # M
    S7_SET_KP               = 7 # K
    S8_SET_KI               = 8 # I


    def __init__(self, task_period):  # Constructor to initialize attributes
        ''' @brief      Initialization of the system
            @details    All methods and attributes associated with the controllerTask class
            @param      task_period is the operation frequency of the encoder
        '''
        
        self.n = 0
        ## @brief   Initializes the count to a small loop to slow down display
        #

        ## Channel pins
        self.CH1 = 1
        self.CH2 = 2
        self.CH3 = 3
        self.CH4 = 4

        ## Pin initialization for Encoders
        self.PB6 = Pin(pyb.Pin.cpu.B6)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinB6 for encoder
        #
        self.PB7 = Pin(pyb.Pin.cpu.B7)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinB7 for encoder
        #
        self.PC6 = Pin(pyb.Pin.cpu.C6)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinC6 for encoder
        #
        self.PC7 = Pin(pyb.Pin.cpu.C7)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinC7 for encoder
        #
        ## Pin inialization for Motors
        self.TIM3 = Timer(3, freq = 20000)
        ## @brief Sets up timer for motor pins
        #
        self.pin_IN1 = self.TIM3.channel(1, mode=Timer.PWM, pin = pyb.Pin.cpu.B4);
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinB4 for motor
        #
        self.pin_IN2 = self.TIM3.channel(2, mode=Timer.PWM, pin = pyb.Pin.cpu.B5);
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinB5 for motor
        #
        self.pin_IN3 = self.TIM3.channel(3, mode=Timer.PWM, pin = pyb.Pin.cpu.B0);
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinB0 for motor
        #
        self.pin_IN4 = self.TIM3.channel(4, mode=Timer.PWM, pin = pyb.Pin.cpu.B1);
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinB1 for motor
        #
        ## Sleep pin to enable motor
        self.pin_nSLEEP = Pin(pyb.Pin.cpu.A15, mode=Pin.OUT_PP, value=0)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Locates PinA15 for motor
        #

        ## Encoder 1 timer set-up
        self.TIM4 = Timer(4, period=0xFFFF, prescaler=0)
        self.TIM4.channel(1, Timer.ENC_AB, pin = self.PB6)
        self.TIM4.channel(2, Timer.ENC_AB, pin = self.PB7)
        
        ## Encoder 2 timer set-up when required
        self.TIM8 = Timer(8, period=0xFFFF, prescaler=0)
        self.TIM8.channel(1, Timer.ENC_AB, pin = self.PC6)
        self.TIM8.channel(2, Timer.ENC_AB, pin = self.PC7)
        
        ## Encoder set up for Encoder 1 and Encoder 2
        self.Enc_1 = encoderDriver_part4(self.PB6, self.PB7, 4)
        ## @brief   Sets the first encoder up with pins B6 and B7 with timer 4
        # @details     Calls on encoderDriver class to input the required arguments into constructor.
         # @param       self.PB6 calls on pin C6 to get into first pin channel
         # @param       self.PB7 calls on pin C7 to get into second pin channel
         # @param       4 is the timer being used for the pin and channel inputs
        #
        self.Enc_2 = encoderDriver_part4(self.PC6, self.PC7, 8)
        ## @brief   Sets the second encoder up with pins C6 and C7 with timer 8
         # @details     Calls on encoderDriver class to input the required arguments into constructor.
         # @param       self.PC6 calls on pin C6 to get into first pin channel
         # @param       self.PC7 calls on pin C7 to get into second pin channel
         # @param       8 is the timer being used for the pin and channel inputs
        #
        ## Motor set up for Motor 1 and Motor 2
        self.moe1 = MotorDriver(self.pin_nSLEEP, self.CH1, self.CH2, self.pin_IN1, self.pin_IN2, 3)
        ## @brief       Sets the first motor up.
         # @details     Call on MotorDriver class to input the required arguments into constructor.
         # @param       self.pin_nSlEEp enables and disables the motor.
         # @param       self.CH1 Calls on channel 1
         # @param       self.CH2 Calls on channel 2
         # @param       self.pin_IN1 calls on pin PB4 to get into first pin channel
         # @param       self.pin_IN2 calls on pin PB5 to get into second pin channel
         # @param       3 is the timer being used for the pin and channel inputs
        #
        self.moe2 = MotorDriver(self.pin_nSLEEP, self.CH3, self.CH4, self.pin_IN3, self.pin_IN4, 3)
        ## @brief       Sets the second motor up.
         # @details     Call on MotorDriver class to input the required arguments into constructor.
         # @param       self.pin_nSlEEp enables and disables the motor.
         # @param       self.CH3 Calls on channel 3
         # @param       self.CH4 Calls on channel 4
         # @param       self.pin_IN3 calls on pin PB0 to get into first pin channel
         # @param       self.pin_IN4 calls on pin PB1 to get into second pin channel
         # @param       3 is the timer being used for the pin and channel inputs
        #

        print('Encoder Initialized')

        ## Referenced data would go here.
        shares.speedref=300
        ## @brief Data imported from CSV file for speed profile
        #
        ## Input gain values
        shares.gain = float(input('Please input Speed Gain between 0.1 - 0.6 [%/RPM]:'))
        ## @brief Allows user to input desired speed gain constant [%/RPM]
        #
        shares.ki = float(input('Please input Position Gain between 0.1 - 0.6 [%/DEG]:'))
        ## @brief Allows user to input desired position gain constant [%/RPM]
        #

        ## Enable the motor driver
        self.moe1.enable()
        #self.moe2.enable()

        ## Sends input user values to closedloop class
        self.Cloop = closedLoop_part4()
        ## @brief Instance that calls on closedLoop class
        #
        self.Cloop.set_Kp(shares.gain)
        ## @brief Sets the gain constant value input by user into the closedLoop class
        #
        self.Cloop.set_Ki(shares.ki)
        ## @brief Sets the position gain constant value input by user into the closedLoop class
        #

        self.lastTime = utime.ticks_us()
        ## @brief Timestamp of previous iteration in microseconds
        #
        self.period = task_period
        ## @brief Period for the controllerTask in microseconds
        #
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        ## @brief Time for the next iteration in microseconds
        #


        
    def run(self):
        ''' @brief runs the controllerTask when called from main
        '''

        thisTime = utime.ticks_us()
        ## @brief Current time in microseconds
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)


        ## Recieves data from selected timer/pin set-up and return updated positions
        shares.encoderPosition,shares.delta_1,shares.tref  = self.Enc_1.update(shares.encoderPosition)
        #shares.encoder2Position,shares.delta_2 = self.Enc_2.update(shares.encoder2Position)


        shares.posref = (shares.speedref*shares.tref)*(6/1000000)
        ## @brief       Position reference using integration of speedref [deg]
        #  @details     Uses time and speed data from CSV file to find position
        #


        ## Motor operations
        # Updates PWM status
        self.PWM = self.Cloop.update(shares.speedref, shares.delta_1, shares.posref, shares.encoderPosition)
        ## @brief       Calls on closedLoop class to update the arguments in the constructor
         # @details     Sets the users input speed reference and measured speed from the encoderDriver
         #              into the arguments of the update function within the closedLoop class
         # @param       shares.speedref is the user speed input
         # @param       shares.delta_1 is the measured speed taken from the encoderDriver
        #
        # Set the duty cycle to PWM
        self.moe1.set_duty(self.PWM)
        #self.moe2.set_duty(int(shares.gain))


        ## Slow down print output
        if self.n%5000>=4999:
            print('Posmeas', shares.encoderPosition)
            print('POSref',shares.posref)
            print('Speedref', shares.speedref)
            print('Measured Speed [RPM]', shares.delta_1)
            print('Gain [%/RPM]', shares.gain)
            print('Power', self.PWM)
        self.n+=1


        ## Beginning of FSM
        if shares.ENCODERstate == self.S0_INIT:
            pass


        # User pressed Z, Change state and reset position of encoder
        elif shares.ENCODERstate == self.S1_ZERO_POSITION:
            shares.encoderPosition = self.Enc_1.set_position(shares.reset)
            shares.encoder2Position = self.Enc_2.set_position(shares.reset)
            shares.ENCODERstate = self.S0_INIT

        # User pressed P, Change state and print encoder's current position
        elif shares.ENCODERstate == self.S2_PRINT_ENC1_POSITION:
            shares.encoderPosition = self.Enc_1.get_position()
            shares.encoder2Position = self.Enc_2.get_position()
            shares.ENCODERstate = self.S0_INIT


        # User pressed D, Change state and print encoder's delta
        elif shares.ENCODERstate == self.S3_PRINT_ENC1_DELTA:
            shares.delta_1 = self.Enc_1.get_delta()
            shares.delta_2 = self.Enc_2.get_delta()
            shares.ENCODERstate = self.S0_INIT

        # User pressed M, Change state and prints gain constant Kp
        elif shares.ENCODERstate == self.S6_GET_KP:
            shares.gain = self.Cloop.get_Kp()
            print('Gain Constant Kp:', shares.gain)
            shares.ENCODERstate = self.S0_INIT

        # User pressed K, Change state and lets user set new Kp
        elif shares.ENCODERstate == self.S7_SET_KP:
            shares.gain = self.Cloop.set_Kp(float((input('Please input new Speed Gain between 0.1-0.6 [%/RPM]:'))))
            shares.ENCODERstate = self.S0_INIT

        # User pressed I, Change state and lets user set new Kp
        elif shares.ENCODERstate == self.S8_SET_KI:
            shares.gain = self.Cloop.set_Ki(float((input('Please input new Speed Gain between 0.1-0.6 [%/deg]:'))))
            shares.ENCODERstate = self.S0_INIT


        ## Back end FSM
        if shares.BACKstate == self.S0_INIT:
            pass

        if shares.BACKstate == self.S4_COLLECT_DATA:
            pass

        if shares.BACKstate == self.S5_STOP_COLLECTING:
            self.moe1.set_duty(0)
            self.moe1.disable()

