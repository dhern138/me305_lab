
''' @file       UI_front_part1.py
    @brief      Sets serial link with Nucleo board to read and write data.
    @details    Sends information back and forth between Nucleo and laptop
    @author     David Hernandez
    @date       2/25/21
    @copyright  2020-2021 Hernandez
'''

import keyboard
import serial
from array import array
from matplotlib import pyplot
import csv

last_key = None

def kb_cb(key):
    """@brief Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name
    ser.write(str(last_key).encode('ascii'))
    
# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode()
#     return myval

def write_csv(file,x,y):
    '''@brief writes a csv file from data collected
    '''
    n = 0
    with open('Lab0xffdata.csv','w',newline='') as data:
        data = csv.writer(data, delimiter = ',', quotechar = '|', quoting = csv.QUOTE_MINIMAL)
        for point in x:
            data.writerow([str(x[n]),str(y[n])])
            n += 1
    pass
    
datacollect = []
##@brief All data read collected in this empty array
#
times = array('f', 3001*[0])
##@brief Collected input data placed in this empty array
#
values = array('f', 3001*[0])
##@brief Collected output data placed in this empty array
#
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
##@brief Opens serial port to communicate with Nucleo
#
n = 0
##@brief Initialized the loop count for data collection
#

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb)#, suppress = True)
keyboard.on_release_key("G", callback=kb_cb)#, suppress = True)

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    print('Press G to collect data:\r\nPress S to STOP anytime:')
     ## this loop forever, or at least until someone presses control-C
    while True:
        try:

            ## Read the data written by the backend Nucleo
            value = ser.readline().decode()

            ## Loops through each iteration from backend
            if n == 0:
                last_key == 'G'
                last_key = None
                n += 1
            ## Breaks out of loop when data has finished compiling
            if value == 'Complete':
                ser.close()
                break
            ## Does not allow quotes, only integers
            if value == '' or value == ' ' or value == '  ':
                pass
            ## Removes excess str
            else:
                value = value.strip()
                datacollect.append(value)
                
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed')
            break

#print('value = '+str(value))
#('data = '+str(data))

## Splits the developed data into two columns in order to plot
for i in range(len(datacollect)):
    a = datacollect[i]
    #print('a = '+str(a))
    b = a.split(',')
    #print('b = '+str(b))
    times[i] = float(b[0])
    values[i] = float(b[1])
    
print(times)
#print(values)

## Plot collected data
pyplot.figure()
pyplot.plot(times,values)
pyplot.xlabel('Time Elapsed (s)')
pyplot.ylabel('Data Collected')

try:
    ## Send data to csv file
    f = open('Lab0xffdata.csv', "x")
except:
    pass
write_csv('Lab0xffdata.csv',times,values)

# Close serial port
ser.close()
# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
