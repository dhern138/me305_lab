## \mainpage Operation Manual
#
#  Tuning Power Output using an ME405 Board with a DCX22S Motor via Python.
#
#
#  All files pertaining to this report can be obtained and viewed from the following link
#  https://bitbucket.org/dhern138/me305_lab/src/master/Lab4/
#
#
#  @author    David Hernandez
#  @date      3-18-2021
#  @copyright 2020-2021 Hernandez
#
#
# This manual is divided in the following sections:
# - \subpage intro
# - \subpage advanced "Advanced usage"
# 

## \page intro Introduction
#  The intention is to develop skills to operate a Nucleo Board, Encoder, Motor
#  individually and later simultaneously via the computer language of Python. 
#  There are a total of four parts that build on one another.
#  The goal is to have independent classes within Python that can work with 
#  independent taskers to run the motor, while collecting data from both the encoder
#  and motor. Then using this built program to run provided referenced data that can 
#  track said data.
#  Next is the \ref advanced "advanced section".
#


## \page advanced Advanced Usage
# This page is for advanced users.
# Make sure you have first read \ref intro "the introduction".
#
# There will be a total of: 2 Tasks, 3 Drivers, 1 User Interface, 1 Task Runner, and 1 Shares py files.
# These files will be the total at the end of the four parts, however, there will be many iterations
# building up to these, where you may not need all of them. As said, there will be iterations of the same files
# because they are modified through each part.
#
# Task 1 - Back end Task that will act as a finite state machine (FSM) to switch between states. Most importantly
#          this task will collect all the data (Equation, Encoder, and Motor). The timer was built within the FSM via utime.
#          After data collection, it uses the UART read/write, more specifically the write in this case. It writes all the appended
#          collected data from the Nucleo over to the PC for processing. It also has a period to run the task set in the contructor.
#
# Task 2 - Controller Task that will also act as a FSM. This task will be in charge of controlling the motor, encoder,
#          and all pertinent drivers. The Nucleo has several channels, pins, and timers to choose from, so the classes
#          were made as modifiable as possible for any appropriate combination of pin and timers. Note: It is crucial 
#          to have separate combinations for each encoder and motor set up. Each must have a unique combination. 
#          Having those encoder and motors properly set up will allow this task to use the functions within the classes.
#          It will also place any gatherd data into the shared file for inter use between tasks.
#          It also has a period to run the task set in the contructor.
#
# Driver 1 - Encoder driver is a class specifically built for functions around the encoder, but later builds into
#            an all encompassing driver, meaning it provided functions for both encoder and motors. This driver class
#            will create objects of the encoder pins, channels, and timers first. By connecting the encoder pins, the driver
#            wass able to count the revolutions of the encoder shaft. This provides positional data of the encoder, which can
#            later be mathematically converted to degrees for ease of use. In addition, because the counter value is not the 
#            angle value, corrections had to be made. Using the period selected of 0xFFFF and taking the current and previous
#            count value, a corrected position developed. Then using this developed position with the old angle a more accurate
#            position (angle) was evaluated. This newly evaluated angle was used as the position for the encoder in use.
#            A timer was also implemented into the class for the purpose of calculating the speed of the motor when needed.
#            The update function dealt with continuously updating the angle, and speed of the encoder/motor. 
#            The other functions:
#            set_position is self-explanatory. It allowed for the positioning of the encoder by inputting desired location.
#            get_position also self-explantory. Provided the current location of the encoder in [DEG].
#            get_delta provided the current measured speed value in [RPM].
#
# Driver 2 - Motor driver is a class with the purpose of establishing the objects required to set up the pins,channels,and timer.
#            This driver had an extra pin that is required to enable and disable the desired motor. This class created the objects,
#            but specific pin,channel, and timer combinations are selected via Task 2 above.
#            Functions involved:
#            Enabling and disabling the motor for operation.
#            set_duty which acted as the 'Truth Table' Running clockwise, counter-clockwise, or braking according to the
#            Pulse-Width-Measurement (PWM)
#
# Driver 3 - Closed loop is a class that helps control the power output. It uses the parameters of: proportional gain Kp,
#            referenced speed, and the measured speed via driver 1. These values allowed control of the velocity. Using equation (2)
#            L = Kp(referenced speed - measured speed), where L is the PWM level. For the final part, this equation was modified
#            to include a second gain that would help stabilize the vibration and noise of the motor. This value was Ki and it was 
#            multiplied with the difference inn the referenced position and measured position and added to equation (2) to evaluate
#            a more accurate PWM level (L).
#            This class had the functions:
#            get_Kp it provided the value of the current Kp.
#            set_Kp it allowed user input for a new Kp value.
#            set_Ki it allowed user input for a new Ki.
#
# User Interface - This program is the file the user interacts with the most. This file list the commands for each designated key
#                  allowing the user to visually interpret which action to take. It communicates mainly with the back end task via
#                  a link created by opening a serial line between the Nucleo and the PC. This allows for one way travel of information
#                  read or written. Specifically this program reads the information written by the back end. Note: Because it is a serial
#                  line, it only reads and writes 1 line of information at a time. This data is gathered then placed in plots and published
#                  into a comma separated values (CSV) file.
#
# Task Runner - As the name implies, this runs both of the main tasks,Back end task and Controller task. It controls the period at which 
#               each task is ran. It allows for task with higher priorities to run first and simulates simultaneous synchronicity.
#
# Shares- It behaves as a bank for variables. It stores all desired variable and allows them to be retrieved by any task.