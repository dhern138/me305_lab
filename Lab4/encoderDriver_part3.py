# -*- coding: utf-8 -*-
''' @file       encoderDriver_part3.py
    @brief      Collects and updates positional and speed data
    @details    Collects, appends and updates the data received from the encoder
                and motors actuated. Then sends that data to controllerTask.
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''

import utime
import pyb
from pyb import Pin, Timer
from math import pi


class encoderDriver_part3:
    ''' @brief Class that will create objects for an encoder and update corrected position and speed.
        @details    This encoder class will continuously interpret the information from
                    quadrature encoder. This data will be able to be pulled for viewing
                    and for collection.
    '''

    def __init__(self,PinA, PinB, TIM):  # Constructor to initialize attributes
        ''' @brief      Initialization of the system
            @details    All methods and attributes associated with the encoder class
            @param      PinA pyb.Pin.cpu.### for first input of encoder used
            @param      PinB pyb.Pin.cpu.### for second input of encoder used.
            @TIM        Timer being used to run encoder of choice
        '''

        self.PinA = Pin(PinA, pyb.Pin.IN)
        ## @brief       Sets up generic pin input to be called on by a task
        #  @details     First pin input for encoder
        #
        self.PinB = Pin(PinB, pyb.Pin.IN)
        ## @brief       Sets up generic pin input to be called on by a task
        #  @details     Second pin input for encoder
        #

        ## Encoder timer set-up
        # Object of clock that reads the encoders channels
        self.TIM = Timer(TIM, period=0xFFFF, prescaler=0)
        ## @brief       Sets up generic timer input to be called on by a task
        #
        # Object of channels for encoder
        self.TIM.channel(1, Timer.ENC_AB, pin = self.PinA)
        self.TIM.channel(2, Timer.ENC_AB, pin = self.PinB)


        self.period = 0xFFFF
        ## @brief Period of timer @ [80 MHz]
        #
        self.encoderPosition = 0
        ## @brief most up-to-date encoder position in [deg]
        #
        self.deltaCorrection = 0
        ## @brief Corrected difference in position of the encoder in [ticks]
        #
        self.speedMeas = 0
        ## @brief most up-to-date measured motor speed in [RPM]
        #
        self.position = [0,0]
        ## @brief Creates an array to hold previous and current positions [n-1, n]
        #

        self.runTime = [0,0]
        ## @brief Creates an array to hold previous and current positions [n-1, n]
        #


    ## Functions associated with each button command
    def update(self, oldDelta):
        ''' @brief      Called regularly to update recorded position and speed.
            @details    Algorithm to correct delta in the case of overflow or underflow
                        of both the encoder and motor. Then sends the appended data to
                        the controllerTask.
            @param      oldDelta keeps track of initial delta for calculations
        '''
        
        self.counter = self.TIM.counter()
        ## Run time of motor in seconds [s]
        self.runTime.pop(0)
        self.runTime.append(utime.ticks_us())
        self.deltaTime = (self.runTime[1] - self.runTime[0])
        # print('Counter = ' +str(self.counter))
        ## @brief   Counts each turn of the encoder
        #
        self.position.pop(0)
        ## @brief   Removes 0 counts from appended values, leaving n-1 from counter
        #
        # Append values of previous position of the counter
        self.position.append(self.counter)
        # Develops the difference in position, Delta
        self.deltaPosition = self.position[1] - self.position[0]
        self.deltaTime = (self.runTime[1] - self.runTime[0])


        ## Alogorithm
        # CASE A
        if -0.5*self.period <= self.deltaPosition <= 0.5*self.period:
            self.deltaCorrection = self.deltaPosition


        # CASE B
        elif self.deltaPosition > 0.5*self.period:
            self.deltaCorrection = self.deltaPosition - self.period


        # CASE C
        elif self.deltaPosition < -0.5*self.period:
            self.deltaCorrection = self.deltaPosition + self.period

        # Gives corrected position of the encoder in [deg]
        # Unit conversion: [ticks*(REV/ticks)*]
        self.encoderPosition = (oldDelta + self.deltaCorrection)*((180*pi)/4000)


        # Gives corrected angular speed in [RPM]
        if self.deltaTime == 0:
            self.speedMeas = 0
        else:
            ## Calculation [(ticks/s)*(REV/ticks)*(s/min)]
            self.speedMeas = (self.deltaCorrection/self.deltaTime)*(60/4000)*(1000000)
        return self.encoderPosition, self.speedMeas


    def set_position(self,reset):
        ''' @brief Resets the position 0 where encoder is physically placed
            @param reset sets the position to 0 at desired location
        '''
        return reset


    def get_position(self):
        ''' brief Returns the most recently updated position of the encoder
        '''
        return self.encoderPosition


    def get_delta(self):
        ''' @brief      Returns the angular speed of motor
            @details    Divides the corrected position by the period of timer 
                        to get the angular speed of the motor
        '''
        return self.speedMeas
