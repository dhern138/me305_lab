# -*- coding: utf-8 -*-
''' @file       main_part2.py
    @brief      Runs tasks at set period for encoder only.
    @details    Will call on both tasks to run simultaneously
    @author     David Hernandez
    @date       3/4/21
    @copyright  2020-2021 Hernandez
'''

from encoderTask import encoderTask
from UI_backTask_part2 import UI_backTask_part2

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    task1 = encoderTask()
    task2 = UI_backTask_part2()
    
    # USER Interface commands
    print("\nHello, please view commands below")
    print("Press 'z' to zero the encoder 1 position")
    print("Press 'p' to print out the encoder 1 position")
    print("Press 'd' to print out the encoder 1 delta")
    print("Press 'g' to collect encoder 1 data for 30 seconds")
    print("Press 's' to end data collection prematurely")
    
    while True:
        try:
            # print('T1:')
            task1.run()
            task2.run()
            
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break