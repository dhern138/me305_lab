''' @file       UI_backTask_part4.py
    @brief      This file gathers data request by user via UI.
    @details    Sends formatted data collected from the encoder and motor
                between the Nucleo and computer.
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''

import utime
import pyb
from array import array
from pyb import UART
import shares


pyb.repl_uart(None)

class UI_backTask_part4:
    ''' @brief      A task class that writes gathered motor speed and positional data to UI for processing.
        @details    When user interface commands are pressed, this task will work with other task
                    to gather any data user demands. Then using UART will write the data over to
                    the front end for processing.
    '''

    ## ASCII CODES FOR:
        # G;g = 71;103
        # S;s = 83;115
        # Z;z = 90;122
        # P;p = 80;112
        # D;d = 68;100
        # M;m = 77;109
        # K;k = 75;107
        # I,i = 49; 69

    S0_INIT                 = 0 # User PRESSES:
    S1_ZERO_POSITION        = 1 # Z
    S2_PRINT_ENC1_POSITION  = 2 # P
    S3_PRINT_ENC1_DELTA     = 3 # D
    S4_COLLECT_DATA         = 4 # G
    S5_STOP_COLLECTING      = 5 # S
    S6_GET_KP               = 6 # M
    S7_SET_KP               = 7 # K
    S8_SET_KI               = 8 # I


    def __init__(self, back_period):  # Constructor to initialize attributes

        self.n = 0
        ## @brief   Initialized counter that will send uart written data to computer
        #
        self.myuart = UART(2)
        ##@brief Establishes communication link to Nucleo
        #
        self.times = array('f',[0])
        # ##@brief Creates blank array for times
        # #
        self.values = array('f',[0])
        ##@brief Creates blank array for speed
        #
        self.position = array('f',[0])
        ##@brief Creates blank array for position
        #

        ## Timestamp for last iteration of the task in microseconds
        #  could eliminate for concision if desired
        self.lastTime = utime.ticks_us()
        ## The period for the task in microseconds
        self.period = back_period
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)


        print('Link Established')


    def run(self):

        while self.n!=0:
                self.myuart.readinto(shares.speedref[1000])
                self.n += 1

        thisTime = utime.ticks_us()
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)

        ## Initial state
        if shares.ENCODERstate == self.S0_INIT:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## User presses Z, zero out encoder position
        elif shares.ENCODERstate == self.S1_ZERO_POSITION:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## User presses P, print out up-to-date encoder position
        elif shares.ENCODERstate == self.S2_PRINT_ENC1_POSITION:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## User presses D, print out encoder difference in recent positions
        elif shares.ENCODERstate == self.S3_PRINT_ENC1_DELTA:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## User presses M, print out encoder difference in recent positions
        elif shares.ENCODERstate == self.S6_GET_KP:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## User presses K, print out encoder difference in recent positions
        elif shares.ENCODERstate == self.S7_SET_KP:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## User presses I, print out encoder difference in recent positions
        elif shares.ENCODERstate == self.S8_SET_KI:
            if self.myuart.any() != 0:
                self.keyboardInput()


        else:
            pass

        if shares.BACKstate == self.S0_INIT:
            if self.myuart.any() != 0:
                self.keyboardInput()


        ## If G button is pressed on keyboard it will transition to state 1
        elif shares.BACKstate == self.S4_COLLECT_DATA:
            if self.myuart.any() != 0:
                self.keyboardInput()

            self.currentTime = utime.ticks_ms()
            ## @brief   Current time since data gather began
            #
            self.deltaTime = utime.ticks_diff(self.currentTime,self.startTime)
            ## @brief   Corrected time between the current and start times
            
            print('deltaTime = '+str(self.deltaTime/1000))


            ## Data collection complete after 30 seconds
            if self.deltaTime >= 15000:
                print('15 seconds of data')
                self.BACK_transitionTo(self.S5_STOP_COLLECTING)


            # The amount of inputs into equation and array.
            elif 45 <= self.deltaTime%50 <= 55:
                self.times.append(self.deltaTime/1000)
                self.values.append(shares.delta_1)
                self.position.append(shares.encoderPosition)
                #self.values2.append(shares.delta_2)
            pass


        ## End of data collection. Write data.
        elif shares.BACKstate == self.S5_STOP_COLLECTING:
            if self.myuart.any() != 0:
                self.keyboardInput()
            print('S5 Retrieving data..')

            ## Print data as CSV in console
            while self.n!=len(self.times):
                self.myuart.write('{:},{:},{:}\r\n'.format(self.times[self.n],self.values[self.n],self.position[self.n]))
                self.n += 1

            self.myuart.write('Complete')
            print('Complete')
            # Standby for next iteration
            self.BACK_transitionTo(self.S0_INIT)
            pass

        else: 
            pass 
      # code to run if state is invalid
      # program should ideally never reach here

    def keyboardInput(self):
        ''' @brief Function that creates an instance for each key press
            @details    When a key is pressed the function calls on the respective
                        state and transitions to the appropriate FSM
        '''
        self.val = self.myuart.readchar()
        if self.val == 90 or self.val == 122:       # if input is 'Z' or 'z'
            self.ENCODER_transitionTo(1)
        elif self.val == 80 or self.val == 112:     # if input is 'P' or 'p'
            self.ENCODER_transitionTo(2)
        elif self.val == 68:# or self.val == 100:   # if input is 'D' or 'd'
            self.ENCODER_transitionTo(3)
        elif self.val == 71 or self.val == 103:     # if input is 'G' or 'g'
            self.startTime = utime.ticks_ms()
            ## @brief Initiates time when gathering data begins
            #
            print('Data Collection Started')
            self.BACK_transitionTo(4)
        elif self.val == 83 or self.val == 115:     # if input is 'S' or 's'
            self.BACK_transitionTo(5)
        elif self.val == 77 or self.val == 109:     # if input is 'M' or 'm'
            self.ENCODER_transitionTo(6)
        elif self.val == 75 or self.val == 107:     # if input is 'K' or 'k'
            self.ENCODER_transitionTo(7)
        elif self.val == 49 or self.val == 69:     # if input is 'I' or 'i'
            self.ENCODER_transitionTo(8)
        else:
            pass


    def BACK_transitionTo(self, newState):
        """ @brief          Function that assists with the transition of UI_back_Task relates states
            @param          newState Input variable, new State that we want to move to.
        """
        print('S{:}->S{:}'.format(shares.BACKstate,newState)) 
        shares.BACKstate = newState 
        
    def ENCODER_transitionTo(self, newState):
        """ @brief          Function that assists with the transition of controllerTask related states
            @param          newState Input variable, new State that we want to move to.
        """
        print('S{:}->S{:}'.format(shares.ENCODERstate,newState))
        shares.ENCODERstate = newState 