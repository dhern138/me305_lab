
''' @file       UI_backTask_part1.py
    @brief      Uses UART to writes appended data from time and equation to UI.
    @details    Sends formatted data collected between Nucleo and laptop
    @author     David Hernandez
    @date       2/25/21
    @copyright  2020-2021 Hernandez
'''

import utime
import pyb
from array import array
from math import exp, sin, pi
from pyb import UART
pyb.repl_uart(None)

## Generating arrays to localize data into a plot while reducing ram usage.
#  The arrays will use floats,f, as the data type

if __name__=="__main__":
    '''@brief   Initiates program when script is executed only.
    '''
    # Program initialization goes here
    state = 0   # Initial state is the init state
    print('S0')
    myuart = UART(2)
    ##@brief Establishes communication link to Nucleo
    #
    times = array('f',[])
    ##@brief Creates blank array for times
    #
    values = array('f',[])
    ##@brief Creates blank array for values
    #

    print('Link Established')
    while True:
        try:
            
            ## Initial state
            if state == 0:
                if myuart.any() != 0: # Stating value returned
                    val = myuart.readchar()
                    print(val)
                    ##@brief Decodes input ASCII character into integer
                    #
                    print('Data Collection Started')
                    ## If G button is pressed on keyboard it will transition to state 1
                    if val == 71 or val == 103:
                        # Transition to state 1
                        state = 1
                        # Printing transition from S0 to S1
                        print('S0->S1')
                        startTime = utime.ticks_ms()
                        print('startTime = '+str(startTime))
                        ##@brief Initial timer [ms]
                        #
                        
                    ## User inputs incorrect letter
                    elif val != 71 or val != 103:
                        print('Wrong button')
                        state = 0
                        print('Return to S0')
                    
            ## Collecion of Data Starts here
            elif state == 1:
                if myuart.any() != 0: # Stating value returned
                    val = myuart.readchar() # Input translated
                    ## If S button is pressed on keyboard it will transition to state 2
                    if val == 83 or val ==115:
                        state = 2
                        n = 0
                        # Printing transition from S1 to S2
                        print('S1->S2')
                        print('Data Collection Stopped')
                        
                    # For debugging purposes
                    elif val != 83 or val != 115:
                        print('ERROR: WRONG BUTTON')
                        print('PRESS S TO STOP DATA COLLECTION')

                currentTime = utime.ticks_ms()
                print('currentTime = '+str(currentTime))
                ##@brief Current timer [ms]
                #
                deltaTime = utime.ticks_diff(currentTime,startTime)
                print('deltaTime = '+str(deltaTime))
                print('deltaTime%1000 = '+str(deltaTime%500))
                ##@brief Difference in timer [s]
                #
                ## Data collection complete after 30 seconds
                if deltaTime >= 30000:
                    print('30s worth of data collected.')
                    state = 2 # Updating state for next iteration
                    n=0
                
                ## The amount of inputs into equation and array.
                elif 45 <= deltaTime%50 <= 55:
                    times.append(deltaTime/1000)
                    # print(times)
                    value = exp(-deltaTime/1000/10)*sin(2*pi/3*deltaTime/1000)
                    ##@brief Output of equation using arrayed time
                    #
                    # Appends each output into a list
                    values.append(value)


            ## End of data collection. Write data.
            elif state == 2:
                print('S2')
                ## Print data as CSV in console
                while n!=len(times):
                    myuart.write('{:},{:}\r\n'.format(times[n],values[n]))
                    n+=1
                myuart.write('Complete')
                print('Complete')
                # Standby for next iteration
                state = 0 
                print('SO again')
            
            else: 
                pass 
            # code to run if state is invalid
            # program should ideally never reach here
            
        except KeyboardInterrupt:
                # This except block catches "Ctrl+C" from the keyboard
                # while (True) loop when desired
                break

