# -*- coding: utf-8 -*-
''' @file       encoderDriver_part2.py
    @brief      Configures pins and timer of an encoder to collect data.
    @details    Updates a separate py files 'shares.py' with position data that
                can be pulled by user
    @author     David Hernandez
    @date       3/4/21
    @copyright  2020-2021 Hernandez
'''

import pyb
from pyb import Pin, Timer



class encoderDriver_part2:
    ''' @brief Class that will created objects for an encoder and update corrected encoder positions.
        @details    This encoder class will continuously interpret the information from
                    quadrature encoder. This data will be able to be pulled for viewing
                    and for collection.
    '''

    def __init__(self,PinA, PinB, TIM):  # Constructor to initialize attributes
        ''' @brief      Initialization of the system
            @details    All methods and attributes associated with the encoder class
            @param      CHA pyb.Pin.cpu.### for first input of encoder used
            @param      CHB pyb.Pin.cpu.### for second input of encoder used.
            @TIM        Timer being used to run encoder of choice
        '''

        self.PinA = Pin(PinA, pyb.Pin.IN)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     First pin input for encoder
        #
        self.PinB = Pin(PinB, pyb.Pin.IN)
        ## @brief       Locates respective pin on the Nucleo board
        #  @details     Second pin input for encoder
        #

        ## Encoder timer set-up
        # Object of clock that reads the encoders channels
        self.TIM = Timer(TIM, period=0xFFFF, prescaler=0)
        
        # Object of channels for encoder
        self.TIM.channel(1, Timer.ENC_AB, pin = self.PinA)
        self.TIM.channel(2, Timer.ENC_AB, pin = self.PinB)


        self.period = 0xFFFF
        ## @brief Period of timer @ [80 MHz]
        #
        self.encoderPosition = 0
        ## @brief most up-to-date encoder position
        #
        self.deltaCorrection = 0


        self.position = [0,0]
        ## @brief Creates an array to hold previous and current positions [n-1, n]
        #


    ## Functions associated with each button command
    def update(self, oldDelta):
        ''' @brief Called regularly to update recorded position of encoder
            @details Algorithm to correct delta in the case of overflow or underflow
        '''
        
        self.counter = self.TIM.counter()
        # print('Counter = ' +str(self.counter))
        ## @brief   Counts each turn of the encoder
        #
        self.position.pop(0)
        ## @brief   Removes 0 counts from appended values, leaving n-1 from counter
        #
        # Append values of previous position of the counter
        self.position.append(self.counter)
        # Develops the difference in position, Delta
        self.deltaPosition = self.position[1] - self.position[0]


        ## Alogorithm
        # CASE A
        if -0.5*self.period <= self.deltaPosition <= 0.5*self.period:
            self.deltaCorrection = self.deltaPosition


        # CASE B, +1 accounts for pop correction
        elif self.deltaPosition > 0.5*self.period:
            self.deltaCorrection = self.deltaPosition - self.period


        # CASE C, +1 accounts for pop correction
        elif self.deltaPosition < -0.5*self.period:
            self.deltaCorrection = self.deltaPosition + self.period


        self.encoderPosition = oldDelta + self.deltaCorrection
        
        return self.encoderPosition


    def set_position(self,reset):
        ''' @brief Resets the position 0 where encoder is physically placed
        '''
        return reset
        ## @brief Creates new 'ZERO'
        #


    def get_position(self):
        ''' brief Returns the most recently updated position of the encoder
        '''
        return self.encoderPosition


    def get_delta(self):
        ''' @brief      Returns the angular speed of encoder
            @details    Divides the corrected position by the period of timer 
                        to get the angular speed of the encoder
        '''
        return self.deltaPosition
