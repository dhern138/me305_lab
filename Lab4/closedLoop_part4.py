# -*- coding: utf-8 -*-
''' @file       closedLoop_part4.py
    @brief      This class updates and calculates 2 factors into power applied to motor.
    @details    The controllerTask will call on this class to operate motors
                The gain constant, and measured speed will continuously update
                the controllerTask with the new power output, until desired 
                speed is achieved
    @author     David Hernandez
    @date       3/18/21
    @copyright  2020-2021 Hernandez
'''
from math import pi

class closedLoop_part4:
    ''' @brief      This class will establish the power output and speed of a motor with Kp and Ki modifiers.
    '''

    def __init__(self):
        ''' @brief      This function established all methods required for motor
                        operation.
        '''
        
        ## Motor output initialization
        self.Pmax         = 100                      # Max PWM
        self.Pmin         = -100                     # Min PWM
        self.PWM          = 20                       # Current PWM
        self.Kp           = 0                        # Gain constant
        self.PWMdelta     = 0                        # Previous change in PWM
        self.Ki           = 0                        # Position gain constant


    def update(self, speedref, speedMeas,posref, posMeas):
        ''' @brief      This function will calculated and update PWM level.
            @param      speedref allows user input desired reference speed [rpm]
            @param      speedMeas takes in calculated measured speed of motor [%*s/rad]
        '''
        ## Actual power being output by motor [%].
         # Speeds in [RPM]
         # Kp [%/RPM]
        self.PWMdelta = (self.Kp*pi/30)*(speedref - speedMeas) + (self.Ki/100000)*(posref - posMeas)
        if self.PWMdelta >= self.Pmin and self.PWMdelta <= self.Pmax:
            self.PWM = self.PWMdelta

        ## Shut off motor if power below minimum
        elif self.PWMdelta <= self.Pmin:
            self.PWM = -100

        ## Shuf off motor if power above maximum
        elif self.PWMdelta >= self.Pmax:
            self.PWM = 100


        return self.PWM

    def get_Kp(self):
        ''' @brief      This functions gets the current constant gain of the motor.
        '''
        return self.Kp

    def set_Kp(self,inputKp):
        ''' @brief      This functions sets the constant gain to desired input.
            @param      inputKp is the desired gain constant input
        '''
        self.Kp = inputKp

    def set_Ki(self,inputKi):
        ''' @brief      This functions sets the constant position gain to desired input.
            @param      inputKi is the desired position gain constant input
        '''
        self.Ki = inputKi