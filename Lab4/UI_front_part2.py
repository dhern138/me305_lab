''' @file       UI_front_part2.py
    @brief      Maintains a serial line between the Nucleo and the PC to gather data.
    @details    Sends information back and forth between Nucleo and laptop using UART
                This data is collected serially, then plotted via python plots and 
                simultaneously exported via a CSV file.
    @author     David Hernandez
    @date       3/4/21
    @copyright  2020-2021 Hernandez
'''

import keyboard
import serial
from array import array
from matplotlib import pyplot
import csv


def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name


def write_csv(file,x,y):
    ''' @brief writes a csv file from data collected
    '''
    n = 0
    with open('Lab0xffdata_part2.csv','w',newline='') as data:
        data = csv.writer(data, delimiter = ',', quotechar = '|', quoting = csv.QUOTE_MINIMAL)
        for point in x:
            data.writerow([str(x[n]),str(y[n])])
            n += 1
    pass


# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb)
## @brief End data collection prematurely
#
keyboard.on_release_key("G", callback=kb_cb)
## @brief Collect encoder 1 data for 30 seconds
#
keyboard.on_release_key("Z", callback=kb_cb)
## @brief Zero the encoder 1 position
#
keyboard.on_release_key("P", callback=kb_cb)
## @brief Print out the encoder 1 position
#
keyboard.on_release_key("D", callback=kb_cb)
## @brief Print out the encoder 1 delta
#

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization

    ## Store the last input key from the Keyboard
    last_key = None
    ## @brief Verifies if a key has been pressed or not
    #
    dataCollect = []
    ## @brief All data read collected in this empty array
    #
    n = 0
    ## @brief Initialized the loop count for data collection
    #
    times = array('f', [])
    ## @brief Collected input data placed in this empty array
    #
    values = array('f', [])
    ## @brief Collected output data placed in this empty array
    #
    ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
    ## @brief Opens serial port to communicate with Nucleo
    #
    print('Press G to collect data:\r\nPress S to STOP anytime:\r\nPress Z to ZERO encoder position'
          '\r\nPress P to PRINT out encoder position:\r\nPress D to PRINT out encoder delta:')


    while True:
        try:


            ## Provides the user feedback when the appropriate command is pressed
            if last_key is not None:
                print("You pressed " + last_key)
                ser.write(str(last_key).encode('ascii'))
                last_key = None


            ## Store the data sent from the Nucleo
            value = ser.readline().decode()
            if value == 'Complete':
                ser.close()
                break


            ## Removes spaces
            elif value == '':
                pass


            ## Strips unecessary info stripped for integers
            else:
                value = value.strip() # strip the value from Nucleo of white space
                dataCollect.append(value) # store it in dataReceived array


        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed')
            break


#print('value = '+str(value))
#('data = '+str(data))
#print('datacollect = ' +str(datacollect))
## Splits the developed data into two columns in order to plot
for i in range(len(dataCollect)):
        dataStrip = dataCollect[i]
        dataSplit = dataStrip.split(',')
        times.append(float(dataSplit[0]))
        values.append(float(dataSplit[1]))

#print(times)
#print(values)
print('Data Collection Complete')

## Plot collected data
pyplot.figure()
pyplot.plot(times,values)
pyplot.xlabel('Time Elapsed (s)')
pyplot.ylabel('Data Collected')


## Places collected data into a csv
try:
    ## Send data to csv file
    f = open('Lab0xffdata_part2.csv', "x")
except:
    pass
write_csv('Lab0xffdata_part2.csv',times,values)

print('Data collected and published')

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()